### [华为云手机开源项目仓主入口](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-phone)

## Android-SDK

### 版本更新：

- #### 24.12.0

  - 升级传输协议，优化弱网环境下的使用体验。
  - 解决已知问题。
  - **注意：24.12.0版本需要搭配CPH官网2024-12-06及以后发布的镜像使用，否则会有不兼容问题。**
  
- #### 24.3.0

  - SDK版本更新内容为：
    - 连接信息字段增加**必选**字段**client_mode**，字段内容为**direct**或**management**，direct代表调用请求华为云CPH服务提供的[获取连接信息](https://support.huaweicloud.com/api-cph/cph_api_0564.html)接口的方式获取连接信息，云侧CAE校验该接口返回字段；management代表连接信息为自研的服务端生成，云侧CAE通过配置接口来校验，即原有校验逻辑。
    - 新增可选字段region_id，内容为当前连接云手机所在region的id，用于direct模式下校验连接信息时下载公钥，配置region_id时，公钥下载流程更快更稳定。
  - App版本更新内容为：通过登陆华为云账号，获取账号token，请求华为云CPH服务提供的[获取连接信息](https://support.huaweicloud.com/api-cph/cph_api_0564.html)接口，获取到指定云手机的连接信息，通过当前版本SDK发送连接信息到云机，即可成功连接云手机。云手机镜像需要为发布时间为2024-04-29之后的镜像，可在官网文档查看[镜像发布动态](https://support.huaweicloud.com/wtsnew-cph/cph_wtsnew_0004.html)。

### 1. 方案简介

华为云手机端云协同方案，致力于高效、便捷的接入并使用华为云手机，涵盖信令交互、接入鉴权、音视频流传输、解码渲染、触控采集等模块，使得客户构建云游戏、个人云手机等业务场景变的简单。客户在云手机内部署 CloudAppEngine，真机侧集成 Android 和 H5 的 SDK，RemoteServer 实现相关 RestApi 接口，进一步结合业务场景需要来丰富相关模块，实现完善的接入方案。

### 2. 组件交互

![](res/组件交互.png)

1. SDK->RemoteServer: 客户端 SDK 向后端 server 请求一个空闲云机实例
2. RemoteServer->SDK: 后端 server 按需求找到一个空闲云机实例信息返回给客户端 SDK
3. SDK->cloudAppEngine: 客户端 SDK 通过云机实例信息向云机服务进程 cloudAppEngine 建链、认证，发心跳、控制信令、触控指令等
4. cloudAppEngine->SDK: 云机服务进程 cloudAppEngine 向客户端 SDK 发信令、发送音视频流等
5. cloudAppEngine->RemoteServer: 云机服务进程 cloudAppEngine 验签、上报事件等
6. RemoteServer->cloudAppEngine: 后端 server 响应 cloudAppEngine 的请求

### 3.工程构建

如需要修改源码进行深度开发，可以参考以下内容进行源码修改后的编译构建。如果只想快速体验试用，可直接下载发行版产物进行安装使用，具体可参考 [快速体验试用](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-phone)。

#### 3.1 目录介绍

```
├── app  <使用 SDK 的 demo apk>
│   └── src
│       └── main
│           ├── java
│           │   └── com
│           │       └── huawei
│           │           └── cloudapp
│           │               ├── common  <连接信息、分辨率管理、日志、状态定义>
│           │               ├── ui  <接入连接 activity 页面、云手机/云游戏画面渲染 activity 页面>
│           │               └── utils <工具类>
│           └── res
├── cloudphone  <SDK 封装, aar 形态>
│   ├── jniLibs  <依赖的 lib 库>
│   │   ├── ARM32
│   │   │   └── armeabi-v7a  <32位库>
│   │   └── ARM64
│   │       └── arm64-v8a  <64位库>
│   └── src
│       └── main
│           ├── aidl  <主进程与子进程 aidl 通信的接口>
│           │   └── com
│           │       └── huawei
│           │           └── cloudphone
│           │               └── common
│           ├── java
│           │   └── com
│           │       └── huawei
│           │           └── cloudphone
│           │               ├── api  <SDK 的 API 接口>
│           │               ├── apiimpl  <API 接口的实现、触控采集等>
│           │               ├── audio  <音频播放>
│           │               ├── common  <日志和字节数组操作>
│           │               ├── datacenter  <从 jni native 层获取数据并分发处理>
│           │               ├── jniwrapper  <jni 接口、native 的调用>
│           │               ├── service   <aidl 的 client 和 server>
│           │               └── utils  <工具类>
│           └── cpp
│               ├── cas_common  <公共>
│               ├── cas_decoder  <视频解码渲染>
│               ├── cas_service  <心跳处理、控制信令处理、触控处理、视频解码处理等>
│               ├── cas_socket  <socket 通信>
│               ├── cas_stream  <server 消息解析、给 server 发送消息>
│               ├── hwscure  <华为安全函数库>
│               └── libs  <依赖的三方 lib 库和对应头文件>
│                  ├── Arm32
│                  │   └── armeabi-v7a
│                  ├── Arm64
│                  │   └── arm64-v8a
│                  ├── libopus  <用于音频数据解码>
│                  │   └── include
│                  ├── mtrans  <用于数据传输>
│                  │   └── include
│                  └── openssl  <用于 tls 安全通信>
│                      └── include
│                          └── openssl
│
└── gradle
```

#### 3.2 编译构建


1. build -> Refresh Linked C++ Projects
1. 执行下 cloudphone 的 build.gradle 中 copy64 和 copy32 函数
1. build -> Rebuild Project


```shell
## 项目构建产物路径
app/build/outputs/apk/arm64/release/*.apk
```

#### 3.3 使用发行版

```shell
## 可以直接下载本项目构建好的产物，直接安装 apk 文件，或解压 rar 压缩包集成 aar 文件进行 apk 开发
https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudPhoneAccess-android/releases
```

#### 3.4 AAR引入注意事项

```shell
## 为保证连接安全性，SDK中引入org.bouncycastle:bcprov-jdk15to18三方库，需要在使用AAR时添加依赖。
implementation 'org.bouncycastle:bcprov-jdk15to18:1.71'
```

### 4. 功能接口

#### 4.1 创建实例

- 接口介绍

  static ICloudPhone createCloudPhoneInstance();

- 功能描述

  创建 Android-SDK 对象实例。

- 调用示例

  ICloudPhone cloudPhone = CloudPhoneManager.createCloudPhoneInstance();

#### 4.2 获取版本号

- 接口介绍

  void getVersion();

- 功能描述

  获取SDK版本号。

- 调用示例

  cloudPhone.getVersion();

#### 4.3 初始化

- 接口介绍

  void init(Context context, DevType type);

- 功能描述

  初始化 Android-SDK。

- 参数介绍

  @param { Context } context：云手机应用的 context

  @param { DevType } type：设备类型，支持DEV_PHONE、DEV_TV

- 调用示例

  cloudPhone.init(activity, CloudPhoneParas.DevType.DEV_PHONE);

#### 4.4 反初始化

- 接口介绍

  void deinit();

- 功能描述

  反初始化 Android-SDK。

- 调用示例

  cloudPhone.deinit();

#### 4.5 进入云手机

- 接口介绍

  void startCloudPhone(final Activity activity, final ViewGroup view, final Map<String, String> params);

- 功能描述

  进入云手机。

- 参数介绍

  @param {Activity} activity：进入云手机的 activity

  @param {ViewGroup} view：承载云手机画面的 viewgroup

  @param {Map} params：启动相关配置，具体如下：

  | 参数                | 是否必选  | 参数类型  | 描述                                         |
  | ------------------ | -------- | -------- | ------------------------------------------- |
  | ip                 | 是       | String   | 云手机IP地址                                  |
  | port               | 是       | String   | 云手机端口信息                                 |
  | session_id         | 是       | String   | 会话id                                       |
  | ticket             | 是       | String   | 随机数                                        |
  | aes_key            | 是       | String   | 对称秘钥                                      |
  | auth_ts            | 是       | String   | 验签时间戳                                     |
  | background_timeout | 是       | String   | 页面非活跃状态超时时长 (取值\[16, 3600\]，单位是s) |
  | available_playtime | 是       | String   | 单次连接可用时长 (值为0时，无超时判断)             |
  | touch_timeout      | 否       | String   | 单次连接可用时长 (值为0时，无超时判断)             |
  | user_id            | 否       | String   | 用户id                                        |
  | client_mode        | 是       | String   | direct/management                            |
  | region_id          | 否       | String   | 云手机所在region的id                           |

  其中client_mode为24.3.0版本新增字段，字段为direct或management：

  - direct代表调用请求华为云CPH服务提供的[获取连接信息](https://support.huaweicloud.com/api-cph/cph_api_0564.html)接口的方式获取连接信息直接连接，连接信息包含access_ip、access_port、session_id、timestamp、ticket，分别对应上表的ip、port、session_id、auth_ts、ticket字段，上表的其他字段中aeskey配置为该云手机的phoneId，client_mode配置为direct ，region_id设置为云手机所在region的id，其余字段按需配置即可；
  - management代表由调度管理系统下发给端侧连接信息，端侧SDK发送给云侧CAE，CAE通过请求调度管理平台接口鉴权、上报心跳、上报事件，即为原有调度接入逻辑。

- 调用示例

  cloudPhone.startCloudPhone(activity, view, params);

#### 4.6 退出云手机

- 接口介绍

  void exitCloudPhone();

- 功能描述

  退出云手机。

- 调用示例

  clouPhone.exitCloudPhone();


#### 4.7 设置音视频参数

- 接口介绍

  void setMediaConfig(HashMap<String, String> mediaConfig);

- 功能描述

  设置音视频参数，支持码率、虚拟宽和虚拟高。

- 参数介绍

  @param { HashMap<String, String> mediaConfig} mediaConfig

  | 参数             | 参数类型  | 描述      | 约束                          |
  | --------------- | -------- | -------- | ---------------------------- |
  | bitrate         | String   | 码率      | 设置范围\[100000, 10000000\]  |
  | stream_width    | String   | 虚拟宽    | 240到4096，且为8的倍数，与stream_height乘积不能大于云手机规格宽高乘积 |
  | stream_height   | String   | 虚拟高    | 240到4096，且为8的倍数，与stream_width乘积不能大于云手机规格宽高乘积  |
  | auto_quality    | String   | 画质自动调整开关 | 1：启用，0：关闭 |

- 调用示例

  cloudPhone.setMediaConfig(mediaConfig);

#### 4.8 设置图像显示模式

- 接口介绍

  void setDisplayMode(DisplayMode mode);

- 功能描述

  设置显示模式，支持fit、fill两种模式。

  fit模式保持图像宽高比，真机和图像比例不匹配时可能会填充黑边。

  fill按照真机的宽高比例显示，图像可能被拉伸。默认是fill模式。

- 参数介绍

  @param { DisplayMode} mode：默认为DISPLAY_MODE_FILL，fill模式。

- 调用示例

  cloudPhone.setDisplayMode(CloudPhoneParas.DisplayMode.DISPLAY_MODE_FIT);

#### 4.9 注册状态监听

- 接口介绍

  void registerCloudPhoneStateListener(CloudPhoneStateListener listener);

- 功能描述

  通过注册状态监听器，实现接入状态的监听。

- 参数介绍

  ```java
  @param { CloudPhoneStateListener} listener：状态监听器，接口如下：
  public interface CloudPhoneStateListener {
      void onNotify(int state, String msg);
  }
  ```

  | state                  | code   | msg                       | description  |
  | ---------------------- | ------ | ------------------------- | ------------ |
  | CAS_SERVER_UNREACHABLE | 0x0301 | Server unreachable        | 网络不可达   |
  | CAS_CONNECT_LOST       | 0x0A00 | Connect lost              | 重连失败     |
  | CAS_FIRST_FRAME        | 0x1800 | First frame               | 首帧画面到达 |
  | CAS_TRAIL_PLAY_TIMEOUT | 0x0E00 | Available time usedup     | 试玩超时     |
  | CAS_NOTOUCH_TIMEOUT    | 0x0F00 | Notouch timeout           | 无触控超时   |
  | CAS_BACKGROUND_TIMEOUT | 0x1000 | Switch background timeout | 切后台超时   |
  
- 调用示例

  ```java
  cloudPhone.registerCloudPhoneStateListener(new CloudPhoneStateListener() {
      @Override
      public void onNotify(int state, String msg) {
          // todo
      }
  );
  ```

#### 4.10 注册方向变化监听

- 接口介绍

  void registerOnOrientationChangeListener(CloudPhoneOrientationChangeListener listener);

- 功能描述

  通过注册云手机方向变化监听器，获取画面方向的变化。

- 参数介绍

  ```
  @param { CloudPhoneOrientationChangeListener } listener：云手机方向监听器，接口如下：
  public interface CloudPhoneOrientationChangeListener {
      void onOrientationChange(int orientation);
  }
  ```

- 调用示例

  ```java
  cloudPhone.registerOnOrientationChangeListener(new CloudPhoneOrientationChangeListener() {
          @Override
          public void onOrientationChange(int orientation) {
              // todo
          }
      }
  );
  ```
  

#### 4.11 注册权限变化监听

- 接口介绍

  void registerPermissionRequestListener(CloudPhonePermissionRequestListener listener);

- 功能描述

  通过注册云手机权限监听回调，实现权限动态申请。

- 参数介绍

  ```java
  @param { CloudPhonePermissionRequestListener } listener: 云手机权限监听器, 接口如下:
  
  public interface CloudPhonePermissionRequestListener {
      void onRequestPermissions(CloudPhonePermissionInfo permissionInfo);
  }
  
  public class CloudPhonePermissionInfo {
      private int requestCode;
      private String[] permissions;
  
      public CloudPhonePermissionInfo(int requestCode, String[] permissions) {
          this.requestCode = requestCode;
          this.permissions = permissions;
      }
  
      public int getRequestCode() {
          return this.requestCode;
      }
  
      public String[] getPermissions() {
          return this.permissions;
      }
  }
  
  摄像头权限:
  requestCode: DEV_TYPE_CAMERA
  permissions: {Manifest.permission.CAMERA}
  
  麦克风权限:
  requestCode: DEV_TYPE_MICROPHONE
  permissions: {Manifest.permission.RECORD_AUDIO}
  
  定位权限:
  requestCode: DEV_TYPE_LOCATION
  permissions: {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}
  ```

- 调用示例

  ```java
  cloudPhone.registerPermissionRequestListener(
      new CloudPhonePermissionRequestListener() {
          @Override
          public void onRequestPermissions(CloudPhonePermissionInfo permissionInfo) {
              // todo
          }
      }
  );
  ```

#### 4.12 发送权限申请结果

- 接口介绍

  void sendPermissionResult(int requestCode, int grantResults);

- 功能描述

  发送权限申请结果。

- 调用示例

  ```java
  cloudPhone.sendPermissionResult(requestCode, PackageManager.PERMISSION_GRANTED);
  ```

#### 4.13 注册剪切板变化监听

- 接口介绍

  void registerClipboardListener(CloudPhoneClipboardListener listener);

- 功能描述

  通过注册云手机剪切板监听回调，获取云手机剪切板内容。

- 参数介绍

  ```java
  @param { CloudPhoneClipboardListener } listener: 云手机剪切板监听器, 接口如下:
  
  public interface CloudPhoneClipboardListener {
      void onClipboardChange(byte[] data);
  }
  ```

- 调用示例

  ```java
  cloudPhone.registerClipboardListener(
      new CloudPhoneClipboardListener() {
          @Override
          public void onClipboardChange(byte[] data) {
              // todo
          }
      }
  );
  ```

#### 4.14 发送剪切板内容

- 接口介绍

  void sendClipboardData(byte[] data);

- 功能描述

  将真机剪切板内容设置到云手机剪切板。

- 调用示例

  ```java
  String clipboard = "clipboard";
  cloudPhone.sendClipboardData(clipboard.getBytes());
  ```

#### 4.15 获取网络时延

- 接口介绍

  int getRtt();

- 功能描述

  获取网络时延。

- 调用示例

  cloudPhone.getRtt();

#### 4.16 双向通道 数据发送接口（支持登录、支付场景）

- 接口介绍
  
  void sendDataToCloudApp(byte[] data) throws Exception；

- 功能描述

  发送数据给云手机中运行的游戏或应用

#### 4.17 双向通道 数据接收监听接口（支持登录、支付场景）

- 接口介绍
  
  void registerCloudAppDataListener(CloudAppDataListener listener);

- 功能描述

  注册一个监听回调来接收云手机中运行的游戏或应用发送的数据


### 5. license
Apache License 2.0。

