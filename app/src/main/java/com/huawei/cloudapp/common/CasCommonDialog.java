/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.common;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huawei.cloudapp.R;

public class CasCommonDialog extends Dialog {

    public CasCommonDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    public void show() {
        WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
        layoutParams.flags = layoutParams.flags | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        super.show();
    }

    public static class Builder {
        private Context context;
        private String title;
        private String message;
        private View contentView;
        private String positiveButtonText;
        private String negativeButtonText;
        private OnClickListener positiveButtonClickListener;
        private OnClickListener negativeButtonClickListener;
        private int contentGravity;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText, OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText, OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setGravity(int gravity) {
            this.contentGravity = gravity;
            return this;
        }

        public CasCommonDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final CasCommonDialog dialog = new CasCommonDialog(context, R.style.dialog);
            View layout = inflater.inflate(R.layout.cas_dialog_common, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

            ((TextView) layout.findViewById(R.id.title)).setText(title);
            View dividerLine = layout.findViewById(R.id.divider_line);
            TextView positive = (TextView) layout.findViewById(R.id.positive);
            if (positiveButtonText != null) {
                positive.setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                    });
                }
            } else {
                positive.setVisibility(View.GONE);
                dividerLine.setVisibility(View.GONE);
            }

            TextView negative = (TextView) layout.findViewById(R.id.negative);
            if (negativeButtonText != null) {
                negative.setText(negativeButtonText);
                if (negativeButtonClickListener != null) {
                    negative.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                        }
                    });
                }
            } else {
                negative.setVisibility(View.GONE);
                dividerLine.setVisibility(View.GONE);
            }

            TextView messageTV = (TextView) layout.findViewById(R.id.message);
            if (contentGravity != 0) {
                messageTV.setGravity(contentGravity);
            }
            LinearLayout content = (LinearLayout) layout.findViewById(R.id.content);
            if (message != null) {
                messageTV.setText(message);
            } else if (contentView != null) {
                // if no message set
                // add the contentView to the dialog body
                content.removeAllViews();
                content.addView(contentView, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            }

            dialog.setContentView(layout);

            return dialog;
        }

        public CasCommonDialog show() {
            final CasCommonDialog dialog = create();
            dialog.show();
            return dialog;
        }
    }

    /**
     * dialog listener
     * <p>listen onClick</p>
     */
    public static class DialogClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }
}
