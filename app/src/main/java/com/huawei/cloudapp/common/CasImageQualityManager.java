/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.common;

public class CasImageQualityManager {

    public static final int QUALITY_BEST = 0;
    public static final int QUALITY_MAIN = 1;
    public static final int QUALITY_BASIC = 2;
    private volatile static CasImageQualityManager manager = null;
    private int imageQualityStatus = 0;

    private CasImageQualityManager() {
    }

    public static CasImageQualityManager getInstance() {
        if (null == manager) {
            synchronized (CasImageQualityManager.class) {
                if (null == manager) {
                    manager = new CasImageQualityManager();
                }
            }
        }

        return manager;
    }

    public int getImageQualityStatus() {
        return this.imageQualityStatus;
    }

    public void setImageQualityStatus(int qualityStatus) {
        this.imageQualityStatus = qualityStatus;
    }

}
