/*
 * Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.common;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;

public class CasRecord {

    private SharedPreferences mPreferences;

    public CasRecord(SharedPreferences preferences) {
        mPreferences = preferences;
    }

    public String[] getRecord(String key, int max) {
        String[] keyRecordList = {};
        String keyHistoryListStr = mPreferences.getString(key, "");
        if (!keyHistoryListStr.isEmpty()) {
            keyRecordList = keyHistoryListStr.replace(" ", "").split(",");
        }
        if (keyRecordList.length > max) {
            String[] newArrays = new String[max];
            System.arraycopy(keyRecordList, 0, newArrays, 0, max);
            keyRecordList = newArrays;
        }
        return keyRecordList;
    }

    public void setRecord(String key, String value, int max) {
        String[] keyHistoryList = mPreferences.getString(key, "").split(",");
        if (keyHistoryList.length > max) {
            String[] newArrays = new String[max];
            System.arraycopy(keyHistoryList, 0, newArrays, 0, max);
            keyHistoryList = newArrays;
        }
        if (!Arrays.asList(keyHistoryList).contains(value)) {
            String keyHistoryListStr = Arrays.toString(keyHistoryList).replace(" ", "");
            StringBuilder sb = new StringBuilder(keyHistoryListStr.substring(1, keyHistoryListStr.length() - 1));
            sb.insert(0, value + ",");
            mPreferences.edit().putString(key, sb.toString()).apply();
        }
    }

    public String getRecord(String key) {
        return mPreferences.getString(key, "");
    }

    public void setRecord(String key, String value) {
        mPreferences.edit().putString(key, value).apply();
    }
    public void setHashMapRecord(String key, HashMap<String, String> value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        mPreferences.edit().putString(key, json).apply();
    }

    public HashMap<String, String> getHashMapRecord(String key) {
        String json = mPreferences.getString(key, "");
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        return gson.fromJson(json, type);
    }

}
