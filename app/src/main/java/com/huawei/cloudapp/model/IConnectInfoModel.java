package com.huawei.cloudapp.model;

import com.huawei.cloudapp.model.bean.User;
import com.huawei.cloudapp.model.bean.management.CasConnectInfo;

import java.util.HashMap;
import java.util.List;

import okhttp3.Response;

public interface IConnectInfoModel {
    void getConnectInfo(User user, String phoneId, String region, final OnRequestListener<CasConnectInfo> onRequestListener);
    List<CasConnectInfo> parseResponseToConnectInfo(Response response) throws Exception;
}
