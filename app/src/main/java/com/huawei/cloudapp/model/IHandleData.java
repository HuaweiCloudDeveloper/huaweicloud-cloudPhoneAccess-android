package com.huawei.cloudapp.model;

import java.util.List;

public interface IHandleData<T> {
    void handleData(List<T> list, int count);
    void handleError(Exception e);
}
