package com.huawei.cloudapp.model;

import com.huawei.cloudapp.model.bean.PhoneInfo;
import com.huawei.cloudapp.model.bean.User;
import com.huawei.cloudapp.model.bean.direct.PhoneDetail;
import com.huawei.cloudapp.model.bean.direct.PhoneJobResponse;

import java.util.HashMap;
import java.util.List;

public interface IPhoneModel {
    void getPhoneList(User user, HashMap<String, String> condition, String region, String projectId, int offset, int limit, final OnRequestListener<PhoneInfo> onRequestListener);
    void getPhoneDetailInfo(User user, String region, String projectId, String phoneId, final OnRequestListener<PhoneDetail> onRequestListener);
    void resetPhone(User user, String region, String projectId, List<String> phoneIdList, String imageId, List<HashMap<String, String>> property, final OnRequestListener<PhoneJobResponse> onRequestListener);
    void restartPhone(User user, String region, String projectId, List<String> phoneIdList, String imageId, List<HashMap<String, String>> property, final OnRequestListener<PhoneJobResponse> onRequestListener);
}
