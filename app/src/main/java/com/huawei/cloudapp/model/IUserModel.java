package com.huawei.cloudapp.model;

import com.huawei.cloudapp.model.bean.User;

import java.util.List;
import java.util.Map;

import okhttp3.Response;

public interface IUserModel {
    void getUser(User user, String password, String region, final OnRequestListener<User> onRequestListener);
    List<User> parseResponseToUser(Response response) throws Exception;
}
