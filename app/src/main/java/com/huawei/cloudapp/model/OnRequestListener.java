package com.huawei.cloudapp.model;

import java.util.List;

public interface OnRequestListener<T> {
    void onSuccess(List<T> t, int count);
    void onFailure(Exception e);
}
