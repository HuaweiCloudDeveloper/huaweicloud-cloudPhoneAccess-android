package com.huawei.cloudapp.model.bean;

import java.io.IOException;

public class CustomException extends IOException {
    public CustomException() {
        super();
    }
    public CustomException(String message) {
        super(message);
    }

    public static class EmptyResponseFromServerException extends CustomException {
    }
    public static class FailedToLogInException extends CustomException {
    }
    public static class LogInInfoErrorException extends CustomException {
    }
    public static class LogInInfoInvalidException extends CustomException {
    }
    public static class FailedToGetTokenException extends CustomException {
    }
    public static class FailedToGetPhoneListException extends CustomException {
    }
    public static class FailedToGetPhoneDetailException extends CustomException {
    }
    public static class BadRequestException extends CustomException {
    }
    public static class ForbiddenException extends CustomException {
    }
    public static class ResourceNotFoundException extends CustomException {
    }
    public static class ServerError extends CustomException {
    }
    public static class ServiceUnavailableException extends CustomException {
    }
    public static class TokenExpireException extends CustomException {
    }
    public static class NoPermissionException extends CustomException {
    }
    public static class GetServerListEmptyException extends CustomException {
    }
    public static class ResponseParamsException extends CustomException {
    }
    public static class FailedToGetConnectInfoException extends CustomException {
        public FailedToGetConnectInfoException() {
            super();
        }
        public FailedToGetConnectInfoException(String message) {
            super(message);
        }
    }
    public static class ConnectInfoErrorException extends CustomException {
    }
    public static class ParamInvalidException extends CustomException {
    }
    public static class OpRestrictedException extends CustomException {
    }
    public static class OpSuspendedException extends CustomException {
    }
    public static class OpUnverifiedException extends CustomException {
    }
    public static class PhoneNotFoundException extends CustomException {
    }
    public static class JobNotFoundException extends CustomException {
    }
    public static class ServerNotFoundException extends CustomException {
    }
    public static class UserNotFoundException extends CustomException {
    }
    public static class DeliverJobFailedException extends CustomException {
    }

}
