package com.huawei.cloudapp.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Phone implements Parcelable {

    @SerializedName("phone_name")
    private String phoneName;
    @SerializedName("server_id")
    private String serverId;
    @SerializedName("phone_id")
    private String phoneId;
    @SerializedName("phone_model_name")
    private String phoneModelName;
    @SerializedName("image_version")
    private String imageVersion;
    @SerializedName("image_id")
    private String imageId;
    @SerializedName("vnc_enable")
    private String vncEnable;
    @SerializedName("status")
    private int status;
    @SerializedName("type")
    private int type;
    @SerializedName("imei")
    private String imei;
    @SerializedName("availability_zone")
    private String availabilityZone;
    @SerializedName("traffic_type")
    private String trafficType;
    @SerializedName("volume_mode")
    private int volumeMode;
    @SerializedName("metadata")
    private Metadata metadata = new Metadata();
    @SerializedName("create_time")
    private String createTime;
    @SerializedName("update_time")
    private String updateTime;

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public String getPhoneModelName() {
        return phoneModelName;
    }

    public void setPhoneModelName(String phoneModelName) {
        this.phoneModelName = phoneModelName;
    }

    public String getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(String imageVersion) {
        this.imageVersion = imageVersion;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getVncEnable() {
        return vncEnable;
    }

    public void setVncEnable(String vncEnable) {
        this.vncEnable = vncEnable;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAvailabilityZone() {
        return availabilityZone;
    }

    public void setAvailabilityZone(String availabilityZone) {
        this.availabilityZone = availabilityZone;
    }

    public String getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(String trafficType) {
        this.trafficType = trafficType;
    }

    public int getVolumeMode() {
        return volumeMode;
    }

    public void setVolumeMode(int volumeMode) {
        this.volumeMode = volumeMode;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public static class Metadata implements Parcelable {
        @SerializedName("order_id")
        private String orderId;
        @SerializedName("product_id")
        private String productId;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.orderId);
            dest.writeString(this.productId);
        }

        public void readFromParcel(Parcel source) {
            this.orderId = source.readString();
            this.productId = source.readString();
        }

        public Metadata() {
        }

        protected Metadata(Parcel in) {
            this.orderId = in.readString();
            this.productId = in.readString();
        }

        public static final Parcelable.Creator<Metadata> CREATOR = new Parcelable.Creator<Metadata>() {
            @Override
            public Metadata createFromParcel(Parcel source) {
                return new Metadata(source);
            }

            @Override
            public Metadata[] newArray(int size) {
                return new Metadata[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phoneName);
        dest.writeString(this.serverId);
        dest.writeString(this.phoneId);
        dest.writeString(this.phoneModelName);
        dest.writeString(this.imageVersion);
        dest.writeString(this.imageId);
        dest.writeString(this.vncEnable);
        dest.writeInt(this.status);
        dest.writeInt(this.type);
        dest.writeString(this.imei);
        dest.writeString(this.availabilityZone);
        dest.writeString(this.trafficType);
        dest.writeInt(this.volumeMode);
        dest.writeParcelable(this.metadata, flags);
        dest.writeString(this.createTime);
        dest.writeString(this.updateTime);
    }

    public void readFromParcel(Parcel source) {
        this.phoneName = source.readString();
        this.serverId = source.readString();
        this.phoneId = source.readString();
        this.phoneModelName = source.readString();
        this.imageVersion = source.readString();
        this.imageId = source.readString();
        this.vncEnable = source.readString();
        this.status = source.readInt();
        this.type = source.readInt();
        this.imei = source.readString();
        this.availabilityZone = source.readString();
        this.trafficType = source.readString();
        this.volumeMode = source.readInt();
        this.metadata = source.readParcelable(Metadata.class.getClassLoader());
        this.createTime = source.readString();
        this.updateTime = source.readString();
    }

    public Phone() {
    }

    public Phone(String phoneName, String phoneId) {
        this.phoneName = phoneName;
        this.serverId = null;
        this.phoneId = phoneId;
        this.phoneModelName = null;
        this.imageVersion = null;
        this.imageId = null;
        this.vncEnable = null;
        this.status = 0;
        this.type = 0;
        this.imei = null;
        this.availabilityZone = null;
        this.trafficType = null;
        this.volumeMode = 0;
        this.metadata = null;
        this.createTime = null;
        this.updateTime = null;
    }

    protected Phone(Parcel in) {
        this.phoneName = in.readString();
        this.serverId = in.readString();
        this.phoneId = in.readString();
        this.phoneModelName = in.readString();
        this.imageVersion = in.readString();
        this.imageId = in.readString();
        this.vncEnable = in.readString();
        this.status = in.readInt();
        this.type = in.readInt();
        this.imei = in.readString();
        this.availabilityZone = in.readString();
        this.trafficType = in.readString();
        this.volumeMode = in.readInt();
        this.metadata = in.readParcelable(Metadata.class.getClassLoader());
        this.createTime = in.readString();
        this.updateTime = in.readString();
    }

    public static final Parcelable.Creator<Phone> CREATOR = new Parcelable.Creator<Phone>() {
        @Override
        public Phone createFromParcel(Parcel source) {
            return new Phone(source);
        }

        @Override
        public Phone[] newArray(int size) {
            return new Phone[size];
        }
    };
}
