/*
 * Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.model.bean;

import android.widget.TextView;

import androidx.annotation.NonNull;

import com.huawei.cloudapp.R;

public class PhoneInfo {

    private Phone mPhone;
    private String mPhoneInfo;
    private String mPhoneRegion;
    private int itemPosition = 0;
    private boolean checked = false;

    public PhoneInfo(String name, String info, String id, String region) {
        mPhone = new Phone(name, id);
        mPhoneInfo = info;
        mPhoneRegion = region;
    }

    public PhoneInfo(Phone phone, String info, String region) {
        mPhone = phone;
        mPhoneInfo = info;
        mPhoneRegion = region;
    }

    public Phone getPhone() {
        return mPhone;
    }

    public void setPhone(Phone phone) {
        mPhone = phone;
    }

    public String getInfo() {
        return mPhoneInfo;
    }

    public void setInfo(String info) {
        mPhoneInfo = info;
    }

    public void setPhoneName(String mPhoneName) {
        this.mPhone.setPhoneName(mPhoneName);
    }
    public String getName() {
        return mPhone.getPhoneName();
    }

    public String getPhoneId() {
        return mPhone.getPhoneId();
    }

    public void setPhoneId(String mPhoneId) {
        this.mPhone.setPhoneId(mPhoneId);
    }

    public String getRegion() {
        return mPhoneRegion;
    }

    public void setRegion(String mPhoneRegion) {
        this.mPhoneRegion = mPhoneRegion;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
