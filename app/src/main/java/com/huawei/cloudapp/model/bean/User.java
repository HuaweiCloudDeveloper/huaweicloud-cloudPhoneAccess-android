/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huawei.cloudapp.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
public class User implements Parcelable {
    public static final String BUNDLE_KEY = "CasUserInfo_Key";

    private String mUsername;
    private String mIamUsername;
    private HashMap<String, String> mUserToken;
    private HashMap<String, String> mUserTokenExpireTime;
    private HashMap<String, String> mUserProjectId;
    private String mUserSessionId;

    public User(String username, String iamUsername,
                HashMap<String, String> userToken,
                HashMap<String, String> userTokenExpireTime,
                HashMap<String, String> userProjectId,
                String userSessionId) {
        mUsername = username;
        mIamUsername = iamUsername;

        if (userToken != null) {
            mUserToken = userToken;
        } else {
            mUserToken = new HashMap<>();
        }

        if (userTokenExpireTime != null) {
            mUserTokenExpireTime = userTokenExpireTime;
        } else {
            mUserTokenExpireTime = new HashMap<>();
        }

        if (userProjectId != null) {
            mUserProjectId = userProjectId;
        } else {
            mUserProjectId = new HashMap<>();
        }
        mUserSessionId = userSessionId;
    }


    public String getUsername() {
        return mUsername;
    }

    public String getIamUsername() {
        return mIamUsername;
    }

    public void setUserToken(HashMap<String, String> mUserToken) {
        this.mUserToken = mUserToken;
    }

    public void setUserTokenExpireTime(HashMap<String, String> mUserTokenExpireTime) {
        this.mUserTokenExpireTime = mUserTokenExpireTime;
    }

    public void setUserSessionId(String mUserSessionId) {
        this.mUserSessionId = mUserSessionId;
    }

    public String getUserTokenByRegion(String region) {
        return mUserToken.get(region);
    }

    public String getUserTokenExpireTimeByRegion(String region) {
        return mUserTokenExpireTime.get(region);
    }

    public HashMap<String, String> getUserToken() {
        return mUserToken;
    }

    public HashMap<String, String> getUserTokenExpireTime() {
        return mUserTokenExpireTime;
    }

    public String getUserSessionId() {
        return mUserSessionId;
    }

    public HashMap<String, String> getUserProjectId() {
        return mUserProjectId;
    }

    public void setUserProjectId(HashMap<String, String> mUserProjectId) {
        this.mUserProjectId = mUserProjectId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mUsername);
        dest.writeString(this.mIamUsername);
        dest.writeSerializable(this.mUserToken);
        dest.writeSerializable(this.mUserTokenExpireTime);
        dest.writeSerializable(this.mUserProjectId);
        dest.writeString(this.mUserSessionId);
    }

    public void readFromParcel(Parcel source) {
        this.mUsername = source.readString();
        this.mIamUsername = source.readString();
        this.mUserToken = (HashMap<String, String>) source.readSerializable();
        this.mUserTokenExpireTime = (HashMap<String, String>) source.readSerializable();
        this.mUserProjectId = (HashMap<String, String>) source.readSerializable();
        this.mUserSessionId = source.readString();
    }

    protected User(Parcel in) {
        this.mUsername = in.readString();
        this.mIamUsername = in.readString();
        this.mUserToken = (HashMap<String, String>) in.readSerializable();
        this.mUserTokenExpireTime = (HashMap<String, String>) in.readSerializable();
        this.mUserProjectId = (HashMap<String, String>) in.readSerializable();
        this.mUserSessionId = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
