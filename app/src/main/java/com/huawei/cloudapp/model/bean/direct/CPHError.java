package com.huawei.cloudapp.model.bean.direct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CPHError implements Parcelable {

    @SerializedName("error_msg")
    private String errorMsg;
    @SerializedName("error_code")
    private String errorCode;
    @SerializedName("request_id")
    private String requestId;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.errorMsg);
        dest.writeString(this.errorCode);
        dest.writeString(this.requestId);
    }

    public void readFromParcel(Parcel source) {
        this.errorMsg = source.readString();
        this.errorCode = source.readString();
        this.requestId = source.readString();
    }

    public CPHError() {
    }

    protected CPHError(Parcel in) {
        this.errorMsg = in.readString();
        this.errorCode = in.readString();
        this.requestId = in.readString();
    }

    public static final Parcelable.Creator<CPHError> CREATOR = new Parcelable.Creator<CPHError>() {
        @Override
        public CPHError createFromParcel(Parcel source) {
            return new CPHError(source);
        }

        @Override
        public CPHError[] newArray(int size) {
            return new CPHError[size];
        }
    };
}
