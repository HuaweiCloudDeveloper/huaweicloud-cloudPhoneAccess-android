package com.huawei.cloudapp.model.bean.direct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConnectInfos implements Parcelable {

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("connect_infos")
    private List<ConnectInfo> connectInfos;
    @SerializedName("errors")
    private List<Errors> errors;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public List<ConnectInfo> getConnectInfos() {
        return connectInfos;
    }

    public void setConnectInfos(List<ConnectInfo> connectInfos) {
        this.connectInfos = connectInfos;
    }

    public List<Errors> getErrors() {
        return errors;
    }

    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    public static class ConnectInfo implements Parcelable {
        @SerializedName("phone_id")
        private String phoneId;
        @SerializedName("udid")
        private String udid;
        @SerializedName("access_info")
        public AccessInfo accessInfo;

        public String getPhoneId() {
            return phoneId;
        }

        public String getUdid() { return udid; }

        public void setPhoneId(String phoneId) {
            this.phoneId = phoneId;
        }

        public AccessInfo getAccessInfo() {
            return accessInfo;
        }

        public void setAccessInfo(AccessInfo accessInfo) {
            this.accessInfo = accessInfo;
        }

        public static class AccessInfo implements Parcelable {
            @SerializedName("access_ip")
            private String accessIp;
            @SerializedName("intranet_ip")
            private String intranetIp;
            @SerializedName("access_ipv6")
            private String accessIpv6;
            @SerializedName("access_port")
            private int accessPort;
            @SerializedName("session_id")
            private String sessionId;
            @SerializedName("timestamp")
            private String timestamp;
            @SerializedName("ticket")
            private String ticket;

            public String getAccessIp() {
                return accessIp;
            }

            public void setAccessIp(String accessIp) {
                this.accessIp = accessIp;
            }

            public String getIntranetIp() {
                return intranetIp;
            }

            public void setIntranetIp(String intranetIp) {
                this.intranetIp = intranetIp;
            }

            public String getAccessIpv6() {
                return accessIpv6;
            }

            public void setAccessIpv6(String accessIpv6) {
                this.accessIpv6 = accessIpv6;
            }

            public int getAccessPort() {
                return accessPort;
            }

            public void setAccessPort(int accessPort) {
                this.accessPort = accessPort;
            }

            public String getSessionId() {
                return sessionId;
            }

            public void setSessionId(String sessionId) {
                this.sessionId = sessionId;
            }

            public String getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(String timestamp) {
                this.timestamp = timestamp;
            }

            public String getTicket() {
                return ticket;
            }

            public void setTicket(String ticket) {
                this.ticket = ticket;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.accessIp);
                dest.writeString(this.intranetIp);
                dest.writeString(this.accessIpv6);
                dest.writeInt(this.accessPort);
                dest.writeString(this.sessionId);
                dest.writeString(this.timestamp);
                dest.writeString(this.ticket);
            }

            public void readFromParcel(Parcel source) {
                this.accessIp = source.readString();
                this.intranetIp = source.readString();
                this.accessIpv6 = source.readString();
                this.accessPort = source.readInt();
                this.sessionId = source.readString();
                this.timestamp = source.readString();
                this.ticket = source.readString();
            }

            public AccessInfo() {
            }

            protected AccessInfo(Parcel in) {
                this.accessIp = in.readString();
                this.intranetIp = in.readString();
                this.accessIpv6 = in.readString();
                this.accessPort = in.readInt();
                this.sessionId = in.readString();
                this.timestamp = in.readString();
                this.ticket = in.readString();
            }

            public static final Parcelable.Creator<AccessInfo> CREATOR = new Parcelable.Creator<AccessInfo>() {
                @Override
                public AccessInfo createFromParcel(Parcel source) {
                    return new AccessInfo(source);
                }

                @Override
                public AccessInfo[] newArray(int size) {
                    return new AccessInfo[size];
                }
            };

            @Override
            public String toString() {
                return "AccessInfo{" +
                        "accessIp='" + accessIp + '\'' +
                        ", intranetIp='" + intranetIp + '\'' +
                        ", accessIpv6='" + accessIpv6 + '\'' +
                        ", accessPort=" + accessPort +
                        ", sessionId='" + sessionId + '\'' +
                        ", timestamp='" + timestamp + '\'' +
                        ", ticket='" + ticket + '\'' +
                        '}';
            }
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.phoneId);
            dest.writeParcelable(this.accessInfo, flags);
        }

        public void readFromParcel(Parcel source) {
            this.phoneId = source.readString();
            this.accessInfo = source.readParcelable(AccessInfo.class.getClassLoader());
        }

        public ConnectInfo() {
        }

        protected ConnectInfo(Parcel in) {
            this.phoneId = in.readString();
            this.accessInfo = in.readParcelable(AccessInfo.class.getClassLoader());
        }

        public static final Parcelable.Creator<ConnectInfo> CREATOR = new Parcelable.Creator<ConnectInfo>() {
            @Override
            public ConnectInfo createFromParcel(Parcel source) {
                return new ConnectInfo(source);
            }

            @Override
            public ConnectInfo[] newArray(int size) {
                return new ConnectInfo[size];
            }
        };
    }

    public static class Errors implements Parcelable {
        @SerializedName("phone_id")
        private String phoneId;
        @SerializedName("error_code")
        private String errorCode;
        @SerializedName("error_msg")
        private String errorMsg;

        public String getPhoneId() {
            return phoneId;
        }

        public void setPhoneId(String phoneId) {
            this.phoneId = phoneId;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.phoneId);
            dest.writeString(this.errorCode);
            dest.writeString(this.errorMsg);
        }

        public void readFromParcel(Parcel source) {
            this.phoneId = source.readString();
            this.errorCode = source.readString();
            this.errorMsg = source.readString();
        }

        public Errors() {
        }

        protected Errors(Parcel in) {
            this.phoneId = in.readString();
            this.errorCode = in.readString();
            this.errorMsg = in.readString();
        }

        public static final Parcelable.Creator<Errors> CREATOR = new Parcelable.Creator<Errors>() {
            @Override
            public Errors createFromParcel(Parcel source) {
                return new Errors(source);
            }

            @Override
            public Errors[] newArray(int size) {
                return new Errors[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.requestId);
        dest.writeTypedList(this.connectInfos);
        dest.writeTypedList(this.errors);
    }

    public void readFromParcel(Parcel source) {
        this.requestId = source.readString();
        this.connectInfos = source.createTypedArrayList(ConnectInfos.ConnectInfo.CREATOR);
        this.errors = source.createTypedArrayList(Errors.CREATOR);
    }

    public ConnectInfos() {
    }

    protected ConnectInfos(Parcel in) {
        this.requestId = in.readString();
        this.connectInfos = in.createTypedArrayList(ConnectInfos.ConnectInfo.CREATOR);
        this.errors = in.createTypedArrayList(Errors.CREATOR);
    }

    public static final Parcelable.Creator<ConnectInfos> CREATOR = new Parcelable.Creator<ConnectInfos>() {
        @Override
        public ConnectInfos createFromParcel(Parcel source) {
            return new ConnectInfos(source);
        }

        @Override
        public ConnectInfos[] newArray(int size) {
            return new ConnectInfos[size];
        }
    };
}
