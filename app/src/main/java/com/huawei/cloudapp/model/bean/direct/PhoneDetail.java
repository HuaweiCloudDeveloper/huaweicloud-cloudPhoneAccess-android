package com.huawei.cloudapp.model.bean.direct;

import com.google.gson.annotations.SerializedName;
import com.huawei.cloudapp.model.bean.Phone;

import java.util.List;

public class PhoneDetail {
    @SerializedName("request_id")
    private String requestId;
    @SerializedName("phone_name")
    private String phoneName;
    @SerializedName("server_id")
    private String serverId;
    @SerializedName("phone_id")
    private String phoneId;
    @SerializedName("image_id")
    private String imageId;
    @SerializedName("phone_model_name")
    private String phoneModelName;
    @SerializedName("image_version")
    private String imageVersion;
    @SerializedName("status")
    private int status;
    @SerializedName("imei")
    private long imei;
    @SerializedName("availability_zone")
    private String availabilityZone;
    @SerializedName("traffic_type")
    private String trafficType;
    @SerializedName("volume_mode")
    private int volumeMode;
    @SerializedName("phone_data_volume")
    private PhoneDataVolume phoneDataVolume;
    @SerializedName("property")
    private String property;
    @SerializedName("metadata")
    private Phone.Metadata metadata;
    @SerializedName("create_time")
    private String createTime;
    @SerializedName("update_time")
    private String updateTime;
    @SerializedName("access_infos")
    private List<AccessInfo> accessInfos;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String phoneId) {
        this.phoneId = phoneId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getPhoneModelName() {
        return phoneModelName;
    }

    public void setPhoneModelName(String phoneModelName) {
        this.phoneModelName = phoneModelName;
    }

    public String getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(String imageVersion) {
        this.imageVersion = imageVersion;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }

    public String getAvailabilityZone() {
        return availabilityZone;
    }

    public void setAvailabilityZone(String availabilityZone) {
        this.availabilityZone = availabilityZone;
    }

    public String getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(String trafficType) {
        this.trafficType = trafficType;
    }

    public int getVolumeMode() {
        return volumeMode;
    }

    public void setVolumeMode(int volumeMode) {
        this.volumeMode = volumeMode;
    }

    public PhoneDataVolume getPhoneDataVolume() {
        return phoneDataVolume;
    }

    public void setPhoneDataVolume(PhoneDataVolume phoneDataVolume) {
        this.phoneDataVolume = phoneDataVolume;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Phone.Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Phone.Metadata metadata) {
        this.metadata = metadata;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public List<AccessInfo> getAccessInfos() {
        return accessInfos;
    }

    public void setAccessInfos(List<AccessInfo> accessInfos) {
        this.accessInfos = accessInfos;
    }

    public static class PhoneDataVolume {
        @SerializedName("volume_type")
        private String volumeType;
        @SerializedName("volume_size")
        private int volumeSize;

        public String getVolumeType() {
            return volumeType;
        }

        public void setVolumeType(String volumeType) {
            this.volumeType = volumeType;
        }

        public int getVolumeSize() {
            return volumeSize;
        }

        public void setVolumeSize(int volumeSize) {
            this.volumeSize = volumeSize;
        }
    }

    public static class AccessInfo {
        @SerializedName("type")
        private String type;
        @SerializedName("device_ip")
        private String deviceIp;
        @SerializedName("phone_ip")
        private String phoneIp;
        @SerializedName("listen_port")
        private int listenPort;
        @SerializedName("access_ip")
        private String accessIp;
        @SerializedName("public_ip")
        private String publicIp;
        @SerializedName("intranet_ip")
        private String intranetIp;
        @SerializedName("server_ip")
        private String serverIp;
        @SerializedName("access_port")
        private int accessPort;
        @SerializedName("phone_ipv6")
        private String phoneIpv6;
        @SerializedName("server_ipv6")
        private String serverIpv6;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDeviceIp() {
            return deviceIp;
        }

        public void setDeviceIp(String deviceIp) {
            this.deviceIp = deviceIp;
        }

        public String getPhoneIp() {
            return phoneIp;
        }

        public void setPhoneIp(String phoneIp) {
            this.phoneIp = phoneIp;
        }

        public int getListenPort() {
            return listenPort;
        }

        public void setListenPort(int listenPort) {
            this.listenPort = listenPort;
        }

        public String getAccessIp() {
            return accessIp;
        }

        public void setAccessIp(String accessIp) {
            this.accessIp = accessIp;
        }

        public String getPublicIp() {
            return publicIp;
        }

        public void setPublicIp(String publicIp) {
            this.publicIp = publicIp;
        }

        public String getIntranetIp() {
            return intranetIp;
        }

        public void setIntranetIp(String intranetIp) {
            this.intranetIp = intranetIp;
        }

        public String getServerIp() {
            return serverIp;
        }

        public void setServerIp(String serverIp) {
            this.serverIp = serverIp;
        }

        public int getAccessPort() {
            return accessPort;
        }

        public void setAccessPort(int accessPort) {
            this.accessPort = accessPort;
        }

        public String getPhoneIpv6() {
            return phoneIpv6;
        }

        public void setPhoneIpv6(String phoneIpv6) {
            this.phoneIpv6 = phoneIpv6;
        }

        public String getServerIpv6() {
            return serverIpv6;
        }

        public void setServerIpv6(String serverIpv6) {
            this.serverIpv6 = serverIpv6;
        }
    }
}
