package com.huawei.cloudapp.model.bean.direct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PhoneJobRequest implements Parcelable {

    @SerializedName("image_id")
    private String imageId;
    @SerializedName("phones")
    private List<PhoneProperty> phones = new ArrayList<>();

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public List<PhoneProperty> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneProperty> phones) {
        this.phones = phones;
    }

    public static class PhoneProperty implements Parcelable {
        @SerializedName("phone_id")
        private String mPhoneId;
        @SerializedName("property")
        private String mProperty;

        public PhoneProperty(String phoneId, String property) {
            mPhoneId = phoneId;
            mProperty = property;
        }

        public String getPhoneId() {
            return mPhoneId;
        }

        public void setPhoneId(String mPhoneId) {
            this.mPhoneId = mPhoneId;
        }

        public String getProperty() {
            return mProperty;
        }

        public void setProperty(String mProperty) {
            this.mProperty = mProperty;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.mPhoneId);
            dest.writeString(this.mProperty);
        }

        public void readFromParcel(Parcel source) {
            this.mPhoneId = source.readString();
            this.mProperty = source.readString();
        }

        protected PhoneProperty(Parcel in) {
            this.mPhoneId = in.readString();
            this.mProperty = in.readString();
        }

        public static final Parcelable.Creator<PhoneProperty> CREATOR = new Parcelable.Creator<PhoneProperty>() {
            @Override
            public PhoneProperty createFromParcel(Parcel source) {
                return new PhoneProperty(source);
            }

            @Override
            public PhoneProperty[] newArray(int size) {
                return new PhoneProperty[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageId);
        dest.writeTypedList(this.phones);
    }

    public void readFromParcel(Parcel source) {
        this.imageId = source.readString();
        this.phones = source.createTypedArrayList(PhoneProperty.CREATOR);
    }

    public PhoneJobRequest() {
    }

    protected PhoneJobRequest(Parcel in) {
        this.imageId = in.readString();
        this.phones = in.createTypedArrayList(PhoneProperty.CREATOR);
    }

    public static final Parcelable.Creator<PhoneJobRequest> CREATOR = new Parcelable.Creator<PhoneJobRequest>() {
        @Override
        public PhoneJobRequest createFromParcel(Parcel source) {
            return new PhoneJobRequest(source);
        }

        @Override
        public PhoneJobRequest[] newArray(int size) {
            return new PhoneJobRequest[size];
        }
    };
}
