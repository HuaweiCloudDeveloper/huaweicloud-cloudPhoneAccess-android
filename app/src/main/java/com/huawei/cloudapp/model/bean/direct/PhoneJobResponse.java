package com.huawei.cloudapp.model.bean.direct;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhoneJobResponse {


    @SerializedName("request_id")
    private String requestId;
    @SerializedName("jobs")
    private List<Jobs> jobs;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public List<Jobs> getJobs() {
        return jobs;
    }

    public void setJobs(List<Jobs> jobs) {
        this.jobs = jobs;
    }

    public static class Jobs {
        @SerializedName("phone_id")
        private String phoneId;
        @SerializedName("job_id")
        private String jobId;
        @SerializedName("error_code")
        private String errorCode;
        @SerializedName("error_msg")
        private String errorMsg;

        public String getPhoneId() {
            return phoneId;
        }

        public void setPhoneId(String phoneId) {
            this.phoneId = phoneId;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }
    }
}
