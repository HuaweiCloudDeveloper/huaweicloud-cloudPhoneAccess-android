package com.huawei.cloudapp.model.bean.direct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.huawei.cloudapp.model.bean.Phone;

import java.util.ArrayList;
import java.util.List;

public class PhoneList implements Parcelable {

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("count")
    private int count;
    @SerializedName("phones")
    private List<Phone> phones = new ArrayList<>();

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.requestId);
        dest.writeInt(this.count);
        dest.writeTypedList(this.phones);
    }

    public void readFromParcel(Parcel source) {
        this.requestId = source.readString();
        this.count = source.readInt();
        this.phones = source.createTypedArrayList(Phone.CREATOR);
    }

    public PhoneList() {
    }

    protected PhoneList(Parcel in) {
        this.requestId = in.readString();
        this.count = in.readInt();
        this.phones = in.createTypedArrayList(Phone.CREATOR);
    }

    public static final Parcelable.Creator<PhoneList> CREATOR = new Parcelable.Creator<PhoneList>() {
        @Override
        public PhoneList createFromParcel(Parcel source) {
            return new PhoneList(source);
        }

        @Override
        public PhoneList[] newArray(int size) {
            return new PhoneList[size];
        }
    };
}
