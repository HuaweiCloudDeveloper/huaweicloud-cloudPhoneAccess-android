package com.huawei.cloudapp.model.bean.direct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ServerList {

    @SerializedName("request_id")
    private String requestId;
    @SerializedName("count")
    private int count;
    @SerializedName("servers")
    private List<Server> servers;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Server> getServers() {
        return servers;
    }

    public void setServers(List<Server> servers) {
        this.servers = servers;
    }

    public class Server implements Parcelable {

        public static final String BUNDLE_KEY = "Server_Key";

        @SerializedName("server_name")
        private String serverName;
        @SerializedName("server_id")
        private String serverId;
        @SerializedName("server_model_name")
        private String serverModelName;
        @SerializedName("phone_model_name")
        private String phoneModelName;
        @SerializedName("keypair_name")
        private String keypairName;
        @SerializedName("status")
        private int status;
        @SerializedName("vpc_id")
        private String vpcId;
        @SerializedName("cidr")
        private String cidr;
        @SerializedName("vpc_cidr")
        private String vpcCidr;
        @SerializedName("subnet_id")
        private String subnetId;
        @SerializedName("subnet_cidr")
        private String subnetCidr;
        @SerializedName("resource_project_id")
        private String resourceProjectId;
        @SerializedName("metadata")
        private Metadata metadata = new Metadata();
        @SerializedName("availability_zone")
        private String availabilityZone;
        @SerializedName("network_version")
        private String networkVersion;
        @SerializedName("create_time")
        private String createTime;
        @SerializedName("update_time")
        private String updateTime;
        @SerializedName("addresses")
        private List<Addresses> addresses;

        public String getServerName() {
            return serverName;
        }

        public void setServerName(String serverName) {
            this.serverName = serverName;
        }

        public String getServerId() {
            return serverId;
        }

        public void setServerId(String serverId) {
            this.serverId = serverId;
        }

        public String getServerModelName() {
            return serverModelName;
        }

        public void setServerModelName(String serverModelName) {
            this.serverModelName = serverModelName;
        }

        public String getPhoneModelName() {
            return phoneModelName;
        }

        public void setPhoneModelName(String phoneModelName) {
            this.phoneModelName = phoneModelName;
        }

        public String getKeypairName() {
            return keypairName;
        }

        public void setKeypairName(String keypairName) {
            this.keypairName = keypairName;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getVpcId() {
            return vpcId;
        }

        public void setVpcId(String vpcId) {
            this.vpcId = vpcId;
        }

        public String getCidr() {
            return cidr;
        }

        public void setCidr(String cidr) {
            this.cidr = cidr;
        }

        public String getVpcCidr() {
            return vpcCidr;
        }

        public void setVpcCidr(String vpcCidr) {
            this.vpcCidr = vpcCidr;
        }

        public String getSubnetId() {
            return subnetId;
        }

        public void setSubnetId(String subnetId) {
            this.subnetId = subnetId;
        }

        public String getSubnetCidr() {
            return subnetCidr;
        }

        public void setSubnetCidr(String subnetCidr) {
            this.subnetCidr = subnetCidr;
        }

        public String getResourceProjectId() {
            return resourceProjectId;
        }

        public void setResourceProjectId(String resourceProjectId) {
            this.resourceProjectId = resourceProjectId;
        }

        public Metadata getMetadata() {
            return metadata;
        }

        public void setMetadata(Metadata metadata) {
            this.metadata = metadata;
        }

        public String getAvailabilityZone() {
            return availabilityZone;
        }

        public void setAvailabilityZone(String availabilityZone) {
            this.availabilityZone = availabilityZone;
        }

        public String getNetworkVersion() {
            return networkVersion;
        }

        public void setNetworkVersion(String networkVersion) {
            this.networkVersion = networkVersion;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public List<Addresses> getAddresses() {
            return addresses;
        }

        public void setAddresses(List<Addresses> addresses) {
            this.addresses = addresses;
        }

        public class Metadata implements Parcelable {
            @SerializedName("product_id")
            private String productId;
            @SerializedName("order_id")
            private String orderId;

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.productId);
                dest.writeString(this.orderId);
            }

            public void readFromParcel(Parcel source) {
                this.productId = source.readString();
                this.orderId = source.readString();
            }

            public Metadata() {
            }

            protected Metadata(Parcel in) {
                this.productId = in.readString();
                this.orderId = in.readString();
            }

            public final Creator<Metadata> CREATOR = new Creator<Metadata>() {
                @Override
                public Metadata createFromParcel(Parcel source) {
                    return new Metadata(source);
                }

                @Override
                public Metadata[] newArray(int size) {
                    return new Metadata[size];
                }
            };
        }

        public class Addresses implements Parcelable {
            @SerializedName("intranet_ip")
            private String intranetIp;
            @SerializedName("server_ip")
            private String serverIp;
            @SerializedName("access_ip")
            private String accessIp;
            @SerializedName("public_ip")
            private String publicIp;

            public String getIntranetIp() {
                return intranetIp;
            }

            public void setIntranetIp(String intranetIp) {
                this.intranetIp = intranetIp;
            }

            public String getServerIp() {
                return serverIp;
            }

            public void setServerIp(String serverIp) {
                this.serverIp = serverIp;
            }

            public String getAccessIp() {
                return accessIp;
            }

            public void setAccessIp(String accessIp) {
                this.accessIp = accessIp;
            }

            public String getPublicIp() {
                return publicIp;
            }

            public void setPublicIp(String publicIp) {
                this.publicIp = publicIp;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.intranetIp);
                dest.writeString(this.serverIp);
                dest.writeString(this.accessIp);
                dest.writeString(this.publicIp);
            }

            public void readFromParcel(Parcel source) {
                this.intranetIp = source.readString();
                this.serverIp = source.readString();
                this.accessIp = source.readString();
                this.publicIp = source.readString();
            }

            public Addresses() {
            }

            protected Addresses(Parcel in) {
                this.intranetIp = in.readString();
                this.serverIp = in.readString();
                this.accessIp = in.readString();
                this.publicIp = in.readString();
            }

            public final Creator<Addresses> CREATOR = new Creator<Addresses>() {
                @Override
                public Addresses createFromParcel(Parcel source) {
                    return new Addresses(source);
                }

                @Override
                public Addresses[] newArray(int size) {
                    return new Addresses[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.serverName);
            dest.writeString(this.serverId);
            dest.writeString(this.serverModelName);
            dest.writeString(this.phoneModelName);
            dest.writeString(this.keypairName);
            dest.writeInt(this.status);
            dest.writeString(this.vpcId);
            dest.writeString(this.cidr);
            dest.writeString(this.vpcCidr);
            dest.writeString(this.subnetId);
            dest.writeString(this.subnetCidr);
            dest.writeString(this.resourceProjectId);
            dest.writeParcelable(this.metadata, flags);
            dest.writeString(this.availabilityZone);
            dest.writeString(this.networkVersion);
            dest.writeString(this.createTime);
            dest.writeString(this.updateTime);
            dest.writeList(this.addresses);
        }

        public void readFromParcel(Parcel source) {
            this.serverName = source.readString();
            this.serverId = source.readString();
            this.serverModelName = source.readString();
            this.phoneModelName = source.readString();
            this.keypairName = source.readString();
            this.status = source.readInt();
            this.vpcId = source.readString();
            this.cidr = source.readString();
            this.vpcCidr = source.readString();
            this.subnetId = source.readString();
            this.subnetCidr = source.readString();
            this.resourceProjectId = source.readString();
            this.metadata = source.readParcelable(Metadata.class.getClassLoader());
            this.availabilityZone = source.readString();
            this.networkVersion = source.readString();
            this.createTime = source.readString();
            this.updateTime = source.readString();
            this.addresses = new ArrayList<Addresses>();
            source.readList(this.addresses, Addresses.class.getClassLoader());
        }

        public Server() {
        }

        protected Server(Parcel in) {
            this.serverName = in.readString();
            this.serverId = in.readString();
            this.serverModelName = in.readString();
            this.phoneModelName = in.readString();
            this.keypairName = in.readString();
            this.status = in.readInt();
            this.vpcId = in.readString();
            this.cidr = in.readString();
            this.vpcCidr = in.readString();
            this.subnetId = in.readString();
            this.subnetCidr = in.readString();
            this.resourceProjectId = in.readString();
            this.metadata = in.readParcelable(Metadata.class.getClassLoader());
            this.availabilityZone = in.readString();
            this.networkVersion = in.readString();
            this.createTime = in.readString();
            this.updateTime = in.readString();
            this.addresses = new ArrayList<Addresses>();
            in.readList(this.addresses, Addresses.class.getClassLoader());
        }

        public final Parcelable.Creator<Server> CREATOR = new Parcelable.Creator<Server>() {
            @Override
            public Server createFromParcel(Parcel source) {
                return new Server(source);
            }

            @Override
            public Server[] newArray(int size) {
                return new Server[size];
            }
        };
    }

}
