package com.huawei.cloudapp.model.bean.direct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserToken implements Parcelable {

    @SerializedName("auth")
    private AuthDTO auth = new AuthDTO();

    public AuthDTO getAuth() {
        return auth;
    }

    public void setAuth(AuthDTO auth) {
        this.auth = auth;
    }

    public static class AuthDTO implements Parcelable {
        @SerializedName("identity")
        private IdentityDTO identity = new IdentityDTO();
        @SerializedName("scope")
        private ScopeDTO scope = new ScopeDTO();

        public IdentityDTO getIdentity() {
            return identity;
        }

        public void setIdentity(IdentityDTO identity) {
            this.identity = identity;
        }

        public ScopeDTO getScope() {
            return scope;
        }

        public void setScope(ScopeDTO scope) {
            this.scope = scope;
        }

        public static class IdentityDTO implements Parcelable {
            @SerializedName("password")
            private PasswordDTO password = new PasswordDTO();
            @SerializedName("methods")
            private List<String> methods = new ArrayList<>();

            public PasswordDTO getPassword() {
                return password;
            }

            public void setPassword(PasswordDTO password) {
                this.password = password;
            }

            public List<String> getMethods() {
                return methods;
            }

            public void setMethods(List<String> methods) {
                this.methods = methods;
            }

            public static class PasswordDTO implements Parcelable {
                @SerializedName("user")
                private UserDTO user = new UserDTO();

                public UserDTO getUser() {
                    return user;
                }

                public void setUser(UserDTO user) {
                    this.user = user;
                }

                public static class UserDTO implements Parcelable {
                    @SerializedName("domain")
                    private DomainDTO domain = new DomainDTO();
                    @SerializedName("name")
                    private String name;
                    @SerializedName("password")
                    private String password;

                    public DomainDTO getDomain() {
                        return domain;
                    }

                    public void setDomain(DomainDTO domain) {
                        this.domain = domain;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getPassword() {
                        return password;
                    }

                    public void setPassword(String password) {
                        this.password = password;
                    }

                    public static class DomainDTO implements Parcelable {
                        @SerializedName("name")
                        private String name;

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }


                        @Override
                        public int describeContents() {
                            return 0;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {
                            dest.writeString(this.name);
                        }

                        public void readFromParcel(Parcel source) {
                            this.name = source.readString();
                        }

                        public DomainDTO() {
                        }

                        protected DomainDTO(Parcel in) {
                            this.name = in.readString();
                        }

                        public static final Creator<DomainDTO> CREATOR = new Creator<DomainDTO>() {
                            @Override
                            public DomainDTO createFromParcel(Parcel source) {
                                return new DomainDTO(source);
                            }

                            @Override
                            public DomainDTO[] newArray(int size) {
                                return new DomainDTO[size];
                            }
                        };
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeParcelable(this.domain, flags);
                        dest.writeString(this.name);
                        dest.writeString(this.password);
                    }

                    public void readFromParcel(Parcel source) {
                        this.domain = source.readParcelable(DomainDTO.class.getClassLoader());
                        this.name = source.readString();
                        this.password = source.readString();
                    }

                    public UserDTO() {
                    }

                    protected UserDTO(Parcel in) {
                        this.domain = in.readParcelable(DomainDTO.class.getClassLoader());
                        this.name = in.readString();
                        this.password = in.readString();
                    }

                    public static final Parcelable.Creator<UserDTO> CREATOR = new Parcelable.Creator<UserDTO>() {
                        @Override
                        public UserDTO createFromParcel(Parcel source) {
                            return new UserDTO(source);
                        }

                        @Override
                        public UserDTO[] newArray(int size) {
                            return new UserDTO[size];
                        }
                    };
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeParcelable(this.user, flags);
                }

                public void readFromParcel(Parcel source) {
                    this.user = source.readParcelable(UserDTO.class.getClassLoader());
                }

                public PasswordDTO() {
                }

                protected PasswordDTO(Parcel in) {
                    this.user = in.readParcelable(UserDTO.class.getClassLoader());
                }

                public static final Creator<PasswordDTO> CREATOR = new Creator<PasswordDTO>() {
                    @Override
                    public PasswordDTO createFromParcel(Parcel source) {
                        return new PasswordDTO(source);
                    }

                    @Override
                    public PasswordDTO[] newArray(int size) {
                        return new PasswordDTO[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeParcelable(this.password, flags);
                dest.writeStringList(this.methods);
            }

            public void readFromParcel(Parcel source) {
                this.password = source.readParcelable(PasswordDTO.class.getClassLoader());
                this.methods = source.createStringArrayList();
            }

            public IdentityDTO() {
            }

            protected IdentityDTO(Parcel in) {
                this.password = in.readParcelable(PasswordDTO.class.getClassLoader());
                this.methods = in.createStringArrayList();
            }

            public static final Creator<IdentityDTO> CREATOR = new Creator<IdentityDTO>() {
                @Override
                public IdentityDTO createFromParcel(Parcel source) {
                    return new IdentityDTO(source);
                }

                @Override
                public IdentityDTO[] newArray(int size) {
                    return new IdentityDTO[size];
                }
            };
        }

        public static class ScopeDTO implements Parcelable {
            @SerializedName("project")
            private ProjectDTO project = new ProjectDTO();

            @SerializedName("domain")
            private IdentityDTO.PasswordDTO.UserDTO.DomainDTO domain = new IdentityDTO.PasswordDTO.UserDTO.DomainDTO();

            public ProjectDTO getProject() {
                return project;
            }

            public void setProject(ProjectDTO project) {
                this.project = project;
            }

            public IdentityDTO.PasswordDTO.UserDTO.DomainDTO getDomain() {
                return domain;
            }

            public void setDomain(IdentityDTO.PasswordDTO.UserDTO.DomainDTO domain) {
                this.domain = domain;
            }

            public static class ProjectDTO implements Parcelable {
                @SerializedName("name")
                private String name;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }


                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.name);
                }

                public void readFromParcel(Parcel source) {
                    this.name = source.readString();
                }

                public ProjectDTO() {
                }

                protected ProjectDTO(Parcel in) {
                    this.name = in.readString();
                }

                public static final Parcelable.Creator<ProjectDTO> CREATOR = new Parcelable.Creator<ProjectDTO>() {
                    @Override
                    public ProjectDTO createFromParcel(Parcel source) {
                        return new ProjectDTO(source);
                    }

                    @Override
                    public ProjectDTO[] newArray(int size) {
                        return new ProjectDTO[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeParcelable(this.project, flags);
                dest.writeParcelable(this.domain, flags);
            }

            public void readFromParcel(Parcel source) {
                this.project = source.readParcelable(ProjectDTO.class.getClassLoader());
                this.domain = source.readParcelable(IdentityDTO.PasswordDTO.UserDTO.DomainDTO.class.getClassLoader());
            }

            public ScopeDTO() {
            }

            protected ScopeDTO(Parcel in) {
                this.project = in.readParcelable(ProjectDTO.class.getClassLoader());
                this.domain = in.readParcelable(IdentityDTO.PasswordDTO.UserDTO.DomainDTO.class.getClassLoader());
            }

            public static final Creator<ScopeDTO> CREATOR = new Creator<ScopeDTO>() {
                @Override
                public ScopeDTO createFromParcel(Parcel source) {
                    return new ScopeDTO(source);
                }

                @Override
                public ScopeDTO[] newArray(int size) {
                    return new ScopeDTO[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.identity, flags);
            dest.writeParcelable(this.scope, flags);
        }

        public void readFromParcel(Parcel source) {
            this.identity = source.readParcelable(IdentityDTO.class.getClassLoader());
            this.scope = source.readParcelable(ScopeDTO.class.getClassLoader());
        }

        public AuthDTO() {
        }

        protected AuthDTO(Parcel in) {
            this.identity = in.readParcelable(IdentityDTO.class.getClassLoader());
            this.scope = in.readParcelable(ScopeDTO.class.getClassLoader());
        }

        public static final Creator<AuthDTO> CREATOR = new Creator<AuthDTO>() {
            @Override
            public AuthDTO createFromParcel(Parcel source) {
                return new AuthDTO(source);
            }

            @Override
            public AuthDTO[] newArray(int size) {
                return new AuthDTO[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.auth, flags);
    }

    public void readFromParcel(Parcel source) {
        this.auth = source.readParcelable(AuthDTO.class.getClassLoader());
    }

    public UserToken() {
    }

    protected UserToken(Parcel in) {
        this.auth = in.readParcelable(AuthDTO.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserToken> CREATOR = new Parcelable.Creator<UserToken>() {
        @Override
        public UserToken createFromParcel(Parcel source) {
            return new UserToken(source);
        }

        @Override
        public UserToken[] newArray(int size) {
            return new UserToken[size];
        }
    };
}
