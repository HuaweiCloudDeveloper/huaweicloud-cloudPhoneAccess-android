/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.model.bean.management;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

import static com.huawei.cloudapp.utils.CasConstantsUtil.AES_KEY;
import static com.huawei.cloudapp.utils.CasConstantsUtil.AES_KEY_PATTERN;
import static com.huawei.cloudapp.utils.CasConstantsUtil.AUTH_TS;
import static com.huawei.cloudapp.utils.CasConstantsUtil.AVAILABLE_PLAYTIME;
import static com.huawei.cloudapp.utils.CasConstantsUtil.BACKGROUND_TIMEOUT;
import static com.huawei.cloudapp.utils.CasConstantsUtil.IP;
import static com.huawei.cloudapp.utils.CasConstantsUtil.IP_PATTERN;
import static com.huawei.cloudapp.utils.CasConstantsUtil.PORT;
import static com.huawei.cloudapp.utils.CasConstantsUtil.POSITIVE_NUMBER_PATTERN;
import static com.huawei.cloudapp.utils.CasConstantsUtil.SESSION_ID;
import static com.huawei.cloudapp.utils.CasConstantsUtil.TICKET;
import static com.huawei.cloudapp.utils.CasConstantsUtil.TOUCH_TIMEOUT;
import static com.huawei.cloudapp.utils.CasConstantsUtil.USER_ID;

import com.huawei.cloudapp.common.CASLog;

public class CasConnectInfo implements Parcelable {

    public static final String BUNDLE_KEY = "CasConnectorInfo_Key";

    /**
     * 构造器
     */
    public static final Parcelable.Creator<CasConnectInfo> CREATOR = new Parcelable.Creator<CasConnectInfo>() {
        @Override
        public CasConnectInfo createFromParcel(Parcel in) {
            return new CasConnectInfo(in);
        }

        @Override
        public CasConnectInfo[] newArray(int size) {
            return new CasConnectInfo[size];
        }
    };

    /**
     * Bundle Key值
     */
    private static final String TAG = "CasConnectInfo";

    /**
     * 连接ip
     */
    private String connectIp;

    /**
     * 连接端口
     */
    private String connectPort;

    /**
     * 连接sessionId
     */
    private String sessionId;

    /**
     * ticket信息(接入加密信息)
     */
    private String ticket;

    /**
     * 超时时长（home时长）, 单位秒级
     */
    private String backgroundTimeout;

    /**
     * 可玩时长，单位秒级
     */
    private String availablePlayTime;

    /**
     * 无触控时长，单位秒级
     */
    public String touchTimeout;

    /**
     * 验签时间戳
     */
    private String authTs;

    /**
     * 客户后台生成的对称秘钥
     */
    private String aesKey;

    /**
     * 用户id
     */
    private String userID;

    /**
     * 默认构造函数
     */
    public CasConnectInfo() {
    }

    protected CasConnectInfo(Parcel in) {
        connectIp = in.readString();
        connectPort = in.readString();
        sessionId = in.readString();
        ticket = in.readString();
        backgroundTimeout = in.readString();
        availablePlayTime = in.readString();
        authTs = in.readString();
        aesKey = in.readString();
        userID = in.readString();
        touchTimeout = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(connectIp);
        parcel.writeString(connectPort);
        parcel.writeString(sessionId);
        parcel.writeString(ticket);
        parcel.writeString(backgroundTimeout);
        parcel.writeString(availablePlayTime);
        parcel.writeString(authTs);
        parcel.writeString(aesKey);
        parcel.writeString(userID);
        parcel.writeString(touchTimeout);
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getConnectIp() {
        return connectIp;
    }

    public void setConnectIp(String connectIp) {
        this.connectIp = connectIp;
    }

    public String getConnectPort() {
        return connectPort;
    }

    public void setConnectPort(String connectPort) {
        this.connectPort = connectPort;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAvailablePlayTime() {
        return availablePlayTime;
    }

    public void setAvailablePlayTime(String availablePlayTime) {
        this.availablePlayTime = availablePlayTime;
    }

    public String getBackgroundTimeout() {
        return backgroundTimeout;
    }

    public void setBackgroundTimeout(String backgroundTimeout) {
        this.backgroundTimeout = backgroundTimeout;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public String getAuthTs() {
        return authTs;
    }

    public void setAuthTs(String authTs) {
        this.authTs = authTs;
    }

    public String getUserId() {
        return userID;
    }

    public void setUserId(String userId) {
        userID = userId;
    }

    public String getTouchTimeout() {
        return touchTimeout;
    }

    public void setTouchTimeout(String timeout) {
        touchTimeout = timeout;
    }

    /**
     * check start params
     * <p>check params</p>
     */
    public boolean initConnectorParams(Map<String, String> params) {
        String ip = params.get(IP);
        if (ip == null || !IP_PATTERN.matcher(ip).matches()) {
            CASLog.i(TAG, "Parameter: ip is invalid.");
            return false;
        }

        String port = params.get(PORT);
        if (port == null || !POSITIVE_NUMBER_PATTERN.matcher(port).matches()) {
            CASLog.i(TAG, "Parameter: port is invalid.");
            return false;
        }

        String sessionId = params.get(SESSION_ID);
        if (sessionId == null) {
            CASLog.i(TAG, "Parameter: session_id is invalid.");
            return false;
        }

        String ticket = params.get(TICKET);
        if (ticket == null) {
            CASLog.i(TAG, "Parameter: ticket is invalid.");
            return false;
        }

        String aesKey = params.get(AES_KEY);
        if (aesKey == null || !AES_KEY_PATTERN.matcher(aesKey).matches()) {
            CASLog.i(TAG, "Parameter: aes_key is invalid.");
            return false;
        }

        String authTs = params.get(AUTH_TS);
        if (authTs == null) {
            CASLog.i(TAG, "Parameter: auth_ts is invalid.");
            return false;
        }

        String backgroundTimeout = params.get(BACKGROUND_TIMEOUT);
        if (backgroundTimeout == null || backgroundTimeout.length() == 0) {
            backgroundTimeout = "60";
        } else {
            try {
                int timeOut = Integer.parseInt(backgroundTimeout);
                if (timeOut < 15 || timeOut > 3600) {
                    backgroundTimeout = "60";
                }
            } catch (NumberFormatException e) {
                CASLog.e(TAG, "Parameter: background_timeout is invalid.");
                return false;
            }
        }

        String availablePlaytime = params.get(AVAILABLE_PLAYTIME);
        if (availablePlaytime == null) {
            availablePlaytime = "0";
        }

        String userId = params.get(USER_ID);
        if (userId == null) {
            userId = "";
            CASLog.i(TAG, "Parameter: userId is null.");
        }

        String touchTimeOut = params.get(TOUCH_TIMEOUT);
        if (touchTimeOut == null || touchTimeOut.length() == 0) {
            touchTimeOut = "0";
        } else {
            try {
                int timeOut = Integer.parseInt(touchTimeOut);
                if (timeOut < 0) {
                    touchTimeOut = "0";
                }
            } catch (NumberFormatException e) {
                CASLog.e(TAG, "Parameter: touch_timeout is invalid.");
                return false;
            }
        }
        setConnectIp(ip);
        setConnectPort(port);
        setBackgroundTimeout(backgroundTimeout);
        setAvailablePlayTime(availablePlaytime);
        setTicket(ticket);
        setSessionId(sessionId);
        setAuthTs(authTs);
        setAesKey(aesKey);
        setUserId(userId);
        setTouchTimeout(touchTimeOut);
        return true;
    }
}
