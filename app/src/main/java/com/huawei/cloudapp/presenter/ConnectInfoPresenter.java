package com.huawei.cloudapp.presenter;

import com.huawei.cloudapp.model.IConnectInfoModel;
import com.huawei.cloudapp.model.IHandleData;
import com.huawei.cloudapp.model.OnRequestListener;
import com.huawei.cloudapp.model.bean.User;
import com.huawei.cloudapp.model.bean.management.CasConnectInfo;

import java.util.HashMap;
import java.util.List;

public class ConnectInfoPresenter {
    private final IConnectInfoModel mConnectInfoModel;
    private final IHandleData mIHandleData;

    public ConnectInfoPresenter(IConnectInfoModel connectInfoModel, IHandleData mIHandleData) {
        this.mConnectInfoModel = connectInfoModel;
        this.mIHandleData = mIHandleData;
    }

    public void getConnectInfo(User user, String phoneId, String region) {
        mConnectInfoModel.getConnectInfo(user, phoneId, region, new OnRequestListener<CasConnectInfo>() {
            @Override
            public void onSuccess(List<CasConnectInfo> connectInfoList, int count) {
                mIHandleData.handleData(connectInfoList, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }
}
