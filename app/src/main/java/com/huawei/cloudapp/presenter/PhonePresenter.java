package com.huawei.cloudapp.presenter;

import com.huawei.cloudapp.model.IHandleData;
import com.huawei.cloudapp.model.IPhoneModel;
import com.huawei.cloudapp.model.OnRequestListener;
import com.huawei.cloudapp.model.bean.PhoneInfo;
import com.huawei.cloudapp.model.bean.User;
import com.huawei.cloudapp.model.bean.direct.PhoneDetail;
import com.huawei.cloudapp.model.bean.direct.PhoneJobResponse;

import java.util.HashMap;
import java.util.List;

public class PhonePresenter {
    private final IPhoneModel mPhoneModel;
    private final IHandleData mIHandleData;

    public PhonePresenter(IHandleData mIHandleData, IPhoneModel phoneModel) {
        this.mPhoneModel = phoneModel;
        this.mIHandleData = mIHandleData;
    }

    public void getPhoneList(User user, HashMap<String, String> condition, String region, String projectId, int offset, int limit) {
        mPhoneModel.getPhoneList(user, condition, region, projectId, offset, limit, new OnRequestListener<PhoneInfo>() {
            @Override
            public void onSuccess(List<PhoneInfo> phoneInfo, int count) {
                mIHandleData.handleData(phoneInfo, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }

    public void restartPhone(User user, String region, String projectId, String imageId, List<HashMap<String, String>> property, List<String> phoneIdList) {
        mPhoneModel.restartPhone(user, region, projectId, phoneIdList, imageId, property, new OnRequestListener<PhoneJobResponse>() {
            @Override
            public void onSuccess(List<PhoneJobResponse> phoneJob, int count) {
                mIHandleData.handleData(phoneJob, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }

    public void resetPhone(User user, String region, String projectId, String imageId, List<HashMap<String, String>> property, List<String>  phoneIdList) {
        mPhoneModel.resetPhone(user, region, projectId, phoneIdList, imageId, property, new OnRequestListener<PhoneJobResponse>() {
            @Override
            public void onSuccess(List<PhoneJobResponse> phoneJob, int count) {
                mIHandleData.handleData(phoneJob, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }

    public void getPhoneDetailInfo(User user, String region, String projectId, String phoneId) {
        mPhoneModel.getPhoneDetailInfo(user, region, projectId, phoneId, new OnRequestListener<PhoneDetail>() {
            @Override
            public void onSuccess(List<PhoneDetail> phoneDetail, int count) {
                mIHandleData.handleData(phoneDetail, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }
}
