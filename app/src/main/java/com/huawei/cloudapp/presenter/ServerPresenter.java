package com.huawei.cloudapp.presenter;

import com.huawei.cloudapp.model.IHandleData;
import com.huawei.cloudapp.model.OnRequestListener;
import com.huawei.cloudapp.model.bean.User;
import com.huawei.cloudapp.model.bean.direct.ServerList;
import com.huawei.cloudapp.model.direct.ServerModel;

import java.util.List;

public class ServerPresenter {
    private final ServerModel mServerModel;
    private final IHandleData mIHandleData;

    public ServerPresenter(ServerModel mServerModel, IHandleData mIHandleData) {
        this.mServerModel = mServerModel;
        this.mIHandleData = mIHandleData;
    }

    public void getServerList(User user, String region, String projectId, int offset, int limit) {
        mServerModel.getServerList(user, region, projectId, offset, limit, new OnRequestListener<ServerList.Server>() {
            @Override
            public void onSuccess(List<ServerList.Server> servers, int count) {
                mIHandleData.handleData(servers, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }
}
