package com.huawei.cloudapp.presenter;

import com.huawei.cloudapp.model.IHandleData;
import com.huawei.cloudapp.model.IUserModel;
import com.huawei.cloudapp.model.OnRequestListener;
import com.huawei.cloudapp.model.bean.User;

import java.util.List;

public class UserPresenter {
   private final IUserModel mUserModel;
   private final IHandleData mIHandleData;

    public UserPresenter(IHandleData mIHandleData, IUserModel userModel) {
        this.mUserModel = userModel;
        this.mIHandleData = mIHandleData;
    }

    public void getUser(User user, String password, String region) {
        mUserModel.getUser(user, password, region, new OnRequestListener<User>() {
            @Override
            public void onSuccess(List<User> users, int count) {
                mIHandleData.handleData(users, count);
            }

            @Override
            public void onFailure(Exception e) {
                mIHandleData.handleError(e);
            }
        });
    }
}
