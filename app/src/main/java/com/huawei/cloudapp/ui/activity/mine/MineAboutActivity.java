package com.huawei.cloudapp.ui.activity.mine;

import static com.huawei.cloudapp.utils.CasConstantsUtil.ENTER_STRING;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.cloudapp.R;
import com.huawei.cloudapp.common.CasCommonDialog;
import com.huawei.cloudapp.ui.common.ButtonTouchStyleListener;
import com.huawei.cloudapp.utils.CasCommonUtils;

public class MineAboutActivity extends AppCompatActivity {

    private CasCommonDialog connectUsDialog;

    private static final String CONNECT_MSG1 = "4000-955-988 转 1";
    private static final String CONNECT_MSG2 = "950808 转 1";
    private static final String CONNECT_MSG3 = "为您提供售前购买咨询、解决方案推荐、配置推荐等1v1服务， 助您上云无忧！\n\n热线服务时间 9:00-18:00";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_about);

        ImageView backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(view -> onBackPressed());

        TextView version = findViewById(R.id.about_version);
        version.setText(CasCommonUtils.getApkVersion(MineAboutActivity.this));

        RelativeLayout connectUs = findViewById(R.id.connect_us_linear);
        connectUs.setOnTouchListener(new ButtonTouchStyleListener());
        connectUs.setOnClickListener(new MineAboutClickListener());

        RelativeLayout checkNewVersion = findViewById(R.id.check_new_version_linear);
        checkNewVersion.setOnTouchListener(new ButtonTouchStyleListener());
        checkNewVersion.setOnClickListener(new MineAboutClickListener());

        RelativeLayout privacyPolicy = findViewById(R.id.privacy_policy_linear);
        privacyPolicy.setOnTouchListener(new ButtonTouchStyleListener());
        privacyPolicy.setOnClickListener(new MineAboutClickListener());
    }

    class MineAboutClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.connect_us_linear:
                    showConnectUsDialog();
                    break;
                case R.id.check_new_version_linear:
                    Toast.makeText(view.getContext(), "检查新版本", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.privacy_policy_linear:
                    Intent intent = new Intent(MineAboutActivity.this, MineAboutPrivacyPolicyActivity.class);
                    startActivity(intent);
                    break;
                default:
                    Toast.makeText(view.getContext(), "空白", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void showConnectUsDialog() {
        if (connectUsDialog == null || !connectUsDialog.isShowing()) {
            final View contentView = View.inflate(MineAboutActivity.this, R.layout.cas_set_ui_level, null);
            CasCommonDialog.DialogClickListener dialogListener = new CasCommonDialog.DialogClickListener();

            final CasCommonDialog.Builder builder = new CasCommonDialog.Builder(MineAboutActivity.this)
                    .setTitle(R.string.connect_us)
                    .setMessage(getConnectMsg(MineAboutActivity.this))
                    .setContentView(contentView)
                    .setGravity(Gravity.START)
                    .setPositiveButton(getResources().getString(R.string.cas_phone_confirm_tip_message), dialogListener);

            connectUsDialog = builder.create();
            connectUsDialog.setCancelable(true);
            if (!isFinishing()) {
                connectUsDialog.show();
            }
        }
    }

    private static String getConnectMsg(Context context) {
        return CONNECT_MSG1
                + ENTER_STRING
                + CONNECT_MSG2
                +ENTER_STRING
                + CONNECT_MSG3;
    }

}