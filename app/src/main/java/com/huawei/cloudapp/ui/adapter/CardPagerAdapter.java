/*
 * Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.ui.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.huawei.cloudapp.R;
import com.huawei.cloudapp.model.bean.PhoneInfo;
import com.huawei.cloudapp.common.CasRecord;

import java.util.ArrayList;
import java.util.List;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<PhoneInfo> mData;
    private float mBaseElevation;
    private Context mContext;
    private CasRecord mCasRecord;
    private AutoCompleteTextView ip, port;
    private boolean removing;

    public CardPagerAdapter(Context context) {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
        mContext = context;
    }

    public void addCardItem(PhoneInfo item) {
        mViews.add(null);
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addCardItemList(List<PhoneInfo> items) {
        for (PhoneInfo phoneInfo : items) {
            mViews.add(null);
            mData.add(phoneInfo);
        }
        notifyDataSetChanged();
    }

    public void setCardItemList(List<PhoneInfo> items) {
        mData.clear();
        for (PhoneInfo phoneInfo : items) {
            mViews.add(null);
            mData.add(phoneInfo);
        }
        notifyDataSetChanged();
    }

    public void removeAllItem(){
        mData.clear();
        notifyDataSetChanged();
    }

    public void removeCardItem(int pos) {
        if (getCount() == 0) {
            return;
        }
        removing = true;
        mData.remove(pos);
        notifyDataSetChanged();
        removing = false;
    }

    public List<PhoneInfo> getAllItem() {
        return mData;
    }

    public PhoneInfo getCardItem(int position) {
        if (getCount() == 0) {
            return null;
        }
        PhoneInfo item = mData.get(position);
        if (item.getName().isEmpty() && ip != null && port != null) {
            String ipStr = ip.getText().toString().trim();
            String portStr = port.getText().toString().trim();
            if (ipStr.isEmpty() || portStr.isEmpty()) {
                item.setInfo("");
            } else {
                StringBuilder phoneInfo = new StringBuilder();
                phoneInfo.append(ip.getText().toString().trim()).append(":").append(port.getText().toString().trim());
                item.setInfo(phoneInfo.toString());
            }
        }
        return item;
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return getCount() == 0 ? null : mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.cas_cloud_phone_info_adapter, container, false);

        View title = view.findViewById(R.id.title);
        int startColor = mContext.getResources().getColor(R.color.cas_title_background_begin);
        int endColor = mContext.getResources().getColor(R.color.cas_title_background_end);
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, new int[]{startColor, endColor});
        gradientDrawable.setDither(true);
        title.setBackground(gradientDrawable);

        container.addView(view);
        bindInfo(mData.get(position), view);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if (removing) {
            return POSITION_NONE;
        }
        int index = mViews.indexOf(super.getItemPosition(object));
        if (index == -1) {
            return POSITION_NONE;
        }
        return index;
    }

    private void bindInfo(PhoneInfo item, View view) {
        TextView titleTextView = view.findViewById(R.id.titleTextView);
        TextView contentTextView = view.findViewById(R.id.contentTextView);
        if (item.getName().isEmpty()) {
        } else {
            titleTextView.setText(item.getName());
            String content = item.getInfo();
            String region = item.getRegion();
            if (region != null && !region.isEmpty()) {
                content = item.getRegion() + System.lineSeparator() + content;
            }
            contentTextView.setText(content);
        }
    }

}
