package com.huawei.cloudapp.ui.adapter;

import android.content.Context;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.huawei.cloudapp.R;
import com.huawei.cloudapp.model.bean.PhoneInfo;

public class PhoneListAdapter extends BaseQuickAdapter<PhoneInfo, BaseViewHolder> implements LoadMoreModule {

    public PhoneListAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, PhoneInfo phoneInfo) {
        Context context = baseViewHolder.itemView.getContext();
        baseViewHolder.setText(R.id.tv_phone_name, phoneInfo.getPhone().getPhoneName())
                .setText(R.id.tv_phone_id, context.getString(R.string.cloud_phone_info_id)
                        + phoneInfo.getPhone().getPhoneId())
                .setText(R.id.tv_image_id, context.getString(R.string.cloud_phone_info_image_id)
                        + phoneInfo.getPhone().getImageId())
                .setText(R.id.tv_phone_model, context.getString(R.string.cloud_phone_info_flavor)
                        + phoneInfo.getPhone().getPhoneModelName())
                .setBackgroundResource(R.id.iv_phone_status, getBackgroundIdByStatus(phoneInfo.getPhone().getStatus()));
    }

    private int getBackgroundIdByStatus(int status) {
        int id = R.drawable.cas_status_normal;
        switch (status) {
            case 1:
            case 3:
            case 4:
            case 7:
                id = R.drawable.cas_status_process;
                break;
            case 2:
                id = R.drawable.cas_status_normal;
                break;
            case 6:
            case 8:
                id = R.drawable.cas_status_shutdown;
                break;
            case -5:
            case -6:
            case -7:
            case -8:
            case -9:
                id = R.drawable.cas_status_error;
                break;
        }
        return id;
    }
}
