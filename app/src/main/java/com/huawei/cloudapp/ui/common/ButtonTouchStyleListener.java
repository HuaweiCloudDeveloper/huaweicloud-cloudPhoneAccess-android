package com.huawei.cloudapp.ui.common;

import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.huawei.cloudapp.R;

public class ButtonTouchStyleListener implements View.OnTouchListener {

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.cas_radio_button_unselected_color));
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // 改变TextView的颜色
                    view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.cas_white));
                    view.performClick();
                }
            }, 200);
        }
        return true;
    }
}
