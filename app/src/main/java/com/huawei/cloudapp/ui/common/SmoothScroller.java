package com.huawei.cloudapp.ui.common;

import android.content.Context;
import android.util.DisplayMetrics;

import androidx.recyclerview.widget.LinearSmoothScroller;

public class SmoothScroller extends LinearSmoothScroller {
    private static final float SPEED = 9000000f; // 滑动速度，单位是像素每毫秒

    public SmoothScroller(Context context) {
        super(context);
    }

    @Override
    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return SPEED / displayMetrics.densityDpi;
    }
}
