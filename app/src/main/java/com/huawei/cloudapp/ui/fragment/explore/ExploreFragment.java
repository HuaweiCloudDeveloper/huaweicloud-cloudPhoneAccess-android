package com.huawei.cloudapp.ui.fragment.explore;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hjq.toast.Toaster;
import com.huawei.cloudapp.R;
import com.huawei.cloudapp.databinding.FragmentExploreBinding;
import com.huawei.cloudapp.ui.fragment.CloudPhoneFragment;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.DefaultWebClient;
import com.just.agentweb.WebChromeClient;

public class ExploreFragment extends CloudPhoneFragment {

    private FragmentExploreBinding binding;
    private static final String TAG = "ExploreFragment";
    private boolean isReadyToExit = false;
    private AgentWeb mAgentWeb;
    private LinearLayout mLinearLayout;
    private com.just.agentweb.WebViewClient mWebViewClient = new com.just.agentweb.WebViewClient();
    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onShowCustomView(View view, CustomViewCallback customViewCallback) {
            super.onShowCustomView(view, customViewCallback);
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentExploreBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLinearLayout = view.findViewById(R.id.web_view);

        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(mLinearLayout, -1, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT))
                .useDefaultIndicator(-1, 3)
                .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
                .setWebViewClient(mWebViewClient)
                .setWebChromeClient(mWebChromeClient)
                .setMainFrameErrorView(R.layout.agentweb_error_page, -1)
                .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.DISALLOW)
                .interceptUnkownUrl()
                .createAgentWeb()
                .ready()
                .go("https://www.huaweicloud.com/product/cloudphone.html");
        mAgentWeb.getWebCreator().getWebView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
        WebSettings webSettings = mAgentWeb.getWebCreator().getWebView().getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
    }

    @Override
    public void onResume() {
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onResume();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mAgentWeb != null) {
            mAgentWeb.getWebLifeCycle().onDestroy();
        }
        binding = null;
    }

    @Override
    public boolean onBackPressed() {
        if (mAgentWeb.back()) {
            return true;
        }
        if (!isReadyToExit) {
            Toaster.show(R.string.cas_exit_app_confirmation);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isReadyToExit = false;
                }
            }, 2000);
            isReadyToExit = true;
            return true;
        } else {
            return false;
        }
    }
}