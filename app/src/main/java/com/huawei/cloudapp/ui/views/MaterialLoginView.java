package com.huawei.cloudapp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;
import com.huawei.cloudapp.R;

public class MaterialLoginView extends FrameLayout {

    private static final String TAG = MaterialLoginView.class.getSimpleName();

    public interface LoginViewListener {
        void onLogin(TextInputLayout loginUser, TextInputLayout loginIamUser, TextInputLayout loginPass, boolean isUseIam);
    }

    private LoginViewListener listener;
    private MaterialCheckBox checkBox;

    public MaterialLoginView(Context context) {
        this(context, null);
    }

    public MaterialLoginView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterialLoginView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.login_view, this, true);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.MaterialLoginView,
                0, 0);

        TextView loginTitle = findViewById(R.id.login_title);
        final TextInputLayout loginUser = findViewById(R.id.login_user);
        final TextInputLayout loginPass = findViewById(R.id.login_pass);
        final TextInputLayout loginIamUser = findViewById(R.id.login_iam_user);

        checkBox = findViewById(R.id.is_use_iam);
        final boolean[] isUseIam = {false};
        final String[] mode = {""};

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    loginIamUser.setVisibility(VISIBLE);
                } else {
                    loginIamUser.setVisibility(GONE);
                }
                isUseIam[0] = b;
            }
        });

        TextView loginBtn = findViewById(R.id.login_btn);
        findViewById(R.id.login_btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onLogin(loginUser, loginIamUser, loginPass, isUseIam[0]);
                }
            }
        });

        try {
            String string = a.getString(R.styleable.MaterialLoginView_loginTitle);
            if (string != null) {
                loginTitle.setText(string);
            }

            string = a.getString(R.styleable.MaterialLoginView_loginHint);
            if (string != null) {
                loginUser.setHint(string);
            }

            string = a.getString(R.styleable.MaterialLoginView_loginPasswordHint);
            if (string != null) {
                loginPass.setHint(string);
            }

            string = a.getString(R.styleable.MaterialLoginView_loginActionText);
            if (string != null) {
                loginBtn.setText(string);
            }

            int color = a.getColor(R.styleable.MaterialLoginView_loginTextColor, ContextCompat.getColor(getContext(), R.color.material_login_login_text_color));
            loginUser.getEditText().setTextColor(color);
            loginIamUser.getEditText().setTextColor(color);
            loginPass.getEditText().setTextColor(color);
            checkBox.setChecked(true);
        } finally {
            a.recycle();
        }
    }

    public void setListener(LoginViewListener listener) {
        this.listener = listener;
    }

    public void setIamEnable(boolean enable) {
        int visibility = enable ? VISIBLE : GONE;
        checkBox.setVisibility(visibility);
    }
}
