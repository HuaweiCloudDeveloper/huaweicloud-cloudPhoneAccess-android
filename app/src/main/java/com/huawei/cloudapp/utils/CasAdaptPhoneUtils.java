/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.utils;

import android.content.Context;
import android.view.WindowManager;

import com.huawei.cloudapp.common.CASLog;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * CasAdaptPhoneUtils
 */
public final class CasAdaptPhoneUtils {

    /**
     * FLAG_NOTCH_SUPPORT_HW
     */
    public static final int FLAG_NOTCH_SUPPORT_HW = 0x00010000;

    /**
     * log tag
     */
    private static final String TAG = "CasAdaptPhoneUtils";

    /**
     * sAdaptUtils
     */
    private static volatile CasAdaptPhoneUtils sAdaptUtils = null;

    /**
     * private constructor
     */
    private CasAdaptPhoneUtils() {
    }

    /**
     * getInstance
     */
    public static CasAdaptPhoneUtils getInstance() {
        if (null == sAdaptUtils) {
            synchronized (CasAdaptPhoneUtils.class) {
                if (null == sAdaptUtils) {
                    sAdaptUtils = new CasAdaptPhoneUtils();
                }
            }
        }
        return sAdaptUtils;
    }

    /**
     * adapt phone notch
     * <p>huawei mobile phone</p>
     * <p>mi phone</p>
     * <p>ov phone</p>
     */
    public void adaptPhoneNotch(Context context, WindowManager.LayoutParams params) {
        if (hasNotchAtHuawei(context)) {
            adaptNotchAtHuawei(params);
        }
    }

    /**
     * judge whether Huawei mobile phone has notch
     */
    private boolean hasNotchAtHuawei(Context context) {
        boolean hasNotch = false;
        try {
            ClassLoader classLoader = context.getClassLoader();
            Class hwNotchSizeUtil = classLoader.loadClass("com.huawei.android.util.HwNotchSizeUtil");
            Method method = hwNotchSizeUtil.getMethod("hasNotchInScreen");
            hasNotch = (boolean) method.invoke(hwNotchSizeUtil);
        } catch (ClassNotFoundException e) {
            CASLog.e(TAG, "[hasNotchAtHuawei] ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            CASLog.e(TAG, "[hasNotchAtHuawei] NoSuchMethodException");
        } catch (Exception e) {
            CASLog.e(TAG, "[hasNotchAtHuawei] Exception");
        }
        return hasNotch;
    }

    /**
     * adapt notch at Huawei mobile phone
     */
    private void adaptNotchAtHuawei(WindowManager.LayoutParams params) {
        try {
            Class layoutParamsExCls = Class.forName("com.huawei.android.view.LayoutParamsEx");
            Constructor con = layoutParamsExCls.getConstructor(WindowManager.LayoutParams.class);
            Object layoutParamsExObj = con.newInstance(params);
            Method method = layoutParamsExCls.getMethod("clearHwFlags", int.class);
            method.invoke(layoutParamsExObj, FLAG_NOTCH_SUPPORT_HW);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException
            | InvocationTargetException e) {
            CASLog.e(TAG, "[adaptNotchAtHuawei] hw clear notch screen flag api error");
        } catch (Exception e) {
            CASLog.e(TAG, "[adaptNotchAtHuawei] other Exception");
        }
    }
}
