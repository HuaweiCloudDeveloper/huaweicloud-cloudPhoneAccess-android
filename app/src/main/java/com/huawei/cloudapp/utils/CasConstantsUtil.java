/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudapp.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * CasConstantsUtil
 */
public class CasConstantsUtil {

    public static final String USERNAME = "username";

    public static final String IAM_USERNAME = "iam_username";

    public static final String PASSWORD = "password";

    public static final String ENCRYPT_IV = "iv";

    public static final String ERROR_CODE = "errorCode";

    public static final String ERROR_MSG = "errorMsg";

    public static final String TOKEN = "token";

    public static final String TOKEN_EXPIRE_TIME = "token_expire_time";

    public static final String PROJECT_ID = "project_id";

    public static final String ORIENTATION = "orientation";

    public static final String MEDIA_CONFIG = "media_config";

    public static final String PHONE_ID = "phone_id";

    public static final String CLOUD_PHONE_ID = "cloud_phone_id";

    public static final String PHONE_NAME = "phone_name";

    public static final String REGION_ID = "region_id";

    public static final String CLIENT_TYPE = "client_type";

    public static final String CLIENT_MODE = "client_mode";

    public static final String ERROR = "error";

    public static final String CODE = "code";

    public static final String MESSAGE = "message";

    public static final String X_SUBJECT_TOKEN = "X-Subject-Token";
    public static final String X_AUTH_TOKEN = "X-Auth-Token";

    /**
     * cph:ip
     */
    public static final String IP = "ip";

    /**
     * check cph ip
     */
    public static final Pattern IP_PATTERN = Pattern.compile("^((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}$");

    /**
     * cph:port
     */
    public static final String PORT = "port";

    /**
     * background timeout
     * <p>timeout when back to home</p>
     */
    public static final String BACKGROUND_TIMEOUT = "background_timeout";

    /**
     * available play time
     * <p>unit is second</p>
     */
    public static final String AVAILABLE_PLAYTIME = "available_playtime";

    /**
     * ticket
     * <p>256 random numbers</p>
     */
    public static final String TICKET = "ticket";

    /**
     * sessionId
     */
    public static final String SESSION_ID = "session_id";

    /**
     * aes key
     * <p>generate by aes 128</p>
     */
    public static final String AES_KEY = "aes_key";

    /**
     * auth ts
     * <p>authentication timestamp</p>
     */
    public static final String AUTH_TS = "auth_ts";

    /**
     * timestamp
     * <p>timestamp</p>
     */
    public static final String TIMESTAMP = "timestamp";

    /**
     * userid
     */
    public static final String USER_ID = "user_id";

    /**
     * touch_timeout
     */
    public static final String TOUCH_TIMEOUT = "touch_timeout";

    /**
     * frame_type
     */
    public static final String FRAME_TYPE = "frame_type";

    /**
     * encode_type
     */
    public static final String ENCODE_TYPE = "encode_type";

    /**
     * remote_scheduling_elb_ip
     */
    public static final String REMOTE_SCHEDULING_ELB_IP = "remote_scheduling_elb_ip";

    /**
     * remote_scheduling_elb_ip
     */
    public static final String REMOTE_SCHEDULING_ELB_PORT = "remote_scheduling_elb_port";

    /**
     * check aes key
     */
    public static final Pattern AES_KEY_PATTERN = Pattern.compile("^[0-9a-fA-F]{32}$");

    /**
     * check whether is positive number
     */
    public static final Pattern POSITIVE_NUMBER_PATTERN = Pattern.compile("^[1-9]\\d*$");

    /**
     * check user name
     */
    public static final Pattern USER_NAME_PATTERN = Pattern.compile("^[a-zA-Z0-9_]{6,18}$");

    /**
     * check password
     */
    public static final Pattern PASSWORD_PATTERN = Pattern.compile("^[a-zA-Z0-9_]{9,18}$");

    /**
     * Physical_width
     */
    public static final String WIDTH = "width";

    /**
     * Physical_width
     */
    public static final String PHYSICAL_WIDTH = "physical_width";

    /**
     * Physical_height
     */
    public static final String PHYSICAL_HEIGHT = "physical_height";

    /**
     * quality
     */
    public static final String QUALITY = "quality";

    /**
     * auto_quality
     */
    public static final String AUTO_QUALITY = "auto_quality";

    /**
     * user_info
     */
    public static final String USER_INFO = "user_info";
    public static final String COMMON = "common";
    public static final String SELECTED_REGION_POSITION = "selected_region_position";

    public static final String FRAME_TYPE_H265 = "h265";
    public static final String FRAME_TYPE_H264 = "h264";

    public static final String RESOLUTION_720P = "720";
    public static final String RESOLUTION_1080P = "1080";

    public static final String DEFINITION_BEST = "best";
    public static final String DEFINITION_MAIN = "main";
    public static final String DEFINITION_BASIC = "basic";

    public static final String DEBUG_MODE = "debug_mode";

    public static final String PERSONAL = "personal";
    public static final String ENTERPRISE = "enterprise";

    public static final String MANAGEMENT_DEV_ENV_URL = "https://139.9.235.129:18000";
    public static final String MANAGEMENT_PROD_ENV_URL = "https://139.9.146.101:18000";
    public static final String PHONE = "phone";
    public static final String LIST = "list";
    public static final String CONNECTION = "connection";
    public static final String ALLOCATION = "allocate";
    public static final String USER = "user";
    public static final String LOGIN = "login";
    public static final String DIRECT = "direct";
    public static final String MANAGEMENT = "management";

    public static final String SERVER_ID = "server_id";
    public static final String STATUS = "status";

    public static final String EMPTY_STRING = "";

    public static final String ENTER_STRING = "\n";



    public static final List<String> REGION_NAME;
    static {
        REGION_NAME = new ArrayList<>();
        REGION_NAME.add("ap-southeast-1");
        REGION_NAME.add("ap-southeast-3");
        REGION_NAME.add("cn-east-2");
        REGION_NAME.add("cn-east-3");
        REGION_NAME.add("cn-north-4");
        REGION_NAME.add("cn-south-1");
        REGION_NAME.add("cn-southwest-2");
        REGION_NAME.add("ru-northwest-2");
    }

    public static final Map<String, String> REGION_INFO;
    static {
        REGION_INFO = new LinkedHashMap<>();
        REGION_INFO.put(REGION_NAME.get(0), "香港");
        REGION_INFO.put(REGION_NAME.get(1), "新加坡");
        REGION_INFO.put(REGION_NAME.get(2), "上海二");
        REGION_INFO.put(REGION_NAME.get(3), "上海一");
        REGION_INFO.put(REGION_NAME.get(4), "北京四");
        REGION_INFO.put(REGION_NAME.get(5), "广州");
        REGION_INFO.put(REGION_NAME.get(6), "贵阳一");
        REGION_INFO.put(REGION_NAME.get(7), "莫斯科二");
    }

    public static final Map<Integer, String> PHONE_STATUS;
    static {
        PHONE_STATUS = new LinkedHashMap<>();
        PHONE_STATUS.put(0, "所有状态");
        PHONE_STATUS.put(1, "创建中");
        PHONE_STATUS.put(2, "运行中");
        PHONE_STATUS.put(3, "重置中");
        PHONE_STATUS.put(4, "重启中");
        PHONE_STATUS.put(6, "冻结");
        PHONE_STATUS.put(7, "正在关机");
        PHONE_STATUS.put(8, "已关机");
        PHONE_STATUS.put(-5, "重置失败");
        PHONE_STATUS.put(-6, "重启失败");
        PHONE_STATUS.put(-7, "手机异常");
        PHONE_STATUS.put(-8, "创建失败");
        PHONE_STATUS.put(-9, "关机失败");
    }
}
