# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-ignorewarnings
-keep class com.huawei.cloudphone.api.CloudPhoneParas {*;}
-keep class com.huawei.cloudphone.api.CloudPhonePermissionInfo {*;}
-keep class com.huawei.cloudphone.api.CloudPhoneManager {*;}
-keep interface com.huawei.cloudphone.api.CloudPhoneStateListener {*;}
-keep interface com.huawei.cloudphone.api.CloudAppDataListener {*;}
-keep interface com.huawei.cloudphone.api.CloudPhoneOrientationChangeListener {*;}
-keep interface com.huawei.cloudphone.api.CloudPhonePermissionRequestListener {*;}
-keep interface com.huawei.cloudphone.api.CloudPhoneClipboardListener {*;}
-keep interface com.huawei.cloudphone.api.ICloudPhone {*;}
-keep public enum com.huawei.cloudphone.api.CloudPhoneParas$* {*;}
-keep interface com.huawei.cloudphone.service.CasInteractiveStateCallback {*;}

-keep class com.huawei.cloudphone.utils.CasDevRandomSeed {*;}