// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASCOMMON_H
#define CLOUDAPPSDK_CASCOMMON_H

#include <string>

const std::string KEY_IP                = "ip";
const std::string KEY_PORT              = "port";
const std::string KEY_TOKEN             = "token";
const std::string KEY_TICKET            = "ticket";

const std::string KEY_SESSION_ID        = "session_id";
const std::string KEY_AES_IV            = "aes_iv";
const std::string KEY_ENCRYPTED_DATA    = "encrypted_data";
const std::string KEY_AUTH_TS           = "auth_ts";
const std::string KEY_VERIFY_DATA       = "verify_data";

const std::string KEY_BACKGROUND_TIMEOUT = "backgroundTimeout";
const std::string KEY_AVAILABLE_PLAYTIME = "available_playtime";
const std::string KEY_CLIENT_TYPE        = "client_type";
const std::string KEY_CLIENT_MODE        = "client_mode";
const std::string KEY_REGION_ID          = "region_id";
const std::string KEY_MEDIA_CONFIG       = "media_config";
const std::string KEY_USER_ID            = "user_id";

const std::string KEY_FRAME_RATE        = "fps";
const std::string KEY_FRAME_TYPE        = "frame_type";
const std::string KEY_BITRATE           = "bitrate";
const std::string KEY_STREAM_WIDTH     = "stream_width";
const std::string KEY_STREAM_HEIGHT    = "stream_height";

enum VirtualDevice {
    VIRTUAL_CAMERA,
    VIRTUAL_MICROPHONE,
    VIRTUAL_SENSOR
};

enum {
    CAS_CONNECTING                      = 0x0100,
    CAS_CONNECT_SUCCESS                 = 0x0200,
    CAS_SERVER_UNREACHABLE              = 0x0301,
    CAS_RESOURCE_IN_USING               = 0x0302,

    CAS_VERIFYING                       = 0x0400,
    CAS_VERIFY_SUCCESS                  = 0x0500,
    CAS_VERIFY_PARAMETER_MISSING        = 0x0601,
    CAS_VERIFY_PARAMETER_INVALID        = 0x0602,
    CAS_VERIFY_AESKEY_QUERY_FAILED      = 0x0603,
    CAS_VERIFY_AESKEY_INVALID           = 0x0604,
    CAS_VERIFY_DECRYPT_FAILED           = 0x0605,
    CAS_VERIFY_FAILED                   = 0x0606,

    CAS_START_SUCCESS                   = 0x0800,
    CAS_PARAMETER_MISSING               = 0x0904,

    CAS_RECONNECTING                    = 0x0B00,
    CAS_RECONNECT_SUCCESS               = 0x0C00,
    CAS_RECONNECT_PARAMETER_INVALID     = 0x0D01,
    CAS_RECONNECT_SERVER_UNREACHABLE    = 0x0D02,
    CAS_RECONNECT_ENGING_START_ERROR    = 0x0D03,

    CAS_TRAIL_PLAY_TIMEOUT              = 0x0E00,
    CAS_HOME_TIMEOUT                    = 0x1000,

    CAS_ENCODE_ERROR                    = 0x1100,
    CAS_ENGINE_START_FAILED             = 0x1101,
    CAS_H265_NOT_SUPPORT                = 0x1102,

    CAS_SWITCH_BACKGROUND_SUCCESS       = 0x1200,
    CAS_SWITCH_BACKGROUND_ERROR         = 0x1301,
    CAS_SWITCH_FOREGROUND_SUCCESS       = 0x1400,
    CAS_SWITCH_FOREGROUND_ERROR         = 0x1501,

    CAS_ORIENTATION                     = 0x1600,
    CAS_EXIT                            = 0x1700,
    CAS_BACK_HOME                       = 0x1900,
    CAS_REQUEST_CAMERA_KEY_FRAME        = 0x2000,
};

const int CAS_THREAD_RUNNING = 1;
const int CAS_THREAD_INIT = 0;
const int CAS_THREAD_EXIT = -1;
const int CAS_THREAD_STOP = -2;

#endif // CLOUDAPPSDK_CASCOMMON_H
