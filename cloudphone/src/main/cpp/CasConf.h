// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASCONF_H
#define CLOUDAPPSDK_CASCONF_H

#include <string>
#include <map>
#include "CasCommon.h"

const std::string KEY_LOG_LEVEL = "log_level";
const std::string KEY_DECODE_METHOD = "decode_method";
const std::string KEY_SDK_VERSION = "sdk_version";
const std::string KEY_PROTOCOL_VERSION = "protocol_version";
const std::string KEY_MAX_DISCONNECT_DURATION = "max_disconnect_duration";

struct CasConf {
    std::string ip;
    int port;
    std::string token;
    std::string ticket;
    std::string sessionId;
    std::string aesIv;
    std::string encryptedData;
    std::string authTs;
    std::string verifyData;
    std::string sdkVersion;
    std::string protocolVersion;
    std::string clientType;
    std::string clientMode;
    std::string regionId;
    std::string backgroundTimeout;

    int stoi_s(const std::string &str)
    {
        if (str.data() == nullptr || str.length() <= 0) {
            return 0;
        } else {
            return std::stoi(str);
        }
    };

    bool parseConf(std::map<std::string, std::string> jniConf)
    {
        ip = (jniConf)[KEY_IP];
        port = stoi_s((jniConf)[KEY_PORT]);
        token = (jniConf)[KEY_TOKEN];
        ticket = (jniConf)[KEY_TICKET];
        sessionId = (jniConf)[KEY_SESSION_ID];
        aesIv = (jniConf)[KEY_AES_IV];
        encryptedData = (jniConf)[KEY_ENCRYPTED_DATA];
        authTs = (jniConf)[KEY_AUTH_TS];
        verifyData = (jniConf)[KEY_VERIFY_DATA];
        sdkVersion = (jniConf)[KEY_SDK_VERSION];
        protocolVersion = (jniConf)[KEY_PROTOCOL_VERSION];
        clientType = (jniConf)[KEY_CLIENT_TYPE];
        clientMode = (jniConf)[KEY_CLIENT_MODE];
        regionId = (jniConf)[KEY_REGION_ID];
        backgroundTimeout = (jniConf)[KEY_BACKGROUND_TIMEOUT];

        return true;
    }
};

#endif // CLOUDAPPSDK_CASCONF_H
