// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <dlfcn.h>
#include <securec.h>
#include <fcntl.h>
#include <CasBuffer.h>
#include <sstream>
#include "spdlog/sinks/rotating_file_sink.h"
#include "CasJniBridge.h"
#include "CasController.h"
#include "CasConf.h"
#include "CasTcpSocket.h"
#include "CasMsgCode.h"
#include "CasExtVideoDataPipe.h"
#include "opus.h"
#include "CasVideoUtil.h"

using namespace std;

const int BITRATE_MAX = 10000000;
const int BITRATE_MIN = 100000;
const int WIDTH_MAX = 4096;
const int WIDTH_MIN = 240;
const int HEIGHT_MAX = 4096;
const int HEIGHT_MIN = 240;
const int TIMES = 8;
const int FRAME_RATE_MIN = 10;
const int FRAME_RATE_MAX = 60;
const std::string CLIENT_TYPE = "1";
const std::string HRTP_LOG_PATH = "/sdcard/Android/media/com.huawei.cloudapp.b004/";
const uint64_t DURATION_USEC = 1000000ULL;

int32_t OnRecvVideoStreamData(uint8_t* data, uint32_t length);
int32_t OnRecvAudioStreamData(uint8_t* data, uint32_t length);
int32_t OnRecvAudioDecodeCallback(int32_t streamId, AudioJbDecode* audioJbDecode);
int32_t OnRecvCmdData(uint8_t* data, uint32_t length);
void OnGotTransLog(const char* str, uint32_t length);
void OnRecvBitrate(uint32_t encBitrate, uint32_t totalBitrate);
void OnNeedKeyFrameCallback();
OpusDecoder *g_opusDecoder = nullptr;
shared_ptr<spdlog::logger> hrtpLogger = nullptr;
int g_plcCount = 0;

CasController *CasController::g_instance = nullptr;
CasController *CasController::GetInstance()
{
    if (g_instance == nullptr) {
        g_instance = new (std::nothrow) CasController();
        if (g_instance == nullptr) {
            ERR("Failed to new CasController.");
            return nullptr;
        }
    }
    return g_instance;
}

bool CasController::DestroyInstance()
{
    if (g_instance != nullptr) {
        delete g_instance;
        g_instance = nullptr;
        return true;
    }
    INFO("Instance already destroyed.");
    return true;
}

CasController::CasController()
{
    m_videoDecodeThread = nullptr;
    m_videoPacketStream = nullptr;
    m_audioPacketStream = nullptr;
    m_orientationStream = nullptr;
    m_virtualDeviceStream = nullptr;
    m_controlStream = nullptr;
    m_channelStream = nullptr;
    m_imeDataStream = nullptr;
    m_cmdController = nullptr;
    m_streamParseThread = nullptr;
    m_heartbeatThread = nullptr;
    m_heartbeatController = nullptr;
    m_casClientSocket = nullptr;
    m_streamBuildSender = nullptr;
    m_streamParser = nullptr;
    m_state = INIT;
    m_touch = nullptr;
    cmdCallBack = nullptr;
    m_orientation = 0;
    m_frameType = FrameType::H264;
    m_rotationDegrees = 0;
    m_mtrans = nullptr;
    m_isMTransValid = false;
}

CasController::~CasController()
{
    m_videoDecodeThread = nullptr;
    m_videoPacketStream = nullptr;
    m_audioPacketStream = nullptr;
    m_orientationStream = nullptr;
    m_virtualDeviceStream = nullptr;
    m_controlStream = nullptr;
    m_channelStream = nullptr;
    m_imeDataStream = nullptr;
    m_cmdController = nullptr;
    m_streamParseThread = nullptr;
    m_heartbeatThread = nullptr;
    m_controlThread = nullptr;
    m_heartbeatController = nullptr;
    m_casClientSocket = nullptr;
    m_streamBuildSender = nullptr;
    m_streamParser = nullptr;
    m_state = INIT;
    m_touch = nullptr;
    cmdCallBack = nullptr;
    m_orientation = 0;
    m_mtrans = nullptr;
    m_isMTransValid = false;
}

void CasController::SetJniConf(string key, string value)
{
    std::lock_guard<std::mutex> lockGuard(m_jniConfLock);
    this->m_jniConf[key] = value;
}

JNIState CasController::GetState()
{
    return this->m_state;
}

void CasController::SetState(JNIState state)
{
    this->m_state = state;
}

bool CasController::Start(ANativeWindow *nativeWindow, bool isHome)
{
    m_orientation = 0;
    m_nativeWindow = nativeWindow;
    bool res = false;
    if (isHome && this->GetState() != STOPPED && m_casClientSocket->GetStatus() != SOCKET_STATUS_RUNNING) {
        res = Reconnect();
        if (res) {
            CreateDecWorker(m_nativeWindow, m_needVideoDecode);
            StartDecWorker(!m_retainVideoDecode);
            return true;
        } else {
            NotifyCommand(CAS_CONNECT_LOST, CasMsgCode::GetMsg(CAS_CONNECT_LOST));
            return false;
        }
    }
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (isHome) {
        return ProcessEnterForeground(m_nativeWindow);
    }

    if (this->GetState() == STOPPED) {
        this->SetState(INIT);
    }

    m_conf.parseConf(m_jniConf);
    m_ticket = m_conf.ticket;
    m_sessionId = m_conf.sessionId;
    m_ip = TransIp(m_conf.ip.c_str());
    m_port = (unsigned short)m_conf.port;
    m_encryptedData = m_conf.encryptedData;
    m_verifyData = m_conf.verifyData;
    m_authTs = m_conf.authTs;
    m_aesIv = m_conf.aesIv;
    m_clientType = CLIENT_TYPE;
    m_maxDisconnectDuration = CalcMaxDisconnectDuration(m_conf.backgroundTimeout);

    if (m_mediaConfig.find(KEY_FRAME_TYPE) != m_mediaConfig.end()) {
        m_frameType = m_mediaConfig[KEY_FRAME_TYPE] == "h264" ? FrameType::H264 : FrameType::H265;
    }

    res = InitDataStream();
    if (!res) {
        CloseDataStream();
        return false;
    }

    res = CreateWorkers();
    if (!res) {
        DestroyWorkers();
        CloseDataStream();
        return false;
    }

    res = BuildConnection();
    if (!res) {
        ERR("Failed to build connection");
        DestroyWorkers();
        CloseDataStream();
        return false;
    }

    StartWorkers();

    if (!SendStartCmd()) {
        ERR("Failed to send start command");
        return false;
    }
    return true;
}

bool CasController::IsValidMediaConfig(map<string, string> mediaConfig)
{
    if (mediaConfig.empty()) {
        ERR("Media config is empty.");
        return false;
    }

    if (mediaConfig.find(KEY_BITRATE) != mediaConfig.end()) {
        uint32_t bitrate = static_cast<uint32_t>(atoi(mediaConfig[KEY_BITRATE].c_str()));
        if (bitrate < BITRATE_MIN || bitrate > BITRATE_MAX) {
            ERR("Bitrate is invalid, value is %u.", bitrate);
            return false;
        }
    }

    if (mediaConfig.find(KEY_FRAME_RATE) != mediaConfig.end()) {
        uint32_t frameRate = static_cast<uint32_t>(atoi(mediaConfig[KEY_FRAME_RATE].c_str()));
        if (frameRate < FRAME_RATE_MIN || frameRate > FRAME_RATE_MAX || frameRate % 10 != 0) {
            ERR("Frame rate is invalid, value is %u.", frameRate);
            return false;
        }
    }

    // 校验虚拟宽高，虚拟宽高需满足同时设置或同时未设置，且大于等于240，小于等于4096，与8对齐
    bool containsStreamWidth = mediaConfig.find(KEY_STREAM_WIDTH) != mediaConfig.end();
    bool containsStreamHeight = mediaConfig.find(KEY_STREAM_HEIGHT) != mediaConfig.end();
    if (containsStreamWidth && containsStreamHeight) {
        uint32_t streamWidth = static_cast<uint32_t>(atoi(mediaConfig[KEY_STREAM_WIDTH].c_str()));
        uint32_t streamHeight = static_cast<uint32_t>(atoi(mediaConfig[KEY_STREAM_HEIGHT].c_str()));
        if (streamWidth > streamHeight || streamWidth < WIDTH_MIN || streamWidth > WIDTH_MAX ||
            streamWidth % TIMES != 0 || streamHeight < HEIGHT_MIN || streamHeight > HEIGHT_MAX ||
            streamHeight % TIMES != 0) {
            ERR("Stream width or Stream height is invalid, Stream width %u Stream height %u", streamWidth, streamHeight);
            return false;
        }
    } else if (containsStreamWidth || containsStreamHeight) {
        ERR("Stream width or Stream height is not included");
        return false;
    }
    return true;
}

string CasController::CalcMaxDisconnectDuration(string backgroundTimeout)
{
    int maxDisconnectDuration = 60;
    int timeout = atoi(backgroundTimeout.c_str());
    if (timeout > 120) {
        maxDisconnectDuration = timeout - 60;
    }
    return to_string(maxDisconnectDuration);
}

bool CasController::Stop(bool isHome)
{
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Current state is stopped.");
        return true;
    }
    if (isHome) {
        ProcessEnterBackground();
        return true;
    }

    map<string, string> parameters = { { KEY_COMMAND, CMD_STOP_APP } };
    bool res = SendCommand(parameters);
    if (!res) {
        ERR("Failed to send stop command.");
    }

    this->SetState(STOPPED);

    DestroyWorkers();
    StopDecWorker(!m_retainVideoDecode);
    CloseDataStream();

#if MTRANS_ENABLED
    if (m_mtrans != nullptr) {
        delete m_mtrans;
        m_mtrans = nullptr;
    }
#endif

    m_isNotifyFirstFrame = false;
    m_rotationDegrees = 0;
    m_orientation = 0;
    m_mediaConfig.clear();
    m_jniConf.clear();
    return true;
}

bool CasController::Release()
{
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Release failed because phone already stop.");
        return false;
    }

    DestroyWorkers();
    StopDecWorker(m_retainVideoDecode);
    ClearDataStream();
    return true;
}

bool CasController::Reconnect()
{
    bool res = Release();
    if (!res) {
        ERR("Reconnect fail because release resource failed.");
        return false;
    }

    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Reconnect failed because phone already stop.");
        return false;
    }

    res = CreateWorkers();
    if (!res) {
        DestroyWorkers();
        return false;
    }

    NotifyCommand(CAS_RECONNECTING, CasMsgCode::GetMsg(CAS_RECONNECTING));
    int connectRes = m_casClientSocket->Connect();
    if (connectRes != 0) {
        ERR("Failed to build Reconnect socket state %d.", m_casClientSocket->GetStatus());
        this->SetState(CONNECTION_FAILURE);
        return false;
    }

    if (m_sessionId.empty()) {
        ERR("SessionId is empty.");
        NotifyCommand(CAS_RECONNECT_PARAMETER_INVALID, CasMsgCode::GetMsg(CAS_RECONNECT_PARAMETER_INVALID));
        return false;
    }
    this->SetState(CONNECTED);
    NotifyCommand(CAS_RECONNECT_SUCCESS, CasMsgCode::GetMsg(CAS_RECONNECT_SUCCESS));

    std::string reconnectCmd = CMD_RECONNECT;
    map<string, string> parameters = { { KEY_COMMAND, reconnectCmd }, { KEY_SESSION_ID, m_sessionId } };

    res = SendCommand(parameters);
    if (!res) {
        ERR("Failed to send reconnect command");
        return false;
    }

    StartWorkers();
    StartDecWorker(m_retainVideoDecode);
    return true;
}

bool CasController::CreateWorkers()
{
    m_casClientSocket = new (std::nothrow) CasTcpClientSocket(m_ip, m_port);
    if (m_casClientSocket == nullptr) {
        ERR("Failed to new client socket.");
        return false;
    }

    m_streamBuildSender = new (std::nothrow) CasStreamBuildSender(m_casClientSocket);
    if (m_streamBuildSender == nullptr) {
        ERR("Failed to new stream build sender.");
        return false;
    }

    m_streamParser = CasStreamRecvParser::GetInstance();
    if (m_streamParser == nullptr) {
        ERR("Failed to get stream parser.");
        return false;
    }

    m_streamParseThread = new (std::nothrow) CasStreamParseThread(m_casClientSocket, m_streamParser);
    if (m_streamParseThread == nullptr) {
        ERR("Failed to new stream parse thread.");
        return false;
    }

    m_heartbeatController = new (std::nothrow) CasHeartbeatController(m_streamBuildSender);
    if (m_heartbeatController == nullptr) {
        ERR("Failed to new heartbeat controller.");
        return false;
    }

    m_heartbeatThread = new (std::nothrow) CasHeartbeatThread(m_heartbeatController, m_casClientSocket);
    if (m_heartbeatThread == nullptr) {
        ERR("Failed to new heartbeat thread.");
        return false;
    }

    m_cmdController = new (std::nothrow) CasCmdController(m_streamBuildSender);
    if (m_cmdController == nullptr) {
        ERR("Failed to new cmd controller.");
        return false;
    }
    m_cmdController->RegisterStateChangeListener(this);

    m_controlThread = new (std::nothrow) CasCmdControlThread(m_cmdController);
    if (m_controlThread == nullptr) {
        ERR("Failed to new cmd controller.");
        return false;
    }
    m_controlThread->SetControlPktHandle(m_controlStream);

    m_streamParser->SetServiceHandle(CasMsgType::HeartBeat, m_heartbeatController);
    m_streamParser->SetServiceHandle(CasMsgType::CmdControl, m_controlStream);
    m_streamParser->SetServiceHandle(CasMsgType::Orientation, m_orientationStream);
    m_streamParser->SetServiceHandle(CasMsgType::Video, m_videoPacketStream);
    m_streamParser->SetServiceHandle(CasMsgType::Audio, m_audioPacketStream);
    m_streamParser->SetServiceHandle(CasMsgType::Channel, m_channelStream);
    m_streamParser->SetServiceHandle(CasMsgType::VirtualDevice, m_virtualDeviceStream);
    m_streamParser->SetServiceHandle(CasMsgType::ImeData, m_imeDataStream);

    m_touch = new (std::nothrow) CasTouch(m_casClientSocket);
    if (m_touch == nullptr) {
        ERR("Failed to new touch.");
        return false;
    }

#if MTRANS_ENABLED
    if (m_mtrans == nullptr) {
        TransConfigParam param;
        param.minVideoSendBitrate = 750;
        param.maxVideoSendBitrate = 5000;
        param.curBandwidth = 5000;
        param.maxBandwidth = 20000;
        if (!m_logInit) {
            m_logInit = true;
            // Create a file rotating logger with 50 MB size max and 3 rotated files
            auto max_size = 1048576 * 50;
            auto max_files = 3;
            hrtpLogger = spdlog::rotating_logger_mt("hrtp_logger", HRTP_LOG_PATH + "hrtp_log.txt", max_size, max_files);
        }

        m_mtrans = new (std::nothrow) NetTrans();
        m_mtrans->EnableLog(HRTP_LOG_PATH + "hrtp_log.txt", TRANS_LOG_LEVEL_INFO);
        int res = m_mtrans->Init(PEER_CLIENT, m_conf.ip.c_str(), m_conf.port, param,
                       OnRecvVideoStreamData,
                       OnRecvAudioStreamData,
                       OnRecvBitrate,
                       OnNeedKeyFrameCallback,
                       OnRecvCmdData,
                       OnRecvAudioDecodeCallback,
                       OnGotTransLog);
        INFO("m_mtrans->Init result = %d", res);
    }
#endif

    int err = 0;
    g_opusDecoder = opus_decoder_create(48000, 2, &err);

    return true;
}

bool CasController::StartWorkers()
{
#if MTRANS_ENABLED
    if (m_mtrans != nullptr) {
        if (m_mtrans->Start() < 0) {
            m_mtrans->Stop();
            m_streamBuildSender->SetNetTrans(nullptr);
        } else {
            m_streamBuildSender->SetNetTrans(m_mtrans);
        }
    }
#endif
    m_streamParseThread->Start();
    m_heartbeatThread->Start();
    m_controlThread->Start();
    INFO("Succeed to start workers");
    return true;
}

bool CasController::DestroyWorkers()
{
    if (m_streamParseThread != nullptr) {
        m_streamParseThread->Stop();
        delete m_streamParseThread;
        m_streamParseThread = nullptr;
    }

    if (m_streamParser != nullptr) {
        CasStreamRecvParser::DestroyInstance();
        m_streamParser = nullptr;
    }

    if (m_touch != nullptr) {
        delete m_touch;
        m_touch = nullptr;
    }

    if (m_heartbeatThread != nullptr) {
        if (m_heartbeatController != nullptr) {
            m_heartbeatController->StopHandle();
        }
        m_heartbeatThread->Exit();
        delete m_heartbeatThread;
        m_heartbeatThread = nullptr;
    }

    if (m_heartbeatController != nullptr) {
        delete m_heartbeatController;
        m_heartbeatController = nullptr;
    }

    if (m_controlThread != nullptr) {
        m_controlThread->Exit();
        delete m_controlThread;
        m_controlThread = nullptr;
    }

    if (m_cmdController != nullptr) {
        delete m_cmdController;
        m_cmdController = nullptr;
    }

    if (m_streamBuildSender != nullptr) {
        delete m_streamBuildSender;
        m_streamBuildSender = nullptr;
    }

    if (m_casClientSocket != nullptr) {
        delete m_casClientSocket;
        m_casClientSocket = nullptr;
    }

#if MTRANS_ENABLED
    if (m_mtrans != nullptr) {
        m_mtrans->Stop();
    }
#endif
    m_isMTransValid = false;

    INFO("Succeed to destroy workers");
    return true;
}

bool CasController::BuildConnection()
{
    NotifyCommand(CAS_CONNECTING, CasMsgCode::GetMsg(CAS_CONNECTING));
    int connectRes = -2;
    int retry = 0;
    while ((connectRes == -1 || connectRes == -2) && retry < 3) {
        retry++;
        INFO("Connect times: %d", retry);
        connectRes = m_casClientSocket->Connect();
        if (connectRes >= 0) {
            break;
        } else {
            usleep(500000);
        }
    }

    if (connectRes == -1) {
        this->SetState(CONNECTION_FAILURE);
        NotifyCommand(CAS_SERVER_UNREACHABLE, CasMsgCode::GetMsg(CAS_SERVER_UNREACHABLE));
        return false;
    } else if (connectRes == -2) {
        this->SetState(CONNECTION_FAILURE);
        NotifyCommand(CAS_RESOURCE_IN_USING, CasMsgCode::GetMsg(CAS_RESOURCE_IN_USING));
        return false;
    }
    NotifyCommand(CAS_CONNECT_SUCCESS, CasMsgCode::GetMsg(CAS_CONNECT_SUCCESS));
    this->SetState(CONNECTED);
    return true;
}

bool CasController::SetMediaConfig(map<string, string> mediaConfig)
{
    if (!IsValidMediaConfig(mediaConfig)) {
        ERR("Media config is invalid");
        return false;
    }

    if (this->GetState() == INIT || this->GetState() == STOPPED) {
        INFO("Init media config");
        this->m_mediaConfig = mediaConfig;
        return true;
    }

    std::string setMediaConfigCmd = CMD_SET_MEDIA_CONFIG;
    string mediaConfigStr = CasAppCtrlCmdUtils::MakeCommand(mediaConfig, SUB_COMMAND_SEPARATOR);
    map<string, string> parameters = {
        { KEY_COMMAND, setMediaConfigCmd },
        { KEY_MEDIA_CONFIG, mediaConfigStr },
    };
    bool res = SendCommand(parameters);
    if (!res) {
        ERR("Failed to send set media config command");
        return false;
    }
    return true;
}

bool CasController::SendCommand(map<string, string> parameters)
{
    if ((m_casClientSocket == nullptr) || (m_casClientSocket->GetStatus() != SOCKET_STATUS_RUNNING)) {
        ERR("Failed to send command because socket status not running.");
        return false;
    }
    bool sendCmdRes = m_cmdController->SendCtrlCmd(parameters);
    if (!sendCmdRes) {
        ERR("Failed to send command.");
        return false;
    }
    return true;
}

bool CasController::InitDataStream()
{
    m_audioPacketStream = new (std::nothrow) CasDataPipe();
    if (m_audioPacketStream == nullptr) {
        ERR("Failed to new audio packet stream.");
        return false;
    }

    m_videoPacketStream = new (std::nothrow) CasExtVideoDataPipe();
    if (m_videoPacketStream == nullptr) {
        ERR("Failed to new video packet stream.");
        return false;
    }

    m_orientationStream = new (std::nothrow) CasDataPipe();
    if (m_orientationStream == nullptr) {
        ERR("Failed to new orientation packet stream.");
        return false;
    }

    m_controlStream = new (std::nothrow) CasDataPipe();
    if (m_controlStream == nullptr) {
        ERR("Failed to new control packet stream.");
        return false;
    }

    m_channelStream = new (std::nothrow) CasDataPipe();
    if (m_channelStream == nullptr) {
        ERR("Failed to new channel packet stream.");
        return false;
    }

    m_virtualDeviceStream = new (std::nothrow) CasDataPipe();
    if (m_virtualDeviceStream == nullptr) {
        ERR("Failed to new virtual device packet stream.");
        return false;
    }

    m_imeDataStream = new (std::nothrow) CasDataPipe();
    if (m_imeDataStream == nullptr) {
        ERR("Failed to new ime data packet stream.");
        return false;
    }

    return true;
}

bool CasController::ClearDataStream()
{
    if (m_audioPacketStream != nullptr) {
        m_audioPacketStream->Clear();
    }

    if (m_videoPacketStream != nullptr) {
        m_videoPacketStream->Clear();
    }

    if (m_controlStream != nullptr) {
        m_controlStream->Clear();
    }

    if (m_orientationStream != nullptr) {
        m_orientationStream->Clear();
    }

    if (m_channelStream != nullptr) {
        m_channelStream->Clear();
    }

    if (m_virtualDeviceStream != nullptr) {
        m_virtualDeviceStream->Clear();
    }

    if (m_imeDataStream != nullptr) {
        m_imeDataStream->Clear();
    }

    INFO("Succeed to clear data stream ");
    return true;
}

bool CasController::CloseDataStream()
{
    if (m_audioPacketStream != nullptr) {
        delete m_audioPacketStream;
        m_audioPacketStream = nullptr;
    }

    if (m_videoPacketStream != nullptr) {
        delete m_videoPacketStream;
        m_videoPacketStream = nullptr;
    }

    if (m_controlStream != nullptr) {
        delete m_controlStream;
        m_controlStream = nullptr;
    }

    if (m_orientationStream != nullptr) {
        delete m_orientationStream;
        m_orientationStream = nullptr;
    }

    if (m_channelStream != nullptr) {
        delete m_channelStream;
        m_channelStream = nullptr;
    }

    if (m_imeDataStream != nullptr) {
        delete m_imeDataStream;
        m_imeDataStream = nullptr;
    }

    INFO("Succeed to close data stream ");
    return true;
}

uint64_t CasController::GetLag()
{
    if (m_heartbeatThread != nullptr) {
        return m_heartbeatThread->GetLag();
    }
    return 0;
}

// 由于mtrans中，TouchInput，KeyEventInput，MotionEventInput数据通过一个stream流传递，因此传输此类Input数据时需要传递消息头用于区分输入类型
bool CasController::SendInputData(stream_msg_head_t msgHead, uint8_t *pkgdata) {
    size_t pkgLen = static_cast<size_t>(msgHead.GetPayloadSize());
    size_t dataLen = sizeof(stream_msg_head_t) + pkgLen;

    char *outBuffer = (char *)malloc(dataLen);
    if (outBuffer == nullptr) {
        ERR("Failed to malloc out buffer, InputData type: %d.", (int)msgHead.type);
        return false;
    }

    if (EOK != memcpy_s(outBuffer, dataLen, &msgHead, sizeof(stream_msg_head_t))) {
        ERR("Copy msg head fail, InputData type: %d.", (int)msgHead.type);
        free(outBuffer);
        return false;
    }
    if (EOK != memcpy_s(outBuffer + sizeof(stream_msg_head_t), pkgLen, pkgdata, pkgLen)) {
        ERR("Copy msg data fail. InputData type: %d.", (int)msgHead.type);
        free(outBuffer);
        return false;
    }

    int ret;
    switch (msgHead.type) {
        case (TouchInput):{
            // 根据不同的action进行混合可靠性处理
            TouchEventParam *touchEventParam = reinterpret_cast<TouchEventParam*>(pkgdata);
            int action = touchEventParam->action;
            ret = m_mtrans->SendTouchEventData(reinterpret_cast<uint8_t *>(outBuffer), dataLen, action);
        }
            break;
        case (KeyEventInput):
            ret = m_mtrans->SendKeyEventData(reinterpret_cast<uint8_t *>(outBuffer), dataLen);
            break;
        case (MotionEventInput):
            ret = m_mtrans->SendMotionEventData(reinterpret_cast<uint8_t *>(outBuffer), dataLen);
            break;
        default:
            break;
    }
    if (ret != static_cast<int>(dataLen)) {
        ERR("Failed to send InputEvent %d, aimed to send dataLen:%d, but actually send:%d", (int)msgHead.type, (int)dataLen, ret);
        free(outBuffer);
        return false;
    }
    free(outBuffer);
    return true;
}

bool CasController::SendTouchEvent(int id, int action, int x, int y, int pressure, long time, int orientation, int height, int width)
{
    int new_time = (int) time;
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Failed to send touch event because phone already stop.");
        return false;
    }
    if (m_casClientSocket == nullptr || m_casClientSocket->GetStatus() != SOCKET_STATUS_RUNNING) {
        ERR("Failed to send touch event because socket status not running.");
        return false;
    }

#if MTRANS_ENABLED
    if (m_mtrans != nullptr && m_isMTransValid) {
        size_t pkgLen = sizeof(TouchEventParam);
        stream_msg_head_t msgHead;
        msgHead.checksum = CAS_MSG_CHECKSUM_TOUCH_INPUT;
        msgHead.SetPayloadSize(pkgLen);
        msgHead.magicword = CAS_STREAM_DELIMITER_MAGICWORD;
        msgHead.type = TouchInput;

        TouchEventParam touchEventParam{};
        touchEventParam.id = static_cast<uint8_t>(id);
        touchEventParam.action = static_cast<uint8_t>(action);
        touchEventParam.x = (unsigned short)htons(x);
        touchEventParam.y = (unsigned short)htons(y);
        touchEventParam.pressure = (unsigned short)htons(pressure);
        touchEventParam.time = htonl(time);
        touchEventParam.orientation = static_cast<uint8_t>(orientation);
        touchEventParam.height = (unsigned short)htons(height);
        touchEventParam.width = (unsigned short)htons(width);

        size_t dataLen = sizeof(stream_msg_head_t) + pkgLen;
        return SendInputData(msgHead, reinterpret_cast<uint8_t*>(&touchEventParam));
    }
#endif

    if (m_touch == nullptr) {
        return false;
    }
    return m_touch->SendTouchEvent(id, action, x, y, pressure, new_time, orientation, height, width);
}

bool CasController::SendKeyEvent(uint16_t keycode, uint16_t action)
{
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Failed to send key event because phone already stop.");
        return false;
    }
    if (m_casClientSocket == nullptr || m_casClientSocket->GetStatus() != SOCKET_STATUS_RUNNING) {
        ERR("Failed to send key event because socket status not running.");
        return false;
    }

#if MTRANS_ENABLED
    if (m_mtrans != nullptr && m_isMTransValid) {
        size_t pkgLen = sizeof(KeyEventParam);
        stream_msg_head_t msgHead;
        msgHead.checksum = CAS_MSG_CHECKSUM_KEYEVENTINPUT;
        msgHead.SetPayloadSize(pkgLen);
        msgHead.magicword = CAS_STREAM_DELIMITER_MAGICWORD;
        msgHead.type = KeyEventInput;

        KeyEventParam keyEventParam{};
        keyEventParam.keycode = (unsigned short) htons(keycode);
        keyEventParam.action = (unsigned short) htons(action);

        size_t dataLen = sizeof(stream_msg_head_t) + pkgLen;
        return SendInputData(msgHead, reinterpret_cast<uint8_t*>(&keyEventParam));
    }
#endif

    if (m_touch == nullptr) {
        return false;
    }
    return m_touch->SendKeyEvent(keycode, action);
}

bool CasController::SendMotionEvent(uint16_t masterAxis, int32_t masterValue, uint16_t secondaryAxis,
    int32_t secondaryValue)
{
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Failed to send motion event because phone already stop.");
        return false;
    }
    if (m_casClientSocket == nullptr || m_casClientSocket->GetStatus() != SOCKET_STATUS_RUNNING) {
        ERR("Failed to send motion event because socket status not running.");
        return false;
    }

#if MTRANS_ENABLED
    if (m_mtrans != nullptr && m_isMTransValid) {
        size_t pkgLen = sizeof(MotionEventParam);
        stream_msg_head_t msgHead;
        msgHead.checksum = CAS_MSG_CHECKSUM_MOTIONEVENTINPUT;
        msgHead.SetPayloadSize(pkgLen);
        msgHead.magicword = CAS_STREAM_DELIMITER_MAGICWORD;
        msgHead.type = MotionEventInput;

        MotionEventParam motionEventParam{};
        motionEventParam.masterAxis = masterAxis;
        motionEventParam.secondaryAxis = secondaryAxis;
        motionEventParam.masterValue = masterValue;
        motionEventParam.secondaryValue = secondaryValue;

        size_t dataLen = sizeof(stream_msg_head_t) + pkgLen;
        return SendInputData(msgHead, reinterpret_cast<uint8_t*>(&motionEventParam));
    }
#endif

    if (m_touch == nullptr) {
        return false;
    }

    return m_touch->SendMotionEvent(masterAxis, masterValue, secondaryAxis, secondaryValue);
}

int CasController::JniSendData(CasMsgType type, uint8_t *data, int length)
{
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
#if MTRANS_ENABLED
    if (m_mtrans != nullptr && m_isMTransValid) {
        switch (type) {
            case (VirtualCamera):
                return length == m_mtrans->SendVideoData(data, length, "h264");
            case (VirtualMicrophone):
                return length == m_mtrans->SendAudioData(data, length);
            case (VirtualSensor):
                return length == m_mtrans->SendSensorData(data, length);
            case (VirtualLocation):
                return length == m_mtrans->SendLocationData(data, length);
            default:
                break;
        }
    }
#endif

    if (m_streamBuildSender == nullptr) {
        return -1;
    }
    return length == m_streamBuildSender->SendDataToServer(type, data, length);
}

int CasController::JniRecvData(CasMsgType type, uint8_t *data, int length)
{
    void *pPkt = nullptr;
    bool isGetOrientation = false;
    switch (type) {
        case CasMsgType::Audio:
            if (m_audioPacketStream != nullptr) {
                pPkt = m_audioPacketStream->GetNextPkt();
            }
            break;
        case CasMsgType::Orientation:
            if (m_orientationStream != nullptr) {
                pPkt = m_orientationStream->GetNextPkt();
                isGetOrientation = true;
            }
            break;
        case CasMsgType::Channel:
            if (m_channelStream != nullptr) {
                pPkt = m_channelStream->GetNextPkt();
            }
            break;
        case CasMsgType::VirtualDevice:
            if (m_virtualDeviceStream != nullptr) {
                pPkt = m_virtualDeviceStream->GetNextPkt();
            }
            break;
        case CasMsgType::ImeData:
            if (m_imeDataStream != nullptr) {
                pPkt = m_imeDataStream->GetNextPkt();
            }
            break;
        default:
            ERR("Invalid type %d, length %d.", type, length);
            return 0;
    }

    if (pPkt == nullptr) {
        DBG("Packet is null.");
        return 0;
    }
    stream_msg_head_t *streamMsgHead = (stream_msg_head_t *)pPkt;
    unsigned int dataLen = streamMsgHead->GetPayloadSize();
    char *pPayload = (char *)pPkt + sizeof(stream_msg_head_t);

    if ((unsigned int)length < dataLen) {
        ERR("Input buffer is not large enough, type %d free %d payload %u", streamMsgHead->type, length, dataLen);
        if (pPkt != nullptr) {
            free(pPkt);
            pPkt = nullptr;
        }
        return 0;
    }

    errno_t ret = memcpy_s(data, dataLen, pPayload, dataLen);
    if (EOK != ret) {
        ERR("Failed to copy ret = %d, dataLen = %u.", ret, dataLen);
        if (pPkt != nullptr) {
            free(pPkt);
            pPkt = nullptr;
        }
        return 0;
    }

    if (isGetOrientation) {
        IsNeedRotation((int) (*pPayload));
    }

    if (pPkt != nullptr) {
        free(pPkt);
        pPkt = nullptr;
    }

    return dataLen;
}

void CasController::IsNeedRotation(int orientation)
{
    INFO("ORIENTATION is %d, old ORIENTATION is %d.", orientation, m_orientation);

    if (orientation == 8) {
        m_rotationDegrees = 270;
    } else if (orientation == 24) {
        m_rotationDegrees = 90;
    } else if (orientation == 16) {
        m_rotationDegrees = 180;
    } else {
        m_rotationDegrees = 0;
    }

    ResetDecoder(m_isNotifyFirstFrame);
    ForceIFrame();
    m_orientation = orientation;
}

bool CasController::GetConnectStatus()
{
#if defined(RECONNECT) || (SOCKET_RECONNECT)
    if (m_casClientSocket != nullptr) {
        int status = m_casClientSocket->GetStatus();
        if (status == SOCKET_STATUS_RUNNING) {
            return true;
        }
    }
    return false;
#else
    // always return true to avoid reconnect dialog
    return true;
#endif
}

void CasController::ProcessEnterBackground()
{
    if (m_sessionId.empty()) {
        ERR("SessionId is empty.");
        return;
    }

    string pauseCmd = CMD_PAUSE;
    map<string, string> parameters = { { KEY_COMMAND, pauseCmd }, { KEY_SESSION_ID, m_sessionId } };

    SendCommand(parameters);
    StopDecWorker(!m_retainVideoDecode);
    ClearDataStream();
}

bool CasController::ProcessEnterForeground(ANativeWindow *nativeWindow)
{
    ClearDataStream();

    if (m_sessionId.empty()) {
        ERR("SessionId is empty.");
        return false;
    }

    std::string resumeCmd = CMD_RESUME;
    map<string, string> parameters = { { KEY_COMMAND, resumeCmd }, { KEY_SESSION_ID, m_sessionId } };

    bool res = SendCommand(parameters);
    if (!res) {
        ERR("Failed to send resume command");
        return false;
    }
    return true;
}

bool CasController::CreateDecWorker(ANativeWindow *nativeWindow, bool needVideoDecode)
{
    std::lock_guard<std::mutex> lockGuard(this->m_decoderLock);
    if (needVideoDecode) {
        m_videoDecodeThread = new (std::nothrow) CasVideoHDecodeThread(nativeWindow, m_frameType, m_rotationDegrees);
        if (m_videoDecodeThread == nullptr) {
            ERR("Failed to new video decode thread.");
            return false;
        }
        m_videoDecodeThread->SetDecodePktHandle(m_videoPacketStream);
    }
    return true;
}

void CasController::StartDecWorker(bool retainVideoDecode)
{
    std::lock_guard<std::mutex> lockGuard(this->m_decoderLock);
    if (retainVideoDecode) {
        if (m_videoDecodeThread != nullptr) {
            m_videoDecodeThread->Restart();
        }
    } else {
        m_videoDecodeThread->Start();
    }
}

void CasController::StopDecWorker(bool retainVideoDecode)
{
    std::lock_guard<std::mutex> lockGuard(this->m_decoderLock);
    if (m_videoDecodeThread == nullptr) {
        return;
    }

    if (retainVideoDecode) {
        m_videoDecodeThread->Stop();
    } else {
        m_videoDecodeThread->Exit();
        delete m_videoDecodeThread;
        m_videoDecodeThread = nullptr;
    }
}

bool CasController::ForceIFrame()
{
    std::lock_guard<std::mutex> lockGuard(this->m_lock);
    if (this->GetState() == STOPPED) {
        INFO("Force IFrame failed because phone already stop.");
        return false;
    }

    if (m_casClientSocket == nullptr || m_casClientSocket->GetStatus() != SOCKET_STATUS_RUNNING) {
        ERR("Force IFrame failed because socket status not running.");
        return false;
    }

    map<string, string> parameters = { { KEY_COMMAND, CMD_REQ_IFRAME }, { KEY_SESSION_ID, m_sessionId } };
    bool res = SendCommand(parameters);
    if (!res) {
        ERR("Send Command request i frame failed.");
        return false;
    }
    return true;
}

void CasController::NotifyCommand(int type, string msg)
{
    std::lock_guard<std::mutex> lockGuard(this->m_callbackLock);
    if (cmdCallBack != nullptr) {
        cmdCallBack(type, std::move(msg));
    }
}

void CasController::NotifyFirstVideoFrame()
{
    std::lock_guard<std::mutex> lockGuard(this->m_callbackLock);
    if (!m_isNotifyFirstFrame) {
        m_isNotifyFirstFrame = true;
        cmdCallBack(CAS_FIRST_FRAME, CasMsgCode::GetMsg(CAS_FIRST_FRAME));
    }
}

void CasController::NotifyBitrate(uint32_t bitrate)
{
    bitrateCallBack(bitrate);
}

void CasController::OnCmdRecv(int code, string msg)
{
    if (code == CAS_H265_NOT_SUPPORT) {
        m_frameType = FrameType::H264;
        m_mediaConfig[KEY_FRAME_TYPE] = "h264";
        if (!SendStartCmd()) {
            this->SetState(START_FAILURE);
            NotifyCommand(CAS_ENGINE_START_FAILED, CasMsgCode::GetMsg(CAS_ENGINE_START_FAILED));
            ERR("Failed to send start command");
        }
    } else {
        NotifyCommand(code, msg);
    }
}

void CasController::ResetDecoder(bool isClearStream)
{
    StopDecWorker(false);
    if (isClearStream) {
        if (m_videoPacketStream != nullptr) {
            m_videoPacketStream->Clear();
        }
    }
    CreateDecWorker(m_nativeWindow, m_needVideoDecode);
    StartDecWorker(false);
}

bool CasController::SendStartCmd()
{
    std::string mediaConfigStr = "";
    if (IsValidMediaConfig(m_mediaConfig)) {
        mediaConfigStr = CasAppCtrlCmdUtils::MakeCommand(m_mediaConfig, SUB_COMMAND_SEPARATOR);
    }
    std::string startCmd = CMD_START_APP;
    map<string, string> parameters = { { KEY_COMMAND, startCmd },
                                       { KEY_TICKET, m_ticket },
                                       { KEY_AUTH_TS, m_authTs },
                                       { KEY_VERIFY_DATA, m_verifyData },
                                       { KEY_ENCRYPTED_DATA, m_encryptedData },
                                       { KEY_SESSION_ID, m_sessionId },
                                       { KEY_AES_IV, m_aesIv },
                                       { KEY_SDK_VERSION, m_conf.sdkVersion },
                                       { KEY_PROTOCOL_VERSION, m_conf.protocolVersion },
                                       { KEY_CLIENT_TYPE, m_clientType },
                                       { KEY_CLIENT_MODE, m_conf.clientMode },
                                       { KEY_REGION_ID, m_conf.regionId },
                                       { KEY_MEDIA_CONFIG, mediaConfigStr },
                                       { KEY_MAX_DISCONNECT_DURATION, m_maxDisconnectDuration } };

    return SendCommand(parameters);
}

void CasController::RecvdVideoData(uint8_t *data, int length)
{
    if (m_videoPacketStream != nullptr) {
        uint8_t *videoData = new uint8_t[length];
        memcpy(videoData, data, length);
        m_videoPacketStream->Handle(videoData);
        CalculateFPS();
    }
    m_isMTransValid = true;
}

void CasController::RecvdAudioData(uint8_t *data, int length)
{
    if (m_audioPacketStream != nullptr) {
        uint8_t *audioData = new uint8_t[length];
        memcpy(audioData, data, length);
        m_audioPacketStream->Handle(audioData);
    }
}

bool CasController::IsMtransValid()
{
    return m_isMTransValid;
}

int32_t OnRecvVideoStreamData(uint8_t* data, uint32_t length)
{
    CasController::GetInstance()->RecvdVideoData(data, length);
    return 0;
}

int32_t OnRecvAudioStreamData(uint8_t* data, uint32_t length)
{
    int headerLen = 36;

    stream_msg_head_t msgHead;
    msgHead.size = length;
    msgHead.checksum = CAS_MSG_CHECKSUM_AUDIO;
    msgHead.magicword = CAS_STREAM_DELIMITER_MAGICWORD;
    msgHead.type = Audio;
    msgHead.SetPayloadSize(length);

    size_t dataLen = sizeof(stream_msg_head_t) + length;
    char *outBuffer = (char *)malloc(dataLen);
    if (outBuffer == nullptr) {
        ERR("Failed to malloc out buffer.");
        return 0;
    }
    if (EOK != memcpy_s(outBuffer, dataLen, &msgHead, sizeof(stream_msg_head_t))) {
        ERR("Copy msg head fail.");
        free(outBuffer);
        return 0;
    }
    if (EOK != memcpy_s(outBuffer + sizeof(stream_msg_head_t), dataLen - sizeof(stream_msg_head_t), data, length)) {
        ERR("Copy msg data fail.");
        free(outBuffer);
        return 0;
    }

    CasController::GetInstance()->RecvdAudioData(reinterpret_cast<uint8_t *>(outBuffer), dataLen);
    return 0;
}

int32_t OnRecvAudioDecodeCallback(int32_t streamId, AudioJbDecode* audioJbDecode)
{
    if (audioJbDecode->inputLength > 0) {
        g_plcCount = 0;
        int opusSize = 240;
        int pcmLen = opus_decode(g_opusDecoder, audioJbDecode->payload + 8, opusSize, audioJbDecode->outputData, 480, 0);
        audioJbDecode->frameType = 1;
        audioJbDecode->outputLength = pcmLen * 2;
    } else {
        int pcmLen = opus_decode(g_opusDecoder, audioJbDecode->payload, 0, audioJbDecode->outputData, 480, 1);
        audioJbDecode->frameType = 1;
        audioJbDecode->outputLength = pcmLen * 2;
        if (g_plcCount >= 4) {
            errno_t et = memset_s(audioJbDecode->outputData, audioJbDecode->outputLength * 2, 0, audioJbDecode->outputLength * 2);
            if (et != EOK) {
                ERR("plc memset 0 error, err : %d", et);
            }
        }
        g_plcCount++;
        if (g_plcCount > 100) {
            g_plcCount = 4;
        }
    }
    return 0;
}

void OnGotTransLog(const char* str, uint32_t length)
{
    if (hrtpLogger == nullptr) {
        return;
    }
    hrtpLogger->info(str);
}

void OnRecvBitrate(uint32_t encBitrate, uint32_t totalBitrate)
{
    uint32_t mtransEncBitrate = encBitrate * 1000;
    CasController::GetInstance()->NotifyBitrate(mtransEncBitrate);
}

void OnNeedKeyFrameCallback()
{
    INFO("Need key frame callback..");
    CasController::GetInstance()->NotifyCommand(CAS_REQUEST_CAMERA_KEY_FRAME, "camera need key frame");
}

int32_t OnRecvCmdData(uint8_t* data, uint32_t length)
{
    INFO("OnRecvCmdData");
    CasController::GetInstance()->HandleCmdData(reinterpret_cast<uint8_t *>(data), length);
    return 0;
}

void CasController::HandleCmdData(uint8_t *data, int length)
{
    streamMsgHead *msgHead = (streamMsgHead *)data;
    INFO("Cmd callback size = %d, type = %d", length, msgHead->type);
    if (msgHead->type <= Invalid || msgHead->type >= End) {
        ERR("msgHead type is invalid : %d.", msgHead->type);
        return;
    }
    CasPktHandle *serviceHandle = m_streamParser->GetServiceHandle(msgHead->type);
    if (serviceHandle) {
        void *pTmp = AllocBuffer(length);
        if (pTmp != nullptr) {
            if (EOK != memcpy_s(pTmp, length, data, length)) {
                ERR("Memory copy data fail.");
                return;
            }
            serviceHandle->Handle((void *)pTmp);
        }
    }
}

std::string CasController::GetSimpleRecvStats() {
    std::string statsString;
    std::stringstream stream;

    uint64_t lag = GetLag() / 1000;
    if (lag >= 600) {
        stream << "600+ms";
    } else {
        stream << lag << "ms";
    }

#if MTRANS_ENABLED
    if (m_mtrans != nullptr && m_isMTransValid) {
        StreamRecvStats stats{};
        if (0 != m_mtrans->GetVideoRecvStats(&stats)) {
            WARN("GetSimpleRecvStats failed");
        } else {
            stream << " " << stats.recvBitrate << "kbps";
        }
    }
#endif

    stream << " " << GetCurrentFPS() << "FPS";
    statsString = stream.str();
    return statsString;
}

std::string CasController::GetVideoRecvStats() {
    std::string statsString;
    std::stringstream stream;
#if MTRANS_ENABLED
    if (m_mtrans != nullptr && m_isMTransValid) {

        uint64_t lag = GetLag() / 1000;
        stream << "网络时延 : ";
        if (lag != 0) {
            if (lag >= 600) {
                stream << "600+ms";
            } else {
                stream << lag << "ms";
            }
        }
        stream << std::endl;

        StreamRecvStats stats{};
        if (0 != m_mtrans->GetVideoRecvStats(&stats)) {
            WARN("GetVideoRecvStats failed");
        } else {
            stream << "下行视频丢包 : " << stats.lostRate << std::endl;
            stream << "视频接收码率 : " << stats.recvBitrate << "kbps" << std::endl;
        }
        StreamRecvStats audioStats{};
        if (0 != m_mtrans->GetAudioRecvStats(&audioStats)) {
            WARN("GetAudioRecvStats failed");
        } else {
            stream << "音频接收码率 : " << audioStats.recvBitrate << "kbps" << std::endl;
        }
        StreamSendStats cmdSendStats{};
        if (0 != m_mtrans->GetCmdSendStats(&cmdSendStats)) {
            WARN("GetCmdSendStats failed");
        } else {
            stream << "指令发送码率(需操作) : " << cmdSendStats.encBitrate << "kbps" << std::endl;
        }
    }
#endif
    stream << "帧率 : " << GetCurrentFPS() << "fps" << std::endl;
    stream << "策略丢帧 : " << CasVideoUtil::GetInstance()->GetCurrentDropFPS() << "fps" << std::endl;

    statsString = stream.str();
    return statsString;
}

void CasController::CalculateFPS() {
    uint64_t currentTime = CasVideoUtil::GetInstance()->GetNow();

    m_videoDataCount++;

    if (m_lastTimeRefreshFPS == 0) {
        m_lastTimeRefreshFPS = currentTime;
    }
    if (currentTime - m_lastTimeRefreshFPS > DURATION_USEC) {
        m_currentFPS = m_videoDataCount;
        m_videoDataCount = 0;
        m_lastTimeRefreshFPS = currentTime;
    }
}

int CasController::GetCurrentFPS() const {
    uint64_t currentTime = CasVideoUtil::GetInstance()->GetNow();
    if (currentTime - m_lastTimeRefreshFPS > 2 * DURATION_USEC) {
        return 0;
    }
    return m_currentFPS;
}