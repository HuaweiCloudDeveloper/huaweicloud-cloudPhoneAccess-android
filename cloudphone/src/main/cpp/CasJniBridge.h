// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_JNIRENDER_H
#define CLOUDAPPSDK_JNIRENDER_H

#include <jni.h>

#define JNI(func) Java_com_huawei_cloudphone_jniwrapper_JNIWrapper_##func

extern "C" {
JNIEXPORT void JNICALL JNI(setJniConf)(JNIEnv *env, jclass, jstring key, jstring value);

JNIEXPORT jboolean JNICALL JNI(start)(JNIEnv *env, jclass, jobject surface, jboolean isHome);

JNIEXPORT jboolean JNICALL JNI(reconnect)(JNIEnv *env, jclass);

JNIEXPORT void JNICALL JNI(stop)(JNIEnv *env, jclass, jboolean isHome);

JNIEXPORT jobject JNICALL JNI(getBitmap)(JNIEnv *env, jclass);

JNIEXPORT int JNICALL JNI(getJniStatus)(JNIEnv *env, jclass);

JNIEXPORT int JNICALL JNI(getLag)(JNIEnv *env, jclass);

JNIEXPORT jstring JNICALL JNI(getVideoStreamStats)(JNIEnv *env, jclass);

JNIEXPORT jstring JNICALL JNI(getSimpleStreamStats)(JNIEnv *env, jclass);

JNIEXPORT jboolean JNICALL JNI(setMediaConfig)(JNIEnv *env, jclass, jobject mediaConfig);

JNIEXPORT jint JNICALL JNI(recvData)(JNIEnv *env, jclass, jbyte type, jbyteArray data, int length);

JNIEXPORT jint JNICALL JNI(sendData)(JNIEnv *env, jclass clazz, jbyte type, jbyteArray data, jint length);

JNIEXPORT jboolean JNICALL JNI(sendTouchEvent)(JNIEnv *env, jclass, int id, int action, int x, int y, int pressure, jlong time,
                                               jint orientation, jint height, jint width);

JNIEXPORT jboolean JNICALL JNI(sendKeyEvent)(JNIEnv *env, jclass, jint keycode, jint action);

JNIEXPORT jboolean JNICALL JNI(sendMotionEvent)(JNIEnv *env, jclass, jint masterAxis, jint masterValue,
    jint secondaryAxis, jint secondaryValue);

JNIEXPORT jboolean JNICALL JNI(setRotation)(JNIEnv *, jclass, int h);

JNIEXPORT jboolean JNICALL JNI(getConnectStatus)(JNIEnv *env, jclass);
}
#endif // CLOUDAPPSDK_JNIRENDER_H