// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASOPUSBRIDGE_H
#define CLOUDAPPSDK_CASOPUSBRIDGE_H

#include <jni.h>

#ifndef _Included_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
#define _Included_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    stringFromJNI
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_stringFromJNI(JNIEnv *, jclass type);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    createOpusDecoder
 * Signature: ()I
 */
JNIEXPORT jlong JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_createOpusDecoder(JNIEnv *, jclass type);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    destroyOpusDecoder
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_destroyOpusDecoder(JNIEnv *, jclass type,
    jlong);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    opusDecode
 * Signature: ([BI[S)I
 */
JNIEXPORT jint JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_opusDecode(JNIEnv *, jclass type, jlong,
    jbyteArray, jint, jshortArray, jint);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    createOpusEncoder
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_createOpusEncoder(JNIEnv *, jclass type,
    jint, jint);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    destroyOpusEncoder
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_destroyOpusEncoder(JNIEnv *, jclass type);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    opusEncode
 * Signature: ([SI[B)I
 */
JNIEXPORT jint JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_opusEncode(JNIEnv *, jclass type,
    jshortArray, jint, jbyteArray);

/*
 * Class:     com_huawei_cloudphone_jniwrapper_OpusJNIWrapper
 * Method:    getOpusEncoderStatus
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_huawei_cloudphone_jniwrapper_OpusJNIWrapper_getOpusEncoderStatus(JNIEnv *, jclass type);

#ifdef __cplusplus
}
#endif
#endif

#endif // CLOUDAPPSDK_CASOPUSBRIDGE_H
