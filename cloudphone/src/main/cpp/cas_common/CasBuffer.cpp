// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cstdlib>
#include <cstddef>
#include "CasBuffer.h"
#include "CasLog.h"

using namespace std;

CasBuffer::CasBuffer(uint8_t *pointer, const size_t bufferSize)
{
    m_pointer = pointer;
    m_bufferSize = bufferSize;
}

uint8_t *CasBuffer::GetPointer()
{
    return m_pointer;
}

size_t CasBuffer::GetSize() const
{
    return m_bufferSize;
}

CasBuffer CasBuffer::Alloc(size_t bufferSize)
{
    CasBuffer buf = {};
    if (bufferSize == 0) {
        buf.m_pointer = nullptr;
    } else {
        buf.m_pointer = reinterpret_cast<uint8_t *>(malloc(bufferSize));
    }
    if (buf.m_pointer != nullptr) {
        buf.m_bufferSize = bufferSize;
    } else {
        buf.m_bufferSize = 0;
    }
    return buf;
}

void CasBuffer::Free()
{
    if (m_pointer != nullptr) {
        free(m_pointer);
        m_pointer = nullptr;
        m_bufferSize = 0;
    }
}

void *AllocBuffer(const unsigned int size)
{
    void *buf = nullptr;
    if (size > 0) {
        buf = malloc(size);
    }
    if (buf == nullptr) {
        ERR("Malloc failed");
    }
    return buf;
}

void FreeBuffer(void *buf)
{
    if (buf != nullptr) {
        free(buf);
        buf = nullptr;
    } else {
        ERR("Free buf failed");
        return;
    }
}
