// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASBUF_H
#define CLOUDAPPSDK_CASBUF_H

#include <cstdlib>
#include <cstdint>

struct CasBuffer {

    CasBuffer() = default;

    CasBuffer(uint8_t *pointer, const size_t bufferSize);

    uint8_t *GetPointer();

    size_t GetSize() const;

    static CasBuffer Alloc(size_t bufferSize);

    void Free();

    uint8_t *m_pointer;
    size_t m_bufferSize;
};

extern void *AllocBuffer(const unsigned int size);

extern void FreeBuffer(void *buf);

#endif // CLOUDAPPSDK_CASBUF_H
