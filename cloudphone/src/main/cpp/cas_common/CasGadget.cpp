// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <securec.h>
#include "CasGadget.h"
#include "CasLog.h"

unsigned int TransIp(const char *ip)
{
    char tmp[255];
    unsigned int rs = 0;
    int len = static_cast<int>(strlen(ip));
    int res = strcpy_s(tmp, sizeof(tmp), ip);
    if (res != EOK) {
        ERR("Failed to trans ip.");
        return 0;
    }

    tmp[len] = '\0';
    int start = 0;
    for (int i = 0; i < len; i++) {
        if (tmp[i] == '.') {
            tmp[i] = '\0';
            rs = (rs << 8) + static_cast<unsigned int>(atoi(tmp + start));
            start = i + 1;
        }
    }
    rs = (rs << 8) + static_cast<unsigned int>(atoi(tmp + start));
    return rs;
}
