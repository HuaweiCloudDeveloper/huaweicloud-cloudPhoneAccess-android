// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASGADGET_H
#define CLOUDAPPSDK_CASGADGET_H

#include <cstring>

unsigned int TransIp(const char *ip);

enum JNIState {
    INIT = 0,
    LIB_INITIALIZED = 1,
    WORKER_THREADS_STARTED = 2,
    CONNECTED = 3,
    START_SUCCESS = 4,
    START_FAILURE = 5,
    START_AUTH_FAILURE = 6,

    DISCONNECTED = 7,
    WORKER_THREADS_STOPPED = 8,
    STOPPED = 9,
    CONNECTION_FAILURE = 10,
#if defined(RECONNECT)
    RESTORE_FAILURE = 11,
    RESTOREING = 12,
    RECONNECT_FAILURE = 13,
#endif
    VALID = 14,
    INVALID = 15,
};

#endif // CLOUDAPPSDK_CASGADGET_H