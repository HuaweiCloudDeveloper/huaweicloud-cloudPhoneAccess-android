// Copyright 2024 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "CasLog.h"
#include "spdlog/sinks/rotating_file_sink.h"

// 最大10M ,最多转储3个文件
static auto max_size = 1048576 * 10;
static auto max_files = 3;
std::shared_ptr<spdlog::logger> sdkLogger = nullptr;

void LogToFile(int level, const char *filename, int line, const char *function, const char* msg)
{
    if (sdkLogger == nullptr) {
        sdkLogger = spdlog::rotating_logger_mt("sdk_logger", "/sdcard/Android/media/com.huawei.cloudapp.b004/sdk_log.txt", max_size, max_files);
    }
    switch (level) {
        case ANDROID_LOG_DEBUG:
            sdkLogger->debug("[{}:{}]:{}:{}", filename, line, function, msg);
            break;
        case ANDROID_LOG_INFO:
            sdkLogger->info("[{}:{}]:{}:{}", filename, line, function, msg);
            break;
        case ANDROID_LOG_WARN:
            sdkLogger->warn("[{}:{}]:{}:{}", filename, line, function, msg);
            break;
        case ANDROID_LOG_ERROR:
            sdkLogger->error("[{}:{}]:{}:{}", filename, line, function, msg);
            break;
        default:
            break;
    }
}