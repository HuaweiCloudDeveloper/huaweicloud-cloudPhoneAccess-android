// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASMSG_H
#define CLOUDAPPSDK_CASMSG_H

#include <endian.h>
#include <cstdint>
#include <sys/time.h>
#include <arpa/inet.h>

enum CasMsgType : uint8_t {
    Invalid = 0,
    Verify = 1,
    Audio = 2,
    Video = 3,
    Channel = 4,
    TouchInput = 6,
    CmdControl = 7,
    HeartBeat = 8,
    Orientation = 9,
    Recorder = 11,
    ImeData = 14,
    KeyEventInput = 15,
    MotionEventInput = 16,
    VirtualDevice = 20,
    VirtualCamera = 21,
    VirtualMicrophone = 22,
    VirtualSensor = 23,
    VirtualLocation = 24,
    End,
};


#define CAS_STREAM_DELIMITER_MAGICWORD 0x5A5A
#define GET_CAS_CHECKSUM(msg_type) ((msg_type + ((CAS_STREAM_DELIMITER_MAGICWORD >> 8) & 0xFF) + (CAS_STREAM_DELIMITER_MAGICWORD & 0xFF)) & 0xFF)

#define CAS_MSG_CHECKSUM_INVALID GET_CAS_CHECKSUM(CasMsgType::Invalid)
#define CAS_MSG_CHECKSUM_AUDIO GET_CAS_CHECKSUM(CasMsgType::Audio)
#define CAS_MSG_CHECKSUM_RECORD GET_CAS_CHECKSUM(CasMsgType::Recorder)
#define CAS_MSG_CHECKSUM_CONTROL GET_CAS_CHECKSUM(CasMsgType::CmdControl)
#define CAS_MSG_CHECKSUM_TOUCH_INPUT GET_CAS_CHECKSUM(CasMsgType::TouchInput)
#define CAS_MSG_CHECKSUM_ORIENTATION GET_CAS_CHECKSUM(CasMsgType::Orientation)
#define CAS_MSG_CHECKSUM_VIDEO GET_CAS_CHECKSUM(CasMsgType::Video)
#define CAS_MSG_CHECKSUM_VERIFY GET_CAS_CHECKSUM(CasMsgType::Verify)
#define CAS_MSG_CHECKSUM_HEARTBEAT GET_CAS_CHECKSUM(CasMsgType::HeartBeat)
#define CAS_MSG_CHECKSUM_IMEDATA GET_CAS_CHECKSUM(CasMsgType::ImeData)
#define CAS_MSG_CHECKSUM_KEYEVENTINPUT GET_CAS_CHECKSUM(CasMsgType::KeyEventInput)
#define CAS_MSG_CHECKSUM_MOTIONEVENTINPUT GET_CAS_CHECKSUM(CasMsgType::MotionEventInput)
#define CAS_MSG_CHECKSUM_CHANNEL GET_CAS_CHECKSUM(CasMsgType::Channel)
#define CAS_MSG_CHECKSUM_VIRTUAL_CAMERA GET_CAS_CHECKSUM(CasMsgType::VirtualCamera)
#define CAS_MSG_CHECKSUM_VIRTUAL_MICROPHONE GET_CAS_CHECKSUM(CasMsgType::VirtualMicrophone)
#define CAS_MSG_CHECKSUM_VIRTUAL_SENSOR GET_CAS_CHECKSUM(CasMsgType::VirtualSensor)
#define CAS_MSG_CHECKSUM_VIRTUAL_LOCATION GET_CAS_CHECKSUM(CasMsgType::VirtualLocation)

// 客户端通用消息头
typedef struct streamMsgHead {
    uint16_t magicword;
    uint8_t checksum;
    CasMsgType type;
    uint32_t size;
    uint32_t GetPayloadSize()
    {
        return (uint32_t)ntohl(size);
    }

    void SetPayloadSize(uint32_t payloadSize)
    {
        this->size = htonl(payloadSize);
    }
} __attribute__((packed)) stream_msg_head_t;

// 客户端触控消息体
typedef struct CasTouchEventMsg {
    uint8_t id;
    uint8_t action;
    uint16_t x;
    uint16_t y;
    uint16_t pressure;
    int32_t time;
    uint8_t orientation;
    uint16_t height;
    uint16_t width;
} __attribute__((packed)) cas_touch_event_msg_t;

// 客户端遥控器指令消息体
typedef struct CasKeyEventMsg {
    uint16_t keycode;
    uint16_t action;
} __packed CasKeyEventMsg;

// 客户端手柄指令消息体
typedef struct CasMotionEventMsg {
    uint16_t masterAxis;
    uint16_t secondaryAxis;
    int32_t masterValue;
    int32_t secondaryValue;
} __packed CasMotionEventMsg;

#endif // CLOUDAPPSDK_CASMSG_H