// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASVIDEOENGINE_H
#define CLOUDAPPSDK_CASVIDEOENGINE_H

#include <cstddef>
#include <mutex>
#include <android/native_window.h>
#include "CasVideoEngineCommon.h"

class CasVideoEngine {
public:
    /*
     * @fn CasVideoEngine
     * @brief constructor
     */
    CasVideoEngine();

    /*
     * @fn CasVideoEngine
     * @brief destructor
     */
    ~CasVideoEngine();

    /*
     * @fn InitDecoder
     * @brief initialisation interface
     * @param[in] nativeWindow View to display of (type <tt>ANativeWindow*</tt>)
     * @param[in] type Method to decode, fixed to DECODER_TYPE_HW, of (type <tt>enum DecoderType</tt>)
     * @return uint32_t, errno: SUCCESS
     *                          VIDEO_ENGINE_CLIENT_INIT_FAIL
     *                          VIDEO_ENGINE_CLIENT_PARAM_INVALID
     *                          VIDEO_ENGINE_CLIENT_PARAM_UNSUPPORTED
     */
    uint32_t InitDecoder(ANativeWindow *nativeWindow, DecoderType type, FrameType frameType, int rotationDegrees);

    /*
     * @fn StartDecoder
     * @brief start interface
     * @return errno: SUCCESS
     *                VIDEO_ENGINE_CLIENT_START_ERR
     */
    uint32_t StartDecoder();

    /*
     * @fn DecodeFrame
     * @brief decode interface
     * @param[in] buf The initial address of current frame of (type <tt>int8_t*</tt>)
     * @param[in] length The length of current frame of (type <tt>size_t</tt>)
     * @return errno: SUCCESS
     *                VIDEO_ENGINE_CLIENT_DECODE_ERR
     *                VIDEO_ENGINE_CLIENT_PARAM_INVALID
     */
    uint32_t DecodeFrame(uint8_t *buf, size_t length);

    /*
     * @fn StopDecoder
     * @brief stop interface
     * @return errno: SUCCESS
     *                VIDEO_ENGINE_CLIENT_STOP_ERR
     */
    uint32_t StopDecoder();

    /*
     * @fn DestroyDecoder
     * @brief destroy interface
     * @return errno: SUCCESS
     *                VIDEO_ENGINE_CLIENT_DESTROY_ERR
     */
    uint32_t DestroyDecoder();

    /*
     * @fn GetDecoderStatus
     * @brief get status interface
     * @param[out] stat The status of the engine decoder, pass by reference, of (type <tt>enum EngineStat</tt>)
     * @return errno: SUCCESS
     *                VIDEO_ENGINE_CLIENT_GET_STATUS_ERR
     */
    uint32_t GetDecoderStatus(EngineStat &status);

    /*
     * @fn GetDecoderStatistics
     * @brief get statistics interface
     * @param[out] stat The statistics of the engine decoder, pass by reference, of (type <tt>struct
     * DecoderStatics</tt>)
     * @return errno: SUCCESS
     *                VIDEO_ENGINE_CLIENT_GET_STAT_ERR
     */
    uint32_t GetDecoderStatistics(DecoderStatistics &statistics);

    /*
     * @fn SetFirstVidFrameCallback
     * @brief get first vid frame callback interface
     */
    void SetFirstVidFrameListener(CasFirstVideoFrameListener *listener);

    /*
     * @fn SetVideoDecodeStatListener
     * @brief get video decode time
     */
    void SetVideoDecodeStatListener(CasVideoDecodeStatListener *listener);

private:
    std::mutex m_lock = {};
};
#endif // CLOUDAPPSDK_CASVIDEOENGINE_H