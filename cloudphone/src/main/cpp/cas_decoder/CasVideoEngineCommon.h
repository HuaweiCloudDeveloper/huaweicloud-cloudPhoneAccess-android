// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASVIDEOENGINECOMMON_H
#define CLOUDAPPSDK_CASVIDEOENGINECOMMON_H

#include <cstdint>

// CasDecoder Type
enum class DecoderType {
    DECODER_TYPE_HW = 0
};

// CasDecoder FrameType
enum class FrameType {
    H264 = 0,
    H265
};

// Video Engine Status
enum class EngineStat {
    ENGINE_INIT = 0x01,
    ENGINE_RUNNING = 0x02,
    ENGINE_STOP = 0x03,
    ENGINE_INVALID = 0x0
};

// Video Engine CasDecoder Statistics
struct DecoderStatistics {
    uint32_t decodeFps;
};

// errno
const uint32_t SUCCESS = 0;
const uint32_t VIDEO_ENGINE_CLIENT_INIT_FAIL = 0x0A070001;
const uint32_t VIDEO_ENGINE_CLIENT_PARAM_INVALID = 0x0A070002;
const uint32_t VIDEO_ENGINE_CLIENT_PARAM_UNSUPPORTED = 0x0A070003;
const uint32_t VIDEO_ENGINE_CLIENT_START_ERR = 0x0A070004;
const uint32_t VIDEO_ENGINE_CLIENT_GET_STAT_ERR = 0x0A070005;
const uint32_t VIDEO_ENGINE_CLIENT_DECODE_ERR = 0x0A070006;
const uint32_t VIDEO_ENGINE_CLIENT_STOP_ERR = 0x0A070007;
const uint32_t VIDEO_ENGINE_CLIENT_DESTROY_ERR = 0x0A070008;
const uint32_t VIDEO_ENGINE_CLIENT_GET_STATUS_ERR = 0x0A070009;
const uint32_t VIDEO_ENGINE_CLIENT_SDK_UNSUPPORTED = 0x0A070010;

class CasFirstVideoFrameListener {
public:
    virtual void OnFirstFrame() = 0;
};

class CasVideoDecodeStatListener {
public:
    virtual void OnDecodeOneFrame(uint64_t decTime) = 0;
};

#endif // CLOUDAPPSDK_CASVIDEOENGINECOMMON_H