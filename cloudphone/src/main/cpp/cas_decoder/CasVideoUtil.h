// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASVIDEOUTIL_H
#define CLOUDAPPSDK_CASVIDEOUTIL_H

#include <chrono>
#include <cstdint>
#include "CasItemQueue.h"

class CasVideoUtil {
public:
    /*
     * @fn GetInstance
     * @brief to get CasVideoUtil singleton
     */
    static CasVideoUtil *GetInstance();

    /*
     * @fn DestroyInstance
     * @brief to release CasVideoUtil singleton
     */
    static void DestroyInstance();

    /*
     * @fn Init
     * @brief to Init CasVideoUtil resource
     */
    bool Init();

    /*
     * @fn getNow
     * @brief to get the time in microsecond this moment.
     * @return uint64_t, the time in microsecond this moment
     */
    uint64_t GetNow();

    /*
     * @fn SetFps
     * @brief to push latest frame TS back into queue
     * @param[in] latest timestamp of (type <tt>timestamp_t</tt>)
     */
    void SetTimestamp(uint64_t timestamp);

    /*
     * @fn GetFps
     * @brief to get the realtime fps
     * @return uint32_t, fps
     */
    uint32_t GetFps();

    /*
     * @fn DropOneFrame
     * @brief count when drop one frame
     */
    void DropOneFrame();

    /*
     * @fn GetCurrentDropFPS
     * @brief get drop FPS
     */
    int GetCurrentDropFPS();

private:
    /*
     * @brief: construct
     */
    CasVideoUtil();

    /*
     * @brief: deconstruct
     */
    ~CasVideoUtil();

    /*
     * @fn Release
     * @brief to release CasVideoUtil resource
     */
    void Release() noexcept;

    std::mutex m_lock = {};
    static CasVideoUtil *g_instance;
    static std::mutex g_instanceLock;
    CasItemQueue<uint64_t> *m_frameQueue = nullptr;
    int m_VideoDropCount = 0;
    int m_VideoDropFPS = 0;
    uint64_t m_lastTimeRefreshDropFPS = 0;
};
#endif // CLOUDAPPSDK_CASVIDEOUTIL_H