// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASAPPCTRLCMDUTILS_H
#define CLOUDAPPSDK_CASAPPCTRLCMDUTILS_H

#include <string>
#include <map>

const std::string COMMAND_SEPARATOR = "&";
const std::string SUB_COMMAND_SEPARATOR = ":";
const std::string KEY_COMMAND = "command";
const std::string KEY_RESULT = "result";
const std::string KEY_CODE = "code";
const std::string KEY_MSG = "msg";
const std::string CMD_HEARTBEAT_REQUEST = "0";
const std::string CMD_START_APP = "1";
const std::string CMD_STOP_APP = "2";
const std::string CMD_RECONNECT = "3";
const std::string CMD_PAUSE = "4";
const std::string CMD_RESUME = "5";
const std::string CMD_REQ_IFRAME = "6";
const std::string CMD_SET_MEDIA_CONFIG = "7";
const std::string CMD_HEARTBEAT_RESPONSE = "1000";

class CasAppCtrlCmdUtils {
public:
    static std::string MakeCommand(std::map<std::string, std::string> parameters,
        std::string separator = COMMAND_SEPARATOR);

    static std::map<std::string, std::string> ParseCommand(const char *data, int length,
        std::string separator = COMMAND_SEPARATOR);
};

#endif