// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASCMDCONTROLTHREAD_H
#define CLOUDAPPSDK_CASCMDCONTROLTHREAD_H

#include <thread>
#include "CasDataPipe.h"
#include "CasCmdController.h"

class CasCmdControlThread {
public:
    explicit CasCmdControlThread(CasCmdController *controller);

    ~CasCmdControlThread();

    void SetControlPktHandle(CasDataPipe *controlStream);

    CasDataPipe *GetControlStream();

    CasCmdController *GetController();

    int GetThreadStatus();

    int Start();

    int Stop();

    int Restart();

    int Exit();

    CasDataPipe *m_controlStream;
    CasCmdController *m_cmdController;

private:
    std::atomic_int m_threadStatus;
    std::thread *m_controlTask;
};

#endif // CLOUDAPPSDK_CASCMDCONTROLTHREAD_H
