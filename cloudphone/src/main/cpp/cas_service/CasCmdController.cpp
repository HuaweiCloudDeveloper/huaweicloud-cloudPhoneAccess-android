// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <unistd.h>
#include <string>
#include <sstream>
#include "CasGadget.h"
#include "CasMsg.h"
#include "CasLog.h"
#include "CasSocket.h"
#include "CasCmdController.h"
#include "CasAppCtrlCmdUtils.h"

using namespace std;

CasCmdController::CasCmdController(CasStreamBuildSender *streamBuildSender)
{
    m_ctrlListener = nullptr;
    m_streamBuildSender = streamBuildSender;
}

CasCmdController::~CasCmdController()
{
    m_streamBuildSender = nullptr;
    m_ctrlListener = nullptr;
}

bool CasCmdController::SendCtrlCmd(std::map<std::string, std::string> parameters)
{
    string msg = CasAppCtrlCmdUtils::MakeCommand(parameters);
    int ret = m_streamBuildSender->SendDataToServer(CasMsgType::CmdControl, msg.data(), msg.size());
    if (ret != static_cast<int>(msg.size())) {
        ERR("Send ret %d, expect %zu, %s", ret, msg.size(), msg.c_str());
        return false;
    }
    return true;
}

void CasCmdController::HandleReceivedCtrlCmdFromServer(void *onePkt)
{
    streamMsgHead *streamHead = (streamMsgHead *)onePkt;
    const char *receivedMsgString = (char *)((uint8_t *)onePkt + sizeof(streamMsgHead));
    map<string, string> parameters = CasAppCtrlCmdUtils::ParseCommand(receivedMsgString, streamHead->GetPayloadSize());

    int command = -1;
    int result = -1;
    int code = -1;
    string msg = "";

    if (parameters.find(KEY_COMMAND) != parameters.end()) {
        command = atoi(parameters[KEY_COMMAND].c_str());
    }
    if (parameters.find(KEY_RESULT) != parameters.end()) {
        result = atoi(parameters[KEY_RESULT].c_str());
    }
    if (parameters.find(KEY_CODE) != parameters.end()) {
        code = atoi(parameters[KEY_CODE].c_str());
    }
    if (parameters.find(KEY_MSG) != parameters.end()) {
        msg = parameters[KEY_MSG];
    }

    if ((command == -1) || (result == -1) || (code == -1) || (msg == "")) {
        return;
    }
    if (nullptr != m_ctrlListener) {
        m_ctrlListener->OnCmdRecv(code, msg);
    }
}

void CasCmdController::RegisterStateChangeListener(CasControllerListener *listener)
{
    m_ctrlListener = listener;
}