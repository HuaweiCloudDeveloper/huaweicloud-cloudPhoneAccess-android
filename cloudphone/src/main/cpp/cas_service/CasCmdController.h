// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASCMDCONTROLLER_H
#define CLOUDAPPSDK_CASCMDCONTROLLER_H

#include <set>
#include <string>
#include <map>
#include "CasLog.h"
#include "CasMsg.h"
#include "CasSocket.h"
#include "CasDataPipe.h"
#include "CasStreamBuildSender.h"

class CasControllerListener {
public:

    virtual void OnCmdRecv(int code, std::string msg) = 0;
};

class CasCmdController {
public:

    explicit CasCmdController(CasStreamBuildSender *streamBuildSender);

    ~CasCmdController();

    bool SendCtrlCmd(std::map<std::string, std::string> parameters);

    void HandleReceivedCtrlCmdFromServer(void *onePkt);

    void RegisterStateChangeListener(CasControllerListener *listener);

private:
    CasControllerListener *m_ctrlListener;
    CasStreamBuildSender *m_streamBuildSender;
};

#endif // CLOUDAPPSDK_CASCMDCONTROLLER_H
