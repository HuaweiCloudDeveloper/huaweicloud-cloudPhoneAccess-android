// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASDATAPIPE_H
#define CLOUDAPPSDK_CASDATAPIPE_H

#include <mutex>
#include <deque>
#include <condition_variable>
#include "CasPktHandle.h"

class CasDataPipe : public CasPktHandle {
public:
    explicit CasDataPipe(bool block = false);

    virtual ~CasDataPipe();

    void Handle(void *pPkt) override;

    void *GetNextPkt();

    void *GetNextPktWaitFor(int timeout);

    int GetNumItems();

    void Exit();

    void Clear() noexcept;

    /**
     * 当使用数据的地方处理完数据时调用，更新未处理数据数量
     */
    void UpdateUnprocessedSize();

    std::atomic_uint64_t m_unprocessedSize{0};
protected:
    uint32_t m_totalSize;
    std::deque<void *> m_deque;
    std::mutex m_lock;
    std::condition_variable m_cv;
    bool m_block;
    volatile bool m_status;
};

#endif // CLOUDAPPSDK_CASDATAPIPE_H
