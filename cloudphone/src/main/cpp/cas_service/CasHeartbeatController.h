// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASHEARTBEATCONTROLLER_H
#define CLOUDAPPSDK_CASHEARTBEATCONTROLLER_H

#include <mutex>
#include "CasDataPipe.h"
#include "CasStreamBuildSender.h"

class CasHeartbeatController : public CasPktHandle {
public:
    explicit CasHeartbeatController(CasStreamBuildSender *streamBuildSender);

    ~CasHeartbeatController();

    bool HeartBeatRequest(int timeout);

    bool WaitResponse(std::string command, int timeout);

    void Handle(void *pPacket) override;

    void StopHandle();

private:
    void Init();

    bool Send(std::string msg);

    CasStreamBuildSender *m_streamBuildSender = nullptr;
    CasDataPipe *m_recvStream = nullptr;
    std::mutex m_lock;
    volatile bool m_status;
};

#endif // CLOUDAPPSDK_CASHEARTBEATCONTROLLER_H
