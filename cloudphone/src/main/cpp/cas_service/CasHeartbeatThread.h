// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASHEARTBEATTHREAD_H
#define CLOUDAPPSDK_CASHEARTBEATTHREAD_H

#include <thread>
#include "CasHeartbeatController.h"

class CasHeartbeatThread {
public:
    CasHeartbeatThread(CasHeartbeatController *heartbeatController, CasSocket *sock);

    virtual ~CasHeartbeatThread();

    int Start();

    int Stop();

    int Restart();

    int Exit();

    int GetThreadStatus();

    void SetThreadStatus(int status);

    uint64_t GetLag();

    uint64_t TestLag();

    void UpdateLag(uint64_t lag);

private:
    CasHeartbeatController *m_heartbeatController = nullptr;
    CasSocket *m_casSocket = nullptr;
    uint64_t m_lag = 0;
    int m_sendHbErrCount = 0;
    std::deque<uint64_t> m_lagDeque;
    std::atomic_int m_threadStatus;
    std::thread m_statLagTask;
};

#endif // CLOUDAPPSDK_CASHEARTBEATTHREAD_H