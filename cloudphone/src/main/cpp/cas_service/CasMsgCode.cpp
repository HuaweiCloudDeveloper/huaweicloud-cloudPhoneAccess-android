// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "CasMsgCode.h"
#include "../CasCommon.h"

using namespace std;

const string CLIENT_INNER_ERROR = "Client inner error";

static map<int, string> gCodeMap = \
    {{CAS_CONNECTING,                   "Connecting"},
    { CAS_CONNECT_SUCCESS,              "Connect success" },
    { CAS_SERVER_UNREACHABLE,           "Server unreachable"},
    { CAS_RESOURCE_IN_USING,            "Resource in using"},

    { CAS_CONNECT_LOST,                 "Connect lost"},
    { CAS_RECONNECTING,                 "Reconnecting"},
    { CAS_RECONNECT_SUCCESS,            "Reconnect success"},
    { CAS_RECONNECT_SERVER_UNREACHABLE, "Reconnect server unrearchable"},
    { CAS_EXIT,                         "Exit"},
    { CAS_FIRST_FRAME,                  "First frame"}};

string CasMsgCode::GetMsg(const int code)
{
    for (map<int, string>::iterator it=gCodeMap.begin(); it!=gCodeMap.end(); it++) {
        if (it->first == code) {
            return it->second;
        }
    }
    return CLIENT_INNER_ERROR;
}