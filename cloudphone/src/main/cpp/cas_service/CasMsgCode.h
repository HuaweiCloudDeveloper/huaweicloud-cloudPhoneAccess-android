// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASMSGCODE_H
#define CLOUDAPPSDK_CASMSGCODE_H

#include <string>
#include <map>

enum {
    CAS_NOTOUCH_TIMEOUT = 0x0F00,
    CAS_CONNECT_LOST = 0x0A00,
    CAS_FIRST_FRAME = 0x1800,
};

class CasMsgCode {
public:
    static std::string GetMsg(const int code);
};

#endif // CLOUDAPPSDK_CASMSGCODE_H