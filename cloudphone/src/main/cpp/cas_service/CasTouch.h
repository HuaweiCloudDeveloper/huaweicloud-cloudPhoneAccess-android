// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASTOUCH_H
#define CLOUDAPPSDK_CASTOUCH_H

#include "CasStreamBuildSender.h"
#include "CasSocket.h"
#include "CasMsg.h"

class CasTouch {
public:
    explicit CasTouch(CasSocket *casSocket);

    ~CasTouch();

    void Init(CasSocket *casSocket);

    bool SendTouchEvent(int id, int action, int x, int y, int pressure, int time, int orientation, int height, int width);

    bool SendKeyEvent(uint16_t keycode, uint16_t action);

    bool SendMotionEvent(uint16_t masterAxis, int32_t masterValue, uint16_t secondaryAxis, int32_t secondaryValue);

private:
    CasStreamBuildSender *m_streamBuildSender;
};

#endif // CLOUDAPPSDK_CASTOUCH_H
