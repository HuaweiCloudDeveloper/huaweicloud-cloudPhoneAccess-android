// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASVIDEODECODETHREAD_H
#define CLOUDAPPSDK_CASVIDEODECODETHREAD_H

#include <thread>
#include <android/native_window.h>
#include "../cas_decoder/CasVideoEngine.h"
#include "CasDataPipe.h"

class CasFirstVideoFrameImpl;
class CasVideoDecodeStatImpl;

class CasVideoHDecodeThread {
public:
    explicit CasVideoHDecodeThread(ANativeWindow *nativeWindow, FrameType frameType, int rotationDegrees);

    ~CasVideoHDecodeThread();

    void SetDecodePktHandle(CasDataPipe *videoPktStream);

    CasDataPipe *GetVideoPktStream();

    CasVideoEngine *GetVideoEngine();

    void SetThreadStatus(int status);

    int GetThreadStatus();

    int Start();

    int Stop();

    int Restart();

    int Exit();

    CasDataPipe *m_videoPktStream;
    CasVideoEngine *m_videoEngine;
    CasFirstVideoFrameImpl *m_firstVideoFrame;
    CasVideoDecodeStatImpl *m_videoDecodeStat;

private:
    std::atomic_int m_threadStatus;
    std::thread *m_decodeTask;
    ANativeWindow *m_nativeWindow;
    FrameType m_frameType;
    int m_rotationDegrees;
};

#endif // CLOUDAPPSDK_CASVIDEODECODETHREAD_H
