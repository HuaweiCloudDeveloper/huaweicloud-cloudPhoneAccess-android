// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cerrno>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <sys/socket.h>
#include <linux/in.h>
#include <linux/tcp.h>
#include <unistd.h>
#include "../cas_common/CasLog.h"
#include "CasSocket.h"

using namespace std;

CasSocket::~CasSocket()
{
    if (m_eventNotice != nullptr) {
        delete m_eventNotice;
        m_eventNotice = nullptr;
    }

    if (m_reconnectNotice != nullptr) {
        delete m_reconnectNotice;
        m_reconnectNotice = nullptr;
    }
}

void CasSocket::EventNotice()
{
    if (this->m_eventNotice != nullptr) {
        this->m_eventNotice->Notice(this);
    }
}

void CasSocket::SetEventNotice(CasEventNotice *eventNotice)
{
    this->m_eventNotice = eventNotice;
}

void CasSocket::SetReconnectNotice(CasEventNotice *reconnectNotice)
{
    this->m_reconnectNotice = reconnectNotice;
}

CasEventNotice *CasSocket::GetReconnectNotice()
{
    return this->m_reconnectNotice;
}

void CasSocket::SetFd(int fd)
{
    this->m_fd = fd;
}

int CasSocket::GetFd()
{
    return m_fd;
}

void CasSocket::SetStatus(int status)
{
    this->m_status = status;
}

int CasSocket::GetStatus()
{
    return m_status;
}
