// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASTCPSOCKET_H
#define CLOUDAPPSDK_CASTCPSOCKET_H

#include <mutex>
#include <map>
#include "CasSocket.h"
#include "../libs/openssl/include/openssl/cpu.h"

#define USE_TLS 1

const int SSL_CONFIG_SUCCESS = 0;
const int SSL_CONFIG_SOCKET_CLOSE = -1;
const int SSL_CONFIG_ERROR = -2;

class CasTcpSocket : public CasSocket {
public:
    CasTcpSocket();

    ~CasTcpSocket() override;

    int Send(void *pkt, size_t size) override;
    int Recv(void *pkt, size_t size) override;
#if USE_TLS
    int ConfigSSL();

private:
    SSL *m_ssl;
    SSL_CTX *m_ctx;
#endif
};

class CasTcpClientSocket : public CasTcpSocket {
public:
    CasTcpClientSocket(uint32_t remoteIp, uint16_t remotePort, uint32_t localIp = 0, uint16_t localPort = 0);

    CasTcpClientSocket(uint32_t socketOption, uint32_t remoteIp, uint16_t remotePort, uint32_t localIp = 0,
        uint16_t localPort = 0);

    ~CasTcpClientSocket() override;

    int Connect() override;

    int Reconnect() override;

    int CreateSocket();

private:
    int CasCreateTcpClient(uint32_t socketOption, uint32_t remoteIp, uint16_t remotePort, uint32_t localIp,
        uint16_t localPort);
};

#endif // CLOUDAPPSDK_CASTCPSOCKET_H
