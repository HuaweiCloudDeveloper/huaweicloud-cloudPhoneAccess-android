// Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CLOUDAPPSDK_CASSTREAMBUILDSENDER_H
#define CLOUDAPPSDK_CASSTREAMBUILDSENDER_H

#include "CasMsg.h"
#include "CasSocket.h"
#include "../libs/mtrans/include/net_trans.h"

class CasStreamBuildSender {
public:
    explicit CasStreamBuildSender(CasSocket *socket);

    ~CasStreamBuildSender();

    void SetNetTrans(NetTrans *netTrans);

    int SendDataToServer(CasMsgType type, const void *buf, size_t len);

private:
    CasSocket *m_socket = nullptr;
    NetTrans *m_mtrans = nullptr;
};

#endif // CLOUDAPPSDK_CASSTREAMBUILDSENDER_H
