// Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef HUAWEICLOUD_CLOUDPHONEACCESS_ENGINE_NET_TRANS_H
#define HUAWEICLOUD_CLOUDPHONEACCESS_ENGINE_NET_TRANS_H

#include <cstdint>
#include <string>
#include "net_trans_def.h"

class NetTrans {
public:
    NetTrans();

    virtual ~NetTrans();

    void EnableLog(const std::string& logPath, NET_TRANS_LOG_LEVLE logLevel);

    int Init(NET_TRANS_PEER_TYPE peerType,
             char const *ip, int port,
             TransConfigParam param,
             RecvVideoDataCallback videoCallback,
             RecvAudioDataCallback audioCallback,
             UpdataBitrateCallback bitrateCallback,
             RequestKeyFrameCallback keyFrameCallback,
             RecvCmdDataCallback cmdCallback,
             RecvAudioDecodeCallback audioDecodeCallback,
             TransLogCallback transLogCallback);

    int Start();
    int Stop();

    int SendVideoData(uint8_t *data, int len, std::string encodeType);
    int SendAudioData(uint8_t *data, int len);
    int SendCmdData(uint8_t *data, int len);

    int SendSensorData(uint8_t *data, int len);

    int SendLocationData(uint8_t *data, int len);

    int SendTouchEventData(uint8_t *data, int len, int action);

    int SendKeyEventData(uint8_t *data, int len);

    int SendMotionEventData(uint8_t *data, int len);
    int GetVideoRecvStats(StreamRecvStats* stats);
    int GetAudioRecvStats(StreamRecvStats* stats);
    int GetRtt();
    int GetCmdSendStats(StreamSendStats* stats);

private:
    class NetTransPri;
    class NetTransPri *m_netTransPri;
};

#endif //HUAWEICLOUD_CLOUDPHONEACCESS_ENGINE_NET_TRANS_H
