// Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef HUAWEICLOUD_CLOUDPHONEACCESS_ENGINE_NET_TRANS_DEF_H
#define HUAWEICLOUD_CLOUDPHONEACCESS_ENGINE_NET_TRANS_DEF_H

enum NET_TRANS_PEER_TYPE {
    PEER_CLIENT,
    PEER_SERVER
};

struct TransConfigParam {
    uint32_t curBandwidth; //单位 kbps
    uint32_t maxBandwidth;
    uint32_t minVideoSendBitrate;
    uint32_t maxVideoSendBitrate;
};

enum NET_TRANS_LOG_LEVLE {
    TRANS_LOG_LEVEL_OFF,    // 关闭日志
    TRANS_LOG_LEVEL_ERROR,  // error级别
    TRANS_LOG_LEVEL_WARN,   // warn级别
    TRANS_LOG_LEVEL_INFO,   // info级别
    TRANS_LOG_LEVEL_DEBUG   // debug级别
};

struct AudioJbDecode {
    int32_t bfi;
    uint8_t * payload;
    uint32_t inputLength;
    int16_t* outputData;
    uint32_t outputLength;
    uint32_t outputBufSize;
    int32_t frameType;
    uint32_t metaDataOffset;
};

struct TouchEventParam {
    uint8_t id;
    uint8_t action;
    uint16_t x;
    uint16_t y;
    uint16_t pressure;
    int32_t time;
    uint8_t orientation;
    uint16_t height;
    uint16_t width;
} __attribute__((packed));

struct KeyEventParam {
    uint16_t keycode;
    uint16_t action;
} __attribute__((packed));

struct MotionEventParam {
    uint16_t masterAxis;
    int32_t masterValue;
    uint16_t secondaryAxis;
    int32_t secondaryValue;
} __attribute__((packed));

struct VideoRecvStats {
    uint32_t recvFrameRate;
    uint32_t redRate;
    uint32_t recoverRate;
    uint32_t jbListpacketNum;
    uint32_t sendKeyRequestCnt;
    int32_t jbJitter;
    uint32_t refFrameErrorCnt;
    uint64_t aveJbDelayAfterBuild;
    uint64_t recvAveDelay;
    uint64_t recvMaxDelay;
};

struct AudioRecvStats {
    uint32_t receiveTotalTime;
    uint32_t plcCount;
    uint32_t tsmCompressCount;
    uint32_t tsmStretchCount;
    int32_t totalDelay;
    int32_t estTotalDelay;
    uint64_t noVoiceFrameCnt;
    uint32_t getFrameCount;
};

struct StreamRecvStats {
    uint32_t recvBitrate;
    uint32_t lostRate;
    uint32_t jitter;
    uint32_t lostPktCnt;
    uint32_t maxContLostPktCnt;
    uint32_t recvTotalFrameCnt;
    uint32_t jbDepth;
    uint32_t jbTotalFrameCnt;
    uint32_t packetRate;
    uint32_t periodNotRecvPktCnt;
    uint32_t periodNotRecvPktTime;
    uint32_t periodFreezeFrameCnt;
    uint32_t periodFreezeTime;
    uint32_t sendNackRequestCnt;
    union {
        VideoRecvStats videoStats;
        AudioRecvStats audioStats;
    };
};

struct VideoSendStats {
    uint32_t sendFrameRate;
    uint32_t availableEncBitrate;
    uint32_t inputFrameCnt;
    uint32_t sendFrameCnt;
    uint32_t redRate;
    uint32_t recvKeyRequestCnt;
    uint32_t recvNackRequestCnt;
    uint32_t nackResponsePktCnt;
    uint32_t nackNotRspCnt;
};

struct StreamSendStats {
    uint32_t encBitrate;
    uint32_t fecBitrate;
    uint32_t arqBitrate;
    uint32_t packetRate;
    uint32_t rtt;
    uint32_t remoteJitter;
    uint32_t remoteLostRate;
    uint32_t pacingSendMaxDelay;
    uint32_t pacingSendAveDelay;
    uint32_t periodNotSendPktCnt;
    uint64_t periodNotSendPktTime;
    VideoSendStats videoStats;
};

typedef int32_t (*RecvVideoDataCallback)(uint8_t* data, uint32_t length);
typedef int32_t (*RecvAudioDataCallback)(uint8_t* data, uint32_t length);
typedef int32_t (*RecvAudioDecodeCallback)(int32_t streamId, AudioJbDecode* audioJbDecode);
typedef int32_t (*RecvCmdDataCallback)(uint8_t* data, uint32_t length);
typedef void (*UpdataBitrateCallback)(uint32_t encBitrate, uint32_t totalBitrate);
typedef void (*RequestKeyFrameCallback)();
typedef void (*TransLogCallback)(const char* str, uint32_t length);

#endif //HUAWEICLOUD_CLOUDPHONEACCESS_ENGINE_NET_TRANS_DEF_H
