package com.huawei.cloudphone.api;

public interface CloudAppDataListener {
    void onRecvCloudAppData(byte[] data);
}
