/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.api;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;

import com.huawei.cloudphone.api.CloudPhoneParas.DevType;
import com.huawei.cloudphone.api.CloudPhoneParas.DisplayMode;

import java.util.HashMap;
import java.util.Map;

public interface ICloudPhone {

    /**
     * 获取SDK版本号
     *
     * @return SDK版本号。
     */
    String getVersion();

    /**
     * 初始化SDK
     *
     * @param context 应用的上下文
     * @param type    TV/PHONE
     */
    void init(Context context, DevType type) throws Exception;

    /**
     * 反初始化SDK
     */
    void deinit() throws Exception;

    /**
     * 启动云手机
     *
     * @param activity activity
     * @param view     手机画面渲染ViewGoup
     * @param params   启动参数
     */
    void startCloudPhone(final Activity activity, final ViewGroup view, final Map<String, String> params) throws Exception;

    /**
     * 停止云手机
     */
    void exitCloudPhone() throws Exception;

    /**
     * 设置音视频参数
     *
     * @param mediaConfig
     */
    void setMediaConfig(HashMap<String, String> mediaConfig);

    /**
     * 设置显示模式
     *
     * @param mode
     */
    void setDisplayMode(DisplayMode mode);

    /**
     * 发送数据给运行在云机的App
     *
     * @param data
     * @throws Exception
     */
    void sendDataToCloudApp(byte[] data) throws Exception;

    /**
     * 注册从云App发送回来数据的回调
     *
     * @param listener
     */
    void registerCloudAppDataListener(CloudAppDataListener listener);

    /**
     * 设置状态回调
     *
     * @param listener
     */
    void registerCloudPhoneStateListener(CloudPhoneStateListener listener);

    /**
     * 设置方向变化的回调
     *
     * @param listener
     */
    void registerOnOrientationChangeListener(CloudPhoneOrientationChangeListener listener);

    /**
     * 发送虚拟设备数据
     * @param devType
     * @param data
     */
    void sendVirtualDeviceData(byte devType, byte[] data);

    /**
     * 发送授权结果
     * @param requestCode
     * @param grantResults
     */
    void sendPermissionResult(int requestCode, int grantResults);

    /**
     * 设置权限监听回调
     *
     * @param listener
     */
    void registerPermissionRequestListener(CloudPhonePermissionRequestListener listener);

    /**
     * 发送剪切板数据
     * @param data
     */
    void sendClipboardData(byte[] data);

    /**
     * 设置剪切板监听器
     *
     * @param listener
     */
    void registerClipboardListener(CloudPhoneClipboardListener listener);

    /**
     * 获取网络时延
     *
     * @return 网络时延
     */
    int getRtt();

    /**
     * 获取当前状态
     *
     * @return 当前状态
     */
    int getState();

    String getVideoStatisticInfo();

    String getSimpleStatisticInfo();

    String getSensorStatusInfo();
}
