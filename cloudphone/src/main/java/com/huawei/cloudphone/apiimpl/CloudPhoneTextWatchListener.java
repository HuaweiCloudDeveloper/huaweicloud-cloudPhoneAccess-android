package com.huawei.cloudphone.apiimpl;

public interface CloudPhoneTextWatchListener {
    void onTextChange(byte[] data);
}
