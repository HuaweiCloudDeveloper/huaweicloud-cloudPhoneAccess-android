/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.apiimpl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.huawei.cloudphone.common.CASLog;

public class PhoneView extends SurfaceView {
    private static final String TAG = "PhoneView";
    private int mOrientation;
    private boolean mIsFullScreen;

    public PhoneView(Context context, boolean isFullScreen) {
        super(context);
        mIsFullScreen = isFullScreen;
        mOrientation = -1;
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        ViewGroup viewGroup = (ViewGroup) getParent();
        //requestLayout 如果不调用，可能父布局的坐标值不会马上改变
        viewGroup.requestLayout();
        int parentWidth = viewGroup.getWidth();
        int parentHeight = viewGroup.getHeight();
        CASLog.i(TAG, "parentWidth = " + parentWidth + " parentHeight = " + parentHeight);
        if (parentWidth > parentHeight) {
            // 1横向
            if (mOrientation == 1) {
                return;
            }
            mOrientation = 1;
        } else {
            // 0纵向
            if (mOrientation == 0) {
                return;
            }
            mOrientation = 0;
        }
        if (mIsFullScreen) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    FrameLayout.LayoutParams layoutParams =
                            new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                    PhoneView.this.setLayoutParams(layoutParams);
                }
            });
        } else {
            final int viewHeight;
            final int viewWidth;
            if (parentWidth > parentHeight) {
                viewHeight = parentHeight;
                viewWidth = (int) (parentHeight * ((float) 1280 / (float) 720));
            } else {
                viewHeight = (int) (parentWidth * ((float) 1280 / (float) 720));
                viewWidth = parentWidth;
            }
            CASLog.i(TAG, "set viewHeight = " + viewHeight + " viewWidth = " + viewWidth);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(viewWidth, viewHeight, Gravity.CENTER);
                    PhoneView.this.setLayoutParams(layoutParams);
                }
            });
        }
    }
}
