/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.common;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class CasParcelableMap implements Parcelable {

    public static final Creator<CasParcelableMap> CREATOR = new Creator<CasParcelableMap>() {
        public CasParcelableMap createFromParcel(Parcel source) {
            return new CasParcelableMap(source);
        }

        public CasParcelableMap[] newArray(int size) {
            return new CasParcelableMap[size];
        }
    };
    private HashMap<String, String> parcelableMap;

    public CasParcelableMap() {
    }

    public CasParcelableMap(Parcel source) {
        readFromParcel(source);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void readFromParcel(Parcel source) {
        this.parcelableMap = source.readHashMap(Thread.currentThread().getContextClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(parcelableMap);
    }

    public HashMap<String, String> getParcelableMap() {
        return parcelableMap;
    }

    public void setParcelableMap(HashMap<String, String> map) {
        this.parcelableMap = map;
    }
}
