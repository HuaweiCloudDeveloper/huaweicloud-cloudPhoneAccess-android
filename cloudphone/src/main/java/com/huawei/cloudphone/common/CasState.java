/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.common;

public class CasState {

    static public final int CAS_CONNECTING = 0x0100;
    static public final int CAS_CONNECT_SUCCESS = 0x0200;
    static public final int CAS_SERVER_UNREACHABLE = 0x0301;
    static public final int CAS_RESOURCE_IN_USING = 0x0302;
    static public final int CAS_CONNECT_EXCEPTION = 0x0303;

    static public final int CAS_VERIFYING = 0x0400;
    static public final int CAS_VERIFY_SUCCESS = 0x0500;
    static public final int CAS_VERIFY_PARAMETER_MISSING = 0x0601;
    static public final int CAS_VERIFY_PARAMETER_INVALID = 0x0602;
    static public final int CAS_VERIFY_AESKEY_QUERY_FAILED = 0x0603;
    static public final int CAS_VERIFY_AESKEY_INVALID = 0x0604;
    static public final int CAS_VERIFY_DECRYPT_FAILED = 0x0605;
    static public final int CAS_VERIFY_FAILED = 0x0606;
    static public final int CAS_VERIFY_SESSION_ID_INVALID = 0x0607;

    static public final int CAS_START_SUCCESS = 0x0800;
    static public final int CAS_PARAMETER_MISSING = 0x0904;

    static public final int CAS_CONNECT_LOST = 0x0A00;
    static public final int CAS_RECONNECTING = 0x0B00;
    static public final int CAS_RECONNECT_SUCCESS = 0x0C00;
    static public final int CAS_RECONNECT_PARAMETER_INVALID = 0x0D01;
    static public final int CAS_RECONNECT_SERVER_UNREACHABLE = 0x0D02;
    static public final int CAS_RECONNECT_ENGING_START_ERROR = 0x0D03;

    static public final int CAS_TRAIL_PLAY_TIMEOUT = 0x0E00;
    static public final int CAS_NOTOUCH_TIMEOUT = 0x0F00;
    static public final int CAS_BACKGROUND_TIMEOUT = 0x1000;

    static public final int CAS_DECODE_ERROR = 0x1100;
    static public final int CAS_ENGINE_START_FAILED = 0x1101;
    static public final int CAS_H265_NOT_SUPPORTED = 0x1102;

    static public final int CAS_SWITCH_BACKGROUND_SUCCESS = 0x1200;
    static public final int CAS_SWITCH_BACKGROUND_ERROR = 0x1301;
    static public final int CAS_SWITCH_FOREGROUND_SUCCESS = 0x1400;
    static public final int CAS_SWITCH_FOREGROUND_ERROR = 0x1501;
    static public final int CAS_ORIENTATION = 0x1600;
    static public final int CAS_EXIT = 0x1700;
    static public final int CAS_FIRST_FRAME = 0x1800;
    static public final int CAS_BACK_HOME = 0x1900;
    static public final int CAS_REQUEST_CAMERA_KEY_FRAME = 0x2000;
    static public final int CAS_SET_MEDIA_CONFIG_SUCCESS = 0x2100;
    static public final int CAS_SET_MEDIA_CONFIG_ERROR = 0x2201;
    static public final int CAS_INVALID_CMD = 0xFFFF;

    public static class CasStateMsg {
        private int mCode;
        private String mMsg;

        public CasStateMsg(int code, String msg) {
            mCode = code;
            mMsg = msg;
        }

        public String getMsg() {
            return mMsg;
        }

        public int getCode() {
            return mCode;
        }
    }
}
