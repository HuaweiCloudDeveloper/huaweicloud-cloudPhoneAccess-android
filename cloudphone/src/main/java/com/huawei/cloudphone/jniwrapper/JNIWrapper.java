/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.jniwrapper;

import android.view.Surface;

import java.util.HashMap;

public class JNIWrapper {
    public static final String KEY_LOG_LEVEL = "log_level";
    public static final String KEY_IP = "ip";
    public static final String KEY_PORT = "port";
    public static final String KEY_DECODE_METHOD = "decode_method";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_TICKET = "ticket";
    public static final String KEY_SESSION_ID = "session_id";
    public static final String KEY_AES_IV = "aes_iv";
    public static final String KEY_ENCRYPTED_DATA = "encrypted_data";
    public static final String KEY_BACKGROUND_TIMEOUT = "backgroundTimeout";
    public static final String KEY_AUTH_TS = "auth_ts";
    public static final String KEY_VERIFY_DATA = "verify_data";
    public static final String KEY_SDK_VERSION = "sdk_version";
    public static final String KEY_PROTOCOL_VERSION = "protocol_version";
    public static final String KEY_REGION_ID = "region_id";
    public static final String KEY_CLIENT_MODE = "client_mode";
    public static final byte INVALID = 0;
    public static final byte VERIFY = 1;
    public static final byte AUDIO = 2;
    public static final byte VIDEO = 3;
    public static final byte CHANNEL = 4;
    public static final byte TOUCH_INPUT = 6;
    public static final byte CONTROL = 7;
    public static final byte ORIENTATION = 9;

    public static final byte RECORDER = 11;

    public static final byte IMEDATA = 14;
    public static final byte RECONNECT = 15;
    public static final byte RECONNECT_HEAD = 16;
    public static final byte NOTIFY = 17;
    public static final byte END = 19;

    public static final byte VIRTUAL_DEVICE_DATA = 20;
    public static final byte CAMERA_DATA = 21;
    public static final byte MICROPHONE_DATA = 22;
    public static final byte SENSOR_DATA = 23;
    public static final byte LOCATION_DATA = 24;

    static {
        System.loadLibrary("cloudapp");
    }

    public static native int recvData(byte type, byte[] data, int length);

    public static native int sendData(byte type, byte[] data, int length);

    public static native boolean sendTouchEvent(final int id, final int action, final int x1, final int y1, final int pressure, long time, int orientation, int height, int width);

    public static native boolean sendKeyEvent(final int keycode, final int action);

    public static native boolean sendMotionEvent(final int masterAxis, final int masterValue, final int secondaryAxis, final int secondaryValue);

    public static native void setJniConf(String key, String value);

    public static native boolean start(Surface surface, boolean isHome);

    public static native void stop(boolean isHome);

    public static native boolean reconnect();

    public static native int getJniStatus();

    public static native boolean getConnectStatus();

    public static native void registerCasJNICallback(Object obj);

    public static native int getLag();

    public static native String getVideoStreamStats();

    public static native String getSimpleStreamStats();

    public static native boolean setMediaConfig(HashMap<String, String> mediaConfigMap);

    public static native boolean setRotation(int rotation);

}