/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.jniwrapper;

import android.view.Surface;

import java.util.HashMap;

public class JniBridge {
    static JniBridge g_jniBridge = null;

    public static synchronized JniBridge getInstance() {
        if (g_jniBridge == null) {
            g_jniBridge = new JniBridge();
        }
        return g_jniBridge;
    }

    private JniBridge() {
    }

    public int sendData(byte type, byte[] data, int length) {
        return JNIWrapper.sendData(type, data, length);
    }

    public int recvData(byte type, byte[] data, int length) {
        return JNIWrapper.recvData(type, data, length);
    }

    public boolean sendTouchEvent(final int id, final int action, final int x1, final int y1, final int pressure, long time, int orientation, int height, int width) {
        return JNIWrapper.sendTouchEvent(id, action, x1, y1, pressure, time, orientation, height, width);
    }

    public boolean sendKeyEvent(final int keycode, final int action) {
        return JNIWrapper.sendKeyEvent(keycode, action);
    }

    public boolean sendMotionEvent(final int masterAxis, final int masterValue, final int secondaryAxis, final int secondaryValue) {
        return JNIWrapper.sendMotionEvent(masterAxis, masterValue, secondaryAxis, secondaryValue);
    }

    public void setJniConf(String key, String value) {
        JNIWrapper.setJniConf(key, value);
    }

    public boolean start(Surface surface, boolean isHome) {
        return JNIWrapper.start(surface, isHome);
    }

    public void stop(boolean isHome) {
        JNIWrapper.stop(isHome);
    }

    public boolean reconnect() {
        return  JNIWrapper.reconnect();
    }

    public int getJniStatus() {
        return JNIWrapper.getJniStatus();
    }

    public boolean getConnectStatus() {
        return JNIWrapper.getConnectStatus();
    }

    public void registerCasJNICallback(Object obj) {
        JNIWrapper.registerCasJNICallback(obj);
    }

    public int getLag() {
        return JNIWrapper.getLag();
    }

    public String getVideoStreamStats() {
        return JNIWrapper.getVideoStreamStats();
    }

    public String getSimpleStreamStats() {
        return JNIWrapper.getSimpleStreamStats();
    }


    public boolean setMediaConfig(HashMap<String, String> mediaConfigMap) {
        if (mediaConfigMap != null) {
            return JNIWrapper.setMediaConfig(mediaConfigMap);
        } else {
            return false;
        }
    }

    public boolean setRotation(int rotation) {
        return JNIWrapper.setRotation(rotation);
    }



}
