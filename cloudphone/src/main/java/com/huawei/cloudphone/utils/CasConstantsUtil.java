/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.utils;

import java.util.regex.Pattern;

/**
 * CasConstantsUtil
 */
public class CasConstantsUtil {

    /**
     * cph:ip
     */
    public static final String IP = "ip";

    /**
     * check cph ip
     */
    public static final Pattern IP_PATTERN = Pattern.compile("^((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}$");

    /**
     * cph:port
     */
    public static final String PORT = "port";

    /**
     * background timeout
     * <p>background timeout when back to home</p>
     */
    public static final String BACKGROUND_TIMEOUT = "background_timeout";

    /**
     * available play time
     * <p>unit is second</p>
     */
    public static final String AVAILABLE_PLAYTIME = "available_playtime";

    /**
     * ticket
     * <p>256 random numbers</p>
     */
    public static final String TICKET = "ticket";

    /**
     * sessionId
     */
    public static final String SESSION_ID = "session_id";

    /**
     * aes key
     * <p>generate by aes 128</p>
     */
    public static final String AES_KEY = "aes_key";

    /**
     * auth th
     * <p>authentication timestamp</p>
     */
    public static final String AUTH_TS = "auth_ts";

    /**
     * userid
     */
    public static final String USER_ID = "user_id";

    /**
     * touch_timeout
     */
    public static final String TOUCH_TIMEOUT = "touch_timeout";

    /**
     * video_frame_type 帧类型，yuv或h264
     */
    public static final String VIDEO_FRAME_TYPE = "video_frame_type";

    /**
     * frame_rate 帧率，大于等于10小于等于60，且为10的倍数
     */
    public static final String FRAME_RATE = "fps";

    /**
     * bitrate 码率，大于等于1000000小于等于10000000
     */
    public static final String BITRATE = "bitrate";

    /**
     * gop_size
     */
    public static final String GOP_SIZE = "gop_size";

    /**
     * profile baseline:66 main:77 high:100
     */
    public static final String PROFILE = "profile";

    /**
     * rc_mode abr:0 crf:1 cbr:2
     */
    public static final String RC_MODE = "rc_mode";

    /**
     * virtual_width
     */
    public static final String STREAM_WIDTH = "stream_width";

    /**
     * virtual_height
     */
    public static final String STREAM_HEIGHT = "stream_height";

    /**
     * Physical_width
     */
    public static final String WIDTH = "width";

    /**
     * Physical_width
     */
    public static final String PHYSICAL_WIDTH = "physical_width";

    /**
     * Physical_height
     */
    public static final String PHYSICAL_HEIGHT = "physical_height";

    /**
     * quality
     */
    public static final String QUALITY = "quality";

    /**
     * rgb2yuv rgb转yuv 0：编码时转换 1：抓图时转换
     */
    public static final String RGB2YUV = "rgb2yuv";

    /**
     * dis_repeat 补帧开关，0：补帧 1：不补帧
     */
    public static final String DIS_REPEAT = "dis_repeat";

    /**
     * no_drop 丢帧开关，0：丢帧 1：不丢帧
     */
    public static final String NO_DROP = "no_drop";

    /**
     * thread_type 编码类型，frame：单slice slice：多slice
     */
    public static final String THREAD_TYPE = "thread_type";

    /**
     * thread_count
     */
    public static final String THREAD_COUNT = "thread_count";

    /**
     * check aes key
     */
    public static final Pattern AES_KEY_PATTERN = Pattern.compile("^[0-9a-fA-F]{32}$");

    /**
     * check whether is positive number
     */
    public static final Pattern POSITIVE_NUMBER_PATTERN = Pattern.compile("^[1-9]\\d*$");

    /**
     * frame_type
     */
    public static final String FRAME_TYPE = "frame_type";

    /**
     * encode_type
     */
    public static final String ENCODE_TYPE = "encode_type";

    /**
     * remote_encode_server_ip
     */
    public static final String REMOTE_ENCODE_SERVER_IP = "remote_encode_server_ip";

    /**
     * remote_encode_server_port
     */
    public static final String REMOTE_ENCODE_SERVER_PORT = "remote_encode_server_port";

    /**
     * region_id
     */
    public static final String REGION_ID = "region_id";

    /**
     * client_mode
     */
    public static final String CLIENT_MODE = "client_mode";
}
