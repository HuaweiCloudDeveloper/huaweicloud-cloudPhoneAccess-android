/*
 * Copyright 2022 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.cloudphone.utils;

import com.huawei.cloudphone.common.CASLog;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.prng.SP800SecureRandomBuilder;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;

public final class CasSecurityUtil {

    private static final String TAG = "CasSecurityUtil";

    static {
        Security.addProvider(new CasHRIProvider());
    }

    public static SecureRandom drbg() {
        return DrbgHolder.DRBG;
    }

    private static class DrbgHolder {

        private static final SecureRandom DRBG;

        private static final int AES_256_CTR_CIPHER_LEN = 256;

        // AES-256-CTR要求种子长度不小于384
        private static final int AES_256_CTR_SEED_LEN = 384;

        static {
            SecureRandom secureRandom = null;
            try {
                secureRandom = drbg();
            } catch (NoSuchAlgorithmException e) {
                CASLog.e(TAG, "Failed to get drbg.");
            }
            DRBG = secureRandom;
        }

        private static SecureRandom drbg() throws NoSuchAlgorithmException {
            // 使用/dev/random生成种子
            SecureRandom source = SecureRandom.getInstance("DevRandomSeed");
            final boolean predictionResistant = true;

            // NID_aes_256_ctr
            BlockCipher cipher = new AESEngine();
            final int cipherLen = AES_256_CTR_CIPHER_LEN;

            // 生成nonce
            byte[] nonce = source.generateSeed(cipherLen / Byte.SIZE);

            // 熵源长度
            final int entropyBitsRequired = AES_256_CTR_SEED_LEN;
            // 是否每次取完随机数都重新刷新熵源
            final boolean reSeedOnEachRequest = false;
            // 默认种子补充周期为2^47次请求，小于AES-256-CTR要求的不超过2^48请求
            return new SP800SecureRandomBuilder(source, predictionResistant).setEntropyBitsRequired(entropyBitsRequired)
                    .buildCTR(cipher, cipherLen, nonce, reSeedOnEachRequest);
        }
    }
}