/*
 * Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huawei.cloudphone.virtualdevice;

import android.content.Context;

import com.huawei.cloudphone.api.CloudPhonePermissionRequestListener;
import com.huawei.cloudphone.virtualdevice.common.IVirtualDeviceIO;
import com.huawei.cloudphone.virtualdevice.common.ParamBundle;
import com.huawei.cloudphone.virtualdevice.common.VirtualDeviceProtocol;

public class VirtualDeviceSession {
    VirtualDeviceProtocol mVirtualDevice;
    CloudPhonePermissionRequestListener mPermissionListener;
    Context mContext;
    ParamBundle mParamBundle;
    IVirtualDeviceIO mVirtualDeviceIO;
    final Object mLock = new Object();

    public VirtualDeviceSession(Context context) {
        mParamBundle = new ParamBundle();
        mParamBundle.setAppContext(context);
        mContext = context;
    }

    public void setVirtualDeviceIoHook(IVirtualDeviceIO virtualDeviceIO) {
        mVirtualDeviceIO = virtualDeviceIO;
    }

    public void setPermissionListener(CloudPhonePermissionRequestListener listener) {
        mPermissionListener = listener;
    }

    public VirtualDeviceProtocol getVirtualDevice() {
        if (mVirtualDevice != null) {
            return mVirtualDevice;
        }
        return null;
    }

    public void start() {
        synchronized (mLock) {
            if (mVirtualDevice == null) {
                mVirtualDevice = new VirtualDeviceProtocol(mVirtualDeviceIO, mContext);
                mVirtualDevice.initVirtualDeviceManagers(mPermissionListener);
            }
            mVirtualDevice.startProcess();
        }
    }

    public void stop() {
        synchronized (mLock) {
            if (mVirtualDevice != null) {
                mVirtualDevice.stopProcess();
                mVirtualDevice = null;
            }
        }
    }
}
