/*
 * Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huawei.cloudphone.virtualdevice.common;

import android.content.Context;
import android.view.SurfaceHolder;

public class ParamBundle {
    static private Context mContext;
    static private SurfaceHolder mSurfaceHolder;

    public static void setAppContext(Context context) {
        mContext = context;
    }

    public static void setSurfaceHolder(SurfaceHolder surfaceHolder) {
        mSurfaceHolder = surfaceHolder;
    }

    public static SurfaceHolder getSurfaceHolder() {
        return mSurfaceHolder;
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static void resetParam() {
        mContext = null;
        mSurfaceHolder = null;
    }
}
