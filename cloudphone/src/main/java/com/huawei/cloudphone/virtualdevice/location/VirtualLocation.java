/*
 * Copyright 2023 Huawei Cloud Computing Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huawei.cloudphone.virtualdevice.location;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;
import static com.huawei.cloudphone.api.CloudPhoneParas.DEV_TYPE_LOCATION;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GnssClock;
import android.location.GnssMeasurement;
import android.location.GnssMeasurementsEvent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.huawei.cloudphone.api.CloudPhonePermissionInfo;
import com.huawei.cloudphone.api.CloudPhonePermissionRequestListener;
import com.huawei.cloudphone.common.CASLog;
import com.huawei.cloudphone.virtualdevice.common.IVirtualDeviceDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class VirtualLocation {
    private static final String TAG = "VirtualLocation";
    private final long MIN_TIME = 1000;

    private static final int GNSSPACKET_SIZE = 7361;
    private final float MIN_DISTANCE = 1;
    private final int TYPE_LOCATION = 0;
    private final int TYPE_GNSS_MEASUREMENT = 1;

    private static final int HAS_SNR = (1<<0);
    private static final int HAS_CARRIER_FREQUENCY = (1<<9);
    private static final int HAS_CARRIER_CYCLES = (1<<10);
    private static final int HAS_CARRIER_PHASE = (1<<11);
    private static final int HAS_CARRIER_PHASE_UNCERTAINTY = (1<<12);
    private static final int HAS_AUTOMATIC_GAIN_CONTROL = (1<<13);

    private static final int HAS_LEAP_SECOND = (1<<0);
    private static final int HAS_TIME_UNCERTAINTY = (1<<1);
    private static final int HAS_FULL_BIAS = (1<<2);
    private static final int HAS_BIAS = (1<<3);
    private static final int HAS_BIAS_UNCERTAINTY = (1<<4);
    private static final int HAS_DRIFT = (1<<5);
    private static final int HAS_DRIFT_UNCERTAINTY = (1<<6);

    private Context mContext;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private GnssMeasurementsEvent.Callback mGnssMeasurementsEventCallback;
    private String mLocationProvider;
    private IVirtualDeviceDataListener mLocationDataListener = null;
    private IVirtualDeviceDataListener mGnssMeasurementsDataListener = null;

    public VirtualLocation(Context context) {
        mContext = context;
    }

    public void registerLocationDataListener(IVirtualDeviceDataListener listener) {
        mLocationDataListener = listener;
    }

    public void registerGnssMeasurementsDataListener(IVirtualDeviceDataListener listener) {
        mGnssMeasurementsDataListener = listener;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @SuppressLint("MissingPermission")
    public void requestUpdates(CloudPhonePermissionRequestListener listener, int type) {
        if ((ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                && (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            CASLog.i(TAG, "request coarse and fine location permission");
            if (listener == null) {
                ActivityCompat.requestPermissions((Activity) mContext,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        DEV_TYPE_LOCATION);
            } else {
                listener.onRequestPermissions(new CloudPhonePermissionInfo(
                        DEV_TYPE_LOCATION, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}));
            }
        } else {
            if (mLocationManager == null) {
                mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            }
            if (type == TYPE_LOCATION) {
                requestLocationUpdates();
            } else if (type == TYPE_GNSS_MEASUREMENT) {
                requestGnssMeasurementUpdates();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @SuppressLint("MissingPermission")
    private void requestLocationUpdates() {
        if (mLocationManager == null) {
            return;
        }
        boolean locationEnable = mLocationManager.isLocationEnabled();
        if (!locationEnable) {
            CASLog.i(TAG, "location enable is false");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            mContext.startActivity(intent);
            return;
        }
        List<String> providers = mLocationManager.getProviders(true);
        if (providers.contains(LocationManager.GPS_PROVIDER)) {
            mLocationProvider = LocationManager.GPS_PROVIDER;
        } else if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
            mLocationProvider = LocationManager.NETWORK_PROVIDER;
        } else {
            return;
        }
        CASLog.i(TAG, "location provider is " + mLocationProvider);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    mLocation = location;
                    String locationInfo = getLocationInfo(location);
                    mLocationDataListener.onRecvData(locationInfo);
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
        mLocationManager.requestLocationUpdates(mLocationProvider, MIN_TIME, MIN_DISTANCE, mLocationListener, Looper.getMainLooper());
        mLocation = mLocationManager.getLastKnownLocation(mLocationProvider);
        String locationInfo = getLocationInfo(mLocation);
        if (locationInfo != null) {
            mLocationDataListener.onRecvData(locationInfo);
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void requestGnssMeasurementUpdates() {
        if (mLocationManager == null) {
            return;
        }
        mGnssMeasurementsEventCallback = new GnssMeasurementsEvent.Callback() {
            @Override
            public void onGnssMeasurementsReceived(GnssMeasurementsEvent eventArgs) {
                super.onGnssMeasurementsReceived(eventArgs);
                try {
                    handleReceivedGnssMeasurements(eventArgs);
                } catch (JSONException e) {
                    CASLog.e(TAG, "Failed to handle received gnss measurements");
                }
            }
        };
        mLocationManager.registerGnssMeasurementsCallback(mGnssMeasurementsEventCallback);
    }

    @SuppressLint("MissingPermission")
    public void closeLocationUpdates() {
        if (mLocationManager != null) {
            mLocationManager.removeUpdates(mLocationListener);
        }
        if (mLocationListener != null) {
            mLocationListener = null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void closeGnssMeasurementUpdates() {
        if (mLocationManager != null) {
            mLocationManager.unregisterGnssMeasurementsCallback(mGnssMeasurementsEventCallback);
        }
        if (mGnssMeasurementsEventCallback != null) {
            mGnssMeasurementsEventCallback = null;
        }
    }

    private String getLocationInfo(Location location) {
        if (location == null) {
            return null;
        }
        String telephonyCellInfo = getTelephonyCellInfo();
        String locationInfo = "longitude=" + location.getLongitude() +
                ":latitude=" + location.getLatitude() +
                ":altitude=" + location.getAltitude() +
                ":speed=" + location.getSpeed() +
                ":bearing=" + location.getBearing() +
                ":accuracy=" + location.getAccuracy();
        if (telephonyCellInfo != null) {
            locationInfo = locationInfo + ":" + telephonyCellInfo;
        }
        return locationInfo;
    }

    @SuppressLint("MissingPermission")
    private String getTelephonyCellInfo() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephonyManager.getNetworkOperator();
        int mcc = -1;
        if (operator != null && operator.length() == 3) {
            mcc = Integer.parseInt(operator.substring(0, 3));
        }

        List<CellInfoData> cellInfoDataList = new ArrayList<>();
        List<CellInfo> cellInfoList = telephonyManager.getAllCellInfo();
        for (CellInfo cellInfo : cellInfoList) {
            CellInfoData cellInfoData = new CellInfoData();
            cellInfoData.mcc = mcc;
            if (cellInfo instanceof CellInfoCdma) {
                CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
                CellIdentityCdma cellIdentityCdma = cellInfoCdma.getCellIdentity();
                cellInfoData.mnc = cellIdentityCdma.getSystemId();
                cellInfoData.lac = cellIdentityCdma.getNetworkId();
                cellInfoData.cid = cellIdentityCdma.getBasestationId();
                cellInfoData.cellType = "CDMA";
            } else if (cellInfo instanceof CellInfoGsm) {
                CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
                CellIdentityGsm cellIdentityGsm = cellInfoGsm.getCellIdentity();
                cellInfoData.mnc = cellIdentityGsm.getMnc();
                cellInfoData.lac = cellIdentityGsm.getLac();
                cellInfoData.cid = cellIdentityGsm.getCid();
                cellInfoData.cellType = "GSM";
            } else if (cellInfo instanceof CellInfoLte) {
                CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
                CellIdentityLte cellIdentityLte = cellInfoLte.getCellIdentity();
                cellInfoData.mnc = cellIdentityLte.getMnc();
                cellInfoData.lac = cellIdentityLte.getTac();
                cellInfoData.cid = cellIdentityLte.getCi();
                cellInfoData.cellType = "LTE";
            } else if (cellInfo instanceof CellInfoWcdma) {
                CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
                CellIdentityWcdma cellIdentityWcdma = null;
                if (SDK_INT >= JELLY_BEAN_MR2) {
                    cellIdentityWcdma = cellInfoWcdma.getCellIdentity();
                    cellInfoData.mnc = cellIdentityWcdma.getMnc();
                    cellInfoData.lac = cellIdentityWcdma.getLac();
                    cellInfoData.cid = cellIdentityWcdma.getCid();
                }
                cellInfoData.cellType = "WCDMA";
            } else {
                return null;
            }
            cellInfoDataList.add(cellInfoData);
        }
        if (cellInfoDataList.size() > 0) {
            return cellInfoDataList.get(0).getCellInfo();
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleReceivedGnssMeasurements(GnssMeasurementsEvent event) throws JSONException {
        mGnssMeasurementsDataListener.onRecvData(getData(event));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private static void gnssMeasurementData(ByteBuffer buffer, GnssMeasurement measurement) {
        int flags = 0;

        if (measurement.hasCarrierFrequencyHz()) {
            flags = flags | HAS_CARRIER_FREQUENCY;
        }

        if (measurement.hasCarrierCycles()) {
            flags = flags | HAS_CARRIER_CYCLES;
        }

        if (measurement.hasCarrierPhase()) {
            flags = flags | HAS_CARRIER_PHASE;
        }

        if (measurement.hasCarrierPhaseUncertainty()) {
            flags = flags | HAS_CARRIER_PHASE_UNCERTAINTY;
        }

        if (measurement.hasSnrInDb()) {
            flags = flags | HAS_SNR;
        }

        if (SDK_INT >= Build.VERSION_CODES.O) {
            if (measurement.hasAutomaticGainControlLevelDb()) {
                flags = flags | HAS_AUTOMATIC_GAIN_CONTROL;
            }
        }

        buffer.putInt((short) flags);
        buffer.putShort((short) measurement.getSvid());
        buffer.put((byte) measurement.getConstellationType());
        buffer.putDouble(measurement.getTimeOffsetNanos());
        buffer.putInt(measurement.getState());
        buffer.putLong(measurement.getReceivedSvTimeNanos());
        buffer.putLong(measurement.getReceivedSvTimeUncertaintyNanos());
        buffer.putDouble(measurement.getCn0DbHz());
        buffer.putDouble(measurement.getPseudorangeRateMetersPerSecond());
        buffer.putDouble(measurement.getPseudorangeRateUncertaintyMetersPerSecond());
        buffer.putShort((short) measurement.getAccumulatedDeltaRangeState());
        buffer.putDouble(measurement.getAccumulatedDeltaRangeMeters());
        buffer.putDouble(measurement.getAccumulatedDeltaRangeUncertaintyMeters());

        if (measurement.hasCarrierFrequencyHz()) {
            buffer.putFloat(measurement.getCarrierFrequencyHz());
        } else {
            buffer.putFloat(0);
        }

        if (measurement.hasCarrierCycles()) {
            buffer.putLong(measurement.getCarrierCycles());
        } else {
            buffer.putLong(0);
        }

        if (measurement.hasCarrierPhase()) {
            buffer.putDouble(measurement.getCarrierPhase());
        } else {
            buffer.putDouble(0);
        }

        if (measurement.hasCarrierPhaseUncertainty()) {
            buffer.putDouble(measurement.getCarrierPhaseUncertainty());
        } else {
            buffer.putDouble(0);
        }

        buffer.put((byte) measurement.getMultipathIndicator());

        if (measurement.hasSnrInDb()) {
            buffer.putDouble(measurement.getSnrInDb());
        } else {
            buffer.putDouble(0);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private static void gnssClockData(ByteBuffer buffer, GnssClock gnssClock) {
        int flags = 0;

        if (gnssClock.hasLeapSecond()) {
            flags = flags | HAS_LEAP_SECOND;
        }

        if (gnssClock.hasTimeUncertaintyNanos()) {
            flags = flags | HAS_TIME_UNCERTAINTY;
        }

        if (gnssClock.hasFullBiasNanos()) {
            flags = flags | HAS_FULL_BIAS;
        }

        if (gnssClock.hasBiasNanos()) {
            flags = flags | HAS_BIAS;
        }

        if (gnssClock.hasBiasUncertaintyNanos()) {
            flags = flags | HAS_BIAS_UNCERTAINTY;
        }

        if (gnssClock.hasDriftNanosPerSecond()) {
            flags = flags | HAS_DRIFT;
        }

        if (gnssClock.hasDriftUncertaintyNanosPerSecond()) {
            flags = flags | HAS_DRIFT_UNCERTAINTY;
        }

        buffer.putShort((short) flags);
        if (gnssClock.hasLeapSecond()) {
            buffer.putShort((short) gnssClock.getLeapSecond());
        } else {
            buffer.putShort((short) 0);
        }

        buffer.putLong(gnssClock.getTimeNanos());

        if (gnssClock.hasTimeUncertaintyNanos()) {
            buffer.putDouble(gnssClock.getTimeUncertaintyNanos());
        } else {
            buffer.putDouble(0);
        }

        if (gnssClock.hasFullBiasNanos()) {
            buffer.putLong(gnssClock.getFullBiasNanos());
        } else {
            buffer.putLong(0);
        }

        if (gnssClock.hasBiasNanos()) {
            buffer.putDouble(gnssClock.getBiasNanos());
        } else {
            buffer.putDouble(0);
        }

        if (gnssClock.hasBiasUncertaintyNanos()) {
            buffer.putDouble(gnssClock.getBiasUncertaintyNanos());
        } else {
            buffer.putDouble(0);
        }

        if (gnssClock.hasDriftNanosPerSecond()) {
            buffer.putDouble(gnssClock.getDriftNanosPerSecond());
        } else {
            buffer.putDouble(0);
        }

        if (gnssClock.hasDriftUncertaintyNanosPerSecond()) {
            buffer.putDouble(gnssClock.getDriftUncertaintyNanosPerSecond());
        } else {
            buffer.putDouble(0);
        }

        buffer.putInt(gnssClock.getHardwareClockDiscontinuityCount());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static ByteBuffer getData(GnssMeasurementsEvent event) {
        // 创建ByteBuffer，大小为 gnssMeasurementPacket结构体的字节数
        ByteBuffer buffer = ByteBuffer.allocate(GNSSPACKET_SIZE);

        // 设置字节序为小端序（与packed属性相对应）
        buffer.order(ByteOrder.BIG_ENDIAN);
        gnssClockData(buffer, event.getClock());
        buffer.put((byte) event.getMeasurements().size());

        int measurementCount = 1;
        for (GnssMeasurement measurement : event.getMeasurements()) {
            // 当Measurement卫星数量大于64时，取前64个卫星中的数据
            if (measurementCount > 64){
                break;
            }
            gnssMeasurementData(buffer, measurement);
            measurementCount+=1;
        }
        return buffer;
    }
    
    private class CellInfoData {
        private String cellType = "";
        private int mcc = -1;
        private int mnc = -1;
        private int lac = -1;
        private int cid = -1;

        private String getCellInfo() {
            return String.format("cell_type=%s:mcc=%d:mnc=%d:lac=%d:cid=%d", cellType, mcc, mnc, lac, cid);
        }
    }
}